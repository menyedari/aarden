<?php
# no namespace here


# legend :: info : readme
# -----------------------------------------------------------------------------------------------------------------------------
# This is a pre-flight check .. written in ancient PHP fashion for graceful fail, this extends the .htaccess
# -----------------------------------------------------------------------------------------------------------------------------




# action :: failed : used to fail explicitly
# -----------------------------------------------------------------------------------------------------------------------------
    function failed($m)
    {
        if (FALSE == isset($_SERVER)){ die("$m\n"); };
        if (FALSE == isset($_SERVER['HTTP_USER_AGENT'])){ die("$m\n"); }; // CLI
        header("HTTP/1.1 503 $m"); die();
    }
# -----------------------------------------------------------------------------------------------------------------------------




# prerun :: debug : make sure all is okay before bootstrap
# -----------------------------------------------------------------------------------------------------------------------------
   if ((FALSE == function_exists('version_compare')) || version_compare(phpversion(),'5.6','<'))
   { failed('Deprecated PHP version .. at least PHP v5.6 is required.'); }; // YOU HAVE DIED

   if ((FALSE == isset($_SERVER)) || (FALSE == isset($_COOKIE)) || (php_sapi_name() === 'cli'))
   { failed('Invalid LAMP environment, or expected to run via web browser.'); };  // YOU HAVE DIED

   ini_set('expose_php',false);         // hide PHP presence/version for security
   ini_set('short_open_tag',true);      // short syntax uniformity .. e.g: <?=
   ini_set('display_errors',true);      // required for debugging .. handled elsewhere
   ini_set('max_execution_time',60);    // never exceed this, except when demanded
   ini_set('default_charset','UTF-8');  // for glyph uniformity

   if(!(ini_get('short_open_tag')))
   { failed("Config Dependecy - short_open_tag is expected to be: On"); };  // YOU HAVE DIED

   chdir(__DIR__); $_SERVER['ROOTPATH']=__DIR__; $_SERVER['COREPATH']=(__DIR__."/.aard.dir");
   require ($_SERVER['COREPATH'].'/Import/server.aard.php');
# -----------------------------------------------------------------------------------------------------------------------------
