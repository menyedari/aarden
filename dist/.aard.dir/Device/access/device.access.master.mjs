


/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: *\

                                                              .___
                                      _____  _____ _______  __| _/____   ____
                                      \__  \ \__  \\_  __ \/ __ |/ __ \ /    \
                                       / __ \_/ __ \|  | \/ /_/ \  ___/|   |  \
                                      (____  (____  /__|  \____ |\___  >___|  /
                                           \/     \/           \/    \/     \/

                               d  e  v  i  c  e  .  a  c  c  e  s  s  .  m  a  s  t  e  r

\* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */





// readme :: info
// ----------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------





// system :: symbol : please do not expose -or export this symbol during runtime in any way whatsoever
// ----------------------------------------------------------------------------------------------------------------------------
    const SYSTEM = Symbol("<SYSTEM>");
// ----------------------------------------------------------------------------------------------------------------------------





// global :: references : please keep this here as the first define block .. to be secured by the *facing module
// ----------------------------------------------------------------------------------------------------------------------------
    Object.assign( globalThis,
    {
        "dump": console.log.bind( console ),
        "moan": console.error.bind( console ),
    });
// ----------------------------------------------------------------------------------------------------------------------------





// global :: Null/Undefined : constructors .. global references for `strain` .. see static method: `Medium.strain` below
// ----------------------------------------------------------------------------------------------------------------------------
    Object.assign( globalThis,
    {
        "Null": new (class Null extends null
        {
            constructor ( )
            {
                return Object.create( Null.prototype );
            }

            valueOf ( )
            {
                return null
            };
        }),


        "Undefined": new (class Undefined extends null
        {
            constructor ( )
            {
                return Object.create( Undefined.prototype );
            }

            valueOf ( )
            {
                return undefined
            };
        }),
    });
// ----------------------------------------------------------------------------------------------------------------------------





// global :: Medium : general extendable proxy
// ----------------------------------------------------------------------------------------------------------------------------
    globalThis.Medium = class Medium
    {
    // new :: Medium : constructor
    // ------------------------------------------------------------------------------------------------------------------------
        constructor ( )
        {
            let entity = this;
            let router = Medium.routed( this, ...arguments );
            let system = router[ SYSTEM ];
            let naming = system.naming;

            Listen( system );


            Object.assign( system,
            {
                config ( design )
                {
                    Medium.assign( this.config, design );
                    return this.medium;
                },


                relate ( intake, string, target )
                {
                    let detect = Medium.detect( intake, 9 );

                    if ( (detect === "object") )
                    {
                        Medium.assign( this.relate, intake );
                        return this.medium;
                    };

                    if ( (detect === "string") && Medium.detect(string,"string") && Medium.detect(target,"string") )
                    {
                        string.split(",").scroll(( aspect )=>
                        {
                            if ( !this.relate[intake] ){ this.relate[intake] = {} };
                            this.relate[intake][ aspect ] = target;
                        });

                        return this.medium;
                    };
                },


                scroll ( intake=[] )
                {
                    let detect = Medium.detect( intake );
                    let output;

                    this.relate.get[ ABSENT ].scroll(( source )=>
                    {
                        let holder = this[source];

                        if ( !Array.isArray( holder ) )
                        { holder = [ holder ] };

                        holder.scroll(( target )=>
                        {
                            Medium.indice( target, "*" ).scroll(( aspect )=>
                            {
                                if ( detect === "arra" )
                                { intake.push( aspect ); return };

                                if ( detect === "func" )
                                { output = intake.apply( this, [aspect, source] ); return output };

                                moan( "expecting intake as: array, or function" );
                                output = FINISH;  return output;
                            });

                            return output;
                        });

                        return output;
                    });

                    if ( detect === "arra" )
                    { return intake };
                },


                locate ( aspect, action="get", backup="entity" )
                {
                    let routes = ( this.relate[action] || this.relate[ABSENT] );
                    let search = ( routes[aspect] || routes[ABSENT] );
                    let detect = ( typeof search ), detail, output, parted, routed;

                    if ( detect === "string" )
                    {
                        if ( !search.includes(".") )
                        { search = ( "entity." + search ) };

                        parted = search.split(".");
                        routed = parted[0];
                        detail = parted[1];
                        output = { direct:true, routed, target:this[routed], detail:this[routed][detail] };

                        if ( (typeof output.detail) === "function" )
                        {
                            output.detail = Medium.create(aspect,output.detail);
                            output.detail = output.detail.bind(output.detail);
                        };

                        return output;
                    };

                    search.scroll(( routed )=>
                    {
                        detail = this[routed];
                        detail = ( !Array.isArray(detail) ? detail : (Medium.locate(detail,aspect)||{}) );

                        Medium.indice( detail ).scroll(( locate )=>
                        {
                            if ( locate !== aspect ){ return };
                            output = { routed, target:detail };
                            return FINISH;
                        });

                        if ( !!output ){ return FINISH };
                    });

                    if ( !!output ){ return output };
                    return {routed:backup, target:this[backup]};
                },


                cohere ( )
                {
                    this.scroll(( aspect, source )=>
                    {
                        this.routes.get[aspect] = [source];
                    });
                },


                enable ( design )
                {
                    Object.keys( design ).map(( action )=>
                    {
                        this.router[ action ] = Medium.create(action, function ( )
                        {
                            let aspect = arguments[1];
                            if ( aspect === Symbol.isProxy ){ return true };
                            let system = Medium.indice( this, "$" )[0];
                            if ( aspect === system ){ return this[( system )] }; // SYSTEM symbol
                            let action = Medium.caller().from.split(".").pop();
                            return this[ system ].access( action, ...arguments )
                        });

                        this.listen( action, design[action] );
                    });

                    return this.medium;
                },


                access ( action, origin, aspect, detail, medium )
                {
                    if ( (Medium.stored.Innate).includes(aspect) )
                    { return this.entity[aspect] }; // direct innate relay

                    if ( !!detail && detail[Symbol.isProxy] )
                    { medium = detail;  detail = SELECT }; // get

                    let source = this.locate( aspect, action );

                    if ( source.direct )
                    { return source.detail };

                    let routed = source.routed;
                    let target = source.target;
                    let custom = this.config;
                    let config = Medium.assign( {}, [custom.access, custom.action[action] ] );
                                 Medium.assign( config, [custom.aspect[aspect], custom.routed[routed]] );
                    let traced = ( config.traced ? {traced:Medium.caller(config.traced)} : {} );
                    let convey = Object.assign( {aspect,detail,config,origin,routed,target}, traced );
                    let output = this.system.signal( action, convey );

                    if ( (typeof output) !== "undefined" )
                    { return output }; // from listener

                    output = Medium.access( target, aspect, detail, config.joiner );
                    return output;
                },
            });


            system.config // convey config inside signal .. these are required
            ({
                access: { medium:naming, traced:false, joiner:"." }, // always present
                action: { defineProperty: {export:true} }, // convey if action match these traps
                aspect: { }, // convey if aspect match these properties
                routed: { }, // convey if routed match these sources
            });


            system.relate
            ({// action:
                get:
                {// aspect:
                    [ ABSENT ]: ["entity","system","origin","target","intake"],

                    // select: "entity.herald",
                },

             // action: missing
                [ ABSENT ]:
                {// aspect: missing
                    [ ABSENT ]: ["entity"]
                },
            });


            Object.assign( system,
            {
                medium: new Proxy ( system.origin, router ),
                memory: { herald:{}, recent:{}, stored:{} },
                router,
            });


            system.enable
            ({
                get ( signal )
                {
                    let output = signal.target[ signal.aspect ];
                    return output;
                },


                has ( signal )
                {
                    let output = this.system.scroll();
                    return ( output.indexOf( signal.aspect ) > -1 );
                },


                set ( signal )
                {
                    signal.target[ signal.aspect ] = signal.detail;
                    return ( signal.target[ signal.aspect ] === signal.detail );
                },


                ownKeys ( signal )
                {
                    let output = [ ...(new Set(this.system.scroll())) ];

                    output.unshift( "prototype" );

                    return output;
                },


                defineProperty ( signal )
                {
                    Object.defineProperty( signal.target, signal.aspect, signal.detail );

                    if ( signal.config.export && (signal.target !== signal.origin) )
                    {
                        try{ Object.defineProperty( signal.origin, signal.aspect, signal.detail ) }
                        catch( failed ){ moan( failed ) };
                    };

                    return Medium.indice( signal.target ).includes( signal.aspect );
                },
            });


            setTimeout( function mediumExists()
            {
                this.signal( "MediumAwaits", naming );
            }.bind(system), 0);


            return system.medium;
        }
    // ------------------------------------------------------------------------------------------------------------------------



    // symbol :: detail : @@toPrimitive .. override `valueOf` primitive values by selecting `hint` option in [SYSTEM].output
    // ------------------------------------------------------------------------------------------------------------------------
        [ Symbol.toPrimitive ] ( hinted )
        {
            return Medium.primed( this[ SYSTEM ].primal[ hinted ], hinted );
        }
    // ------------------------------------------------------------------------------------------------------------------------


    // ::: ABOVE : for `new Medium` only ... these below will not be carried over to new instances



    // ::: BELOW : static aspects that compliments `Object` -but `Object.values` exists and we should not impose



    // TODO !!
    // symbol :: detail : @@hasInstance .. override `instanceof`
    // ------------------------------------------------------------------------------------------------------------------------
        // static [ Symbol.hasInstance ] ( intake )
        // {
        //     return intake[ Symbol.isProxy ];
        // }
    // ------------------------------------------------------------------------------------------------------------------------
    // symbol :: praxis : ?



    // .stored :: detail : library for useful things
    // ------------------------------------------------------------------------------------------------------------------------
        static stored = Object
        ({
            Primes: "undefined null boolean number string symbol bigint",
            Recent: {}, // mem-cache

            Extend: function Extend ( naming, schema, assign )
            {
                let script = `class ${(naming+schema.name)} extends schema { constructor (){ return super( ...arguments ) } }`;
                let output = Script.render( script, {naming,schema,assign} );
                return output;
            },

            Design:
            {
                Router: class Router
                {
                    constructor ( naming, schema, assign )
                    {
                        assign.naming = ( assign.naming || naming );
                        let system = Medium.stored.Extend( naming, Medium.stored.Design["System"], assign );

                        Medium.secure( this, SYSTEM, (new (system)( assign )) );
                    }

                    valueOf ( hinted )
                    {
                        return this[ SYSTEM ].target;
                    }
                },

                System: class System
                {
                    constructor ( assign )
                    {
                        Medium.secure( this, assign );
                    }
                },
            },

            Permit: Object.assign( (new (function Permit(){})), { configurable:false, enumerable:false, writable:false } ),
            Innate: [ ...(Reflect.ownKeys((function x(){}))), ...(Reflect.ownKeys(Object.getPrototypeOf({}))) ].concat
                    ([ Symbol.toPrimitive, Symbol.toStringTag, "arguments", "callee", "caller" ]),
        })
    // ------------------------------------------------------------------------------------------------------------------------
    // .stored :: praxis : new (Medium.stored.Schema); <- Schema{}



    // .detect :: detail : to avoid frustrations like: `(typeof null)` <- "object" .. and `(typeof [])` <- "object"
    // ------------------------------------------------------------------------------------------------------------------------
        static detect ( intake, option=4 )
        {
            let affirm = ( !!option.split ? option.split(",") : null );
            let length = ( !affirm ? option : affirm[0].length );
            let output = ( Array.isArray( intake ) ? "array" : (typeof intake) );
                output = ( !output.startsWith("f") ? output : (intake.toString().startsWith("class ") ? "class" : output ) );
                output = ( (!intake && (intake === null)) ? "null" : output ).slice( 0, length );

            return ( !affirm ? output : (affirm.includes( output ) ? output : "") ); // expected return type: string
        }
    // ------------------------------------------------------------------------------------------------------------------------
    // .detect :: praxis : Medium.detect("yo") <- "stri" .. .detect( [], 3 ) <- "arr" .. .detect( {}.x, "unde,null" ) <- "unde"



    // .strain :: detail : identify class .. this is also globally identifiable in cases of Null and Unidentified classes above
    // ------------------------------------------------------------------------------------------------------------------------
        static strain ( intake )
        {
            let output = ( ({}).toString.call( intake ) ).split(" ").pop().slice( 0, -1 );
            return output;
        }
    // ------------------------------------------------------------------------------------------------------------------------
    // .strain :: praxis : Medium.strain( null ) <- "Null"



    // .create :: detail : create new objects, or new object-instances
    // ------------------------------------------------------------------------------------------------------------------------
        static create ( intake, design, assign, stored=true )
        {
            let family = Medium.detect( intake, 9 );
            let strain = Medium.detect( design, 9 );
            let hybrid = ( family + "_" + strain );
            let output, schema, naming, script, method;


            if ( !("object,string,function").includes( family ) )
            {
                return Medium.merged( design );
            };


            if ( hybrid === "object_object" )
            {
                return Medium.merged( Medium.cloned(intake), Medium.cloned(design) );
            };


            if ( family === "function" )
            {
                naming = Medium.naming( intake );
                output = Medium.create( naming );
                design = Medium.merged( intake, design );

                return Medium.assign( output, design );
            };


            if ( ("symbol,string").includes(family) )
            {
                if ( !design || (strain==="object") )
                {
                    script = `new (function ${intake}(){})`;

                    if ( stored )
                    {
                        output = Medium.stored.Recent[intake];

                        if ( output === undefined )
                        {
                            output = Script.render( script );
                            Medium.stored.Recent[intake] = output;
                        };
                    }
                    else
                    {
                        output = Script.render( script );
                    };

                    output = Object.create( output );

                    if ( !!design )
                    { Object.assign( output, design ) };

                    return output;
                };


                if ( ("symbol,string").includes(strain) )
                {
                    let schema = Medium.stored.Design[design];

                    if ( !Medium.detect(schema,"class")  )
                    { moan( `expecting optional design[${strain}] to exist as class in: Medium.stored.Design` );  return };

                    let extend = Medium.stored.Extend( intake, schema, assign );

                    output = new ( extend )( intake, schema, assign );
                    return output;
                };


                if ( strain === "function" )
                {// given (string,method) .. so below we render output from function.toString()
                    script = (design + "");

                    if ( script.startsWith("function ") )
                    { script = script.slice(9) };

                    script = script.slice( script.indexOf("(") );
                    output = Script.render( `function ${intake} ${script}`, assign );

                    return output;
                };

                // string = `return ( function ${intake}(){} )`;
                //
                // if ( stored && !Medium.stored.Create[intake] )
                // { Medium.stored.Create[intake] = Script.render(string) }; // remember result
                //
                // if ( stored )
                // { output = new ( Medium.stored.Create[ intake ] ) };
                //
                // return Medium.assign( output, design );
            };


            return output;
        }
    // ------------------------------------------------------------------------------------------------------------------------
    // .create :: praxis : Medium.create() <- {} .. .create("foo") <- foo{} .. .create( {}, [{a:1},{b:2}] ) <- { a:1, b:2 }



    // .routed :: detail : normalised intake tailored for given reformed method as origin
    // ------------------------------------------------------------------------------------------------------------------------
        static routed ( )
        {
            let intake = [ ...arguments ];
            let entity, origin, target, family, naming;

            if ( (typeof intake[0]) !== "function" )
            { entity = intake.shift() };

            origin = ( intake.shift() || {} );
            target = ( intake.shift() || {} );
            family = Medium.detect( origin );
            naming = Medium.naming( (entity||origin), "Medium" ); // replace "Medium" with origin-name

            if ( !intake.length && (Array.isArray(target) || (Medium.strain(target) === "Arguments")) )
            {
                intake = [ ...target ];
                target = intake.shift();
            };

            let output = Medium.create( naming, "Router", {entity, origin, target, intake} );
            let system = output[ SYSTEM ];
            let format = Medium.detect(target,9);  format = ( format[0].toUpperCase() + format.slice(1) ); // .. toCamelCase
            let handle = ( origin[ (Script.facing.access+format) ] || origin[ format ] || (entity?undefined:origin) );

            Object.assign( system, { system } );

            if ( !!entity || (family !== "func") || !handle )
            { return output }; // medium .. or anything else than handler

            Medium.assign( output, handle );
            return output; // handler for function calls by intake-type as implied by this call to manage family by handler
        }
    // ------------------------------------------------------------------------------------------------------------------------
    // .routed :: praxis : ?



    // .expect :: detail : flow control helper .. moans gracefully .. ... without throwing errors
    // ------------------------------------------------------------------------------------------------------------------------
        static expect ( intake, affirm )
        {
            let result = Medium.detect( intake );

            if ( ((typeof result) !== "string") || ((typeof affirm) !== "string") )
            { moan(`expecting affirm and method-return as string`); return };

            if ( affirm.includes( result ) )
            { return result };

            let traced = Medium.caller("*").slice(2); Object.assign( (new function caller(){}),  );

            moan( "expecting: " + affirm + "\n", traced );
            return false;
        }
    // ------------------------------------------------------------------------------------------------------------------------
    // .expect :: praxis : Medium.expect( "detect", null, "stri" ) <- false // see moan() output .. console.error


    // .caller :: detail : identify class .. this is also globally identifiable in cases of Null and Unidentified classes above
    // ------------------------------------------------------------------------------------------------------------------------
        static caller ( select=0, ignore={from:"ModuleJob.run"} )
        {
            let format = (typeof select);
            let traced = ( (select instanceof Error) ? select : (new Error(".")) ).stack.split("\n");
            let output = [], inline, string, pieces, record, number = 0;
            let prefix = "$/";

            traced.shift(); traced.shift(); // for the `.` error-line above and static caller

            if ( (select === "?") || (select === SYSTEM) )
            { return traced }; // useful for debugging

            if (format === "function")
            { return traced.filter(select) }; // function was given as filter

            for (string of traced)
            {
                string = string.trim();  if (!string){ continue };
                pieces = string.split( (string.includes(" ")?" ":"@") );

                if (!pieces[0] || (pieces[0]==="at")){ pieces.shift() };
                if (pieces.length < 2){ pieces.unshift("Global") };
                if (pieces[1].startsWith("(") && pieces[1].endsWith(")")){ pieces[1] = pieces[1].slice(1,-1) };

                pieces[1] = pieces[1].split("://").pop(); pieces[1] = pieces[1].slice(pieces[1].indexOf("/")+1);
                record = { from:pieces[0],  file:pieces[1].split(":")[0],  line:(pieces[1].split(":")[1]*1)};

                if ( record.file.endsWith("(eval") || (record.from === "new") )
                {
                    if (record.from === "new")
                    { record.from += (" "+pieces[1]) };

                    string = ((pieces.length > 5) ? pieces[4] : pieces.pop() ).split("/aarden/").pop();
                    pieces = string.split(":");
                    record.file = pieces[0];
                    record.line = ( pieces[1] * 1 );
                };

                if ( record.file.startsWith("aarden/") )
                {
                    record.file = ( "/" + record.file );
                };

                if ( record.file.includes(".aard.dir") )
                {
                    record.file = ( prefix + record.file.split(".aard.dir/").pop() );
                }
                else
                {
                    record.file = ( prefix + record.file.split("/aarden/").pop() );
                };

                if ( record.file === "$/<anonymous>" )
                { continue };


                // if ( !record.line || ignore.from.includes(record.from) )
                // { continue };

                if ( (format === "number") && (number === select) )
                { return record };

                if ( (format === "string") && (record.from.includes(select) || record.file.includes(select)) )
                { return record };

                output.push(record);
                number++;
            };

            return output;
        }
    // ------------------------------------------------------------------------------------------------------------------------
    // .caller :: praxis : ?


    // .locate :: detail : returns the first object that contains aspect
    // ------------------------------------------------------------------------------------------------------------------------
        static locate ( within, aspect, joiner, naming )
        {
            let format = Medium.detect( aspect ), object, locale, output;
            let access = ( (format === "arra") ? true : ((format === "stri") && !!joiner && aspect.includes( joiner )) );

            if ( !Array.isArray( within ) )
            { within = [within] };

            if ( !!naming && (naming.length !== within.length) )
            { moan( "expecting 1st & optional-but-given-4th-argument to be of the same type & length" ); return };

            for ( let number in within )
            {
                object = within[number];

                if ( !object || !("object,function").includes(typeof object) )
                { continue }; // goes for arrays too

                if ( !access && Medium.indice( object, "*" ).indexOf( aspect ) > -1 ) // .. including symbols
                { return ( Array.isArray(naming) ? {naming:naming[number],detail:object} : object ) };

                if ( !access ){ continue };
                output = Medium.access( object, aspect, HOLDER, joiner ) // multi-dimensional search
                if ( (typeof output) !== "undefined" )
                { return output }; // break loop
            };
        }
    // ------------------------------------------------------------------------------------------------------------------------
    // .locate :: praxis : Medium.locate( [{bar:1},{foo:2}], "bar" ); <- {bar:1}


    // .select :: detail : ?
    // ------------------------------------------------------------------------------------------------------------------------
        static select ( within, search )
        {
            if ( !Expect( within ).detect("obje","as within") || !Expect( search ).detect("stri,arra","as search") )
            { return };

            if ( (typeof search) === "string" )
            { search = search.split(",") };

            let aspect, output = [];

            for ( aspect of [...search] )
            {
                let detail = within[aspect];
                if ( Array.isArray( detail ) )
                {
                    detail.map(( object )=>
                    {
                        output.push( object );
                        if ( search.length !== output.length )
                        { search.push(aspect) };
                    });
                }
                else
                { output.push( detail ) }
            };

            return output;
        }
    // ------------------------------------------------------------------------------------------------------------------------
    // .select :: praxis : ?



    // .access :: detail : traverse object-levels by string/array reference .. given detail in crud-speak means MODIFY
    // ------------------------------------------------------------------------------------------------------------------------
        static access ( holder, locate, detail=SELECT, joiner="." )
        {
            let aspect;  holder = ( holder || {} );
            let depend = [ SELECT, EXISTS, DELETE, HOLDER ]; // these depend on if aspect exists while browsing
            let detect = Medium.detect( locate );
            let ladder = ( (detect==="arra") ? locate : ((detect==="stri") ? locate.split( joiner+"" ) : [locate]) );
            let option = ( depend.includes( detail ) ? detail : MODIFY );

            if ( Array.isArray( holder ) )
            { holder = ( Medium.locate(holder,ladder[0]) || {} ) };

            while ( ladder.length > 0 )
            {
                aspect = ladder.shift();

                if ( !Medium.indice(holder).includes( aspect ) ) // NB :: Object.hasOwnProperty() - is unreliable here
                {
                    if ( depend.includes( option ) )
                    { return }; // undefined .. so stop here

                    if ( (option === MODIFY) && (ladder.length > 0) )
                    { holder[ aspect ] = {} };
                };

                if ( (ladder.length > 0) )
                { holder = holder[ aspect ] };
            };

            if ( (typeof detail) === "function" )
            { detail = detail.bind( holder ) } // this may not be required .. chamnge if issues arise .. use Medium.secure
            else if ( option === DELETE )
            { detail = (holder||{})[aspect] };

            switch ( option )
            {
                case ( HOLDER ) : return holder;
                case ( SELECT ) : return holder[ aspect ];
                case ( EXISTS ) : return Object.keys( holder ).includes( aspect );
                case ( MODIFY ) : holder[ aspect ] = detail;  return holder; // the `.bind()` above is probably because of this
                case ( DELETE ) : delete holder[ aspect ];  return detail;
            };
        }
    // ------------------------------------------------------------------------------------------------------------------------
    // .access :: praxis : Medium.access( {foo:{bar:{zee:3}}}, "foo/bar/zee" ); <- 3



    // .primed :: detail : return intake converted to primitive value
    // ------------------------------------------------------------------------------------------------------------------------
        static primed ( intake, hinted, invoke=true )
        {
            let output = intake, format;
            let primes = Medium.stored.Primes;

            if ( primes.includes( Medium.detect( intake, 9 ) ) )
            { return output };

            if ( ((typeof output.apply) === "function") && invoke )
            { output = output.apply( output, [ hinted ] ) }
            else if ( (typeof output.toString) === "function" )
            { output = output.toString() };

            format = (typeof output);

            if ( primes.includes( format ) )
            { return output };

            try { output = JSON.stringify( output ) }
            catch ( failed ){ output += "" };

            return output;
        }
    // ------------------------------------------------------------------------------------------------------------------------
    // .primed :: praxis : Medium.primed( {foo:2} ); <- '{"foo":2}'



    // .origin :: detail : get/set the prototype of intake .. if no schema: get, else set prototype of intake to schema
    // ------------------------------------------------------------------------------------------------------------------------
        static origin ( intake, schema )
        {
            if ( Medium.detect(intake,"unde,null") )
            { return intake };

            if ( !schema )
            { return ( intake.prototype || Object.getPrototypeOf( intake ) ) };

            Object.setPrototypeOf( intake, schema )
            return intake;
        }
    // ------------------------------------------------------------------------------------------------------------------------
    // .origin :: praxis : Medium.origin( {foo:2}, {bar:1} ).bar; <- 1



    // .naming :: detail : get the name of an object, or get name from object's constructor .. or fallback to backup/""
    // ------------------------------------------------------------------------------------------------------------------------
        static naming ( intake, backup, ignore )
        {
            let output = ( Medium.detect(intake,"unde,null,bool") ? Medium.strain( intake ) : intake.name );
                output = ( output || (intake[Symbol.isProxy] ? intake[SYSTEM].naming : "" ));
                output = ( output || (intake.constructor||{}).name || backup || "" );

            if ( !!backup && !!ignore && ignore.includes(output) )
            { output = backup }; // convenience .. #DRYKIS

            if ( output.startsWith("bound ") && ((typeof intake)==="function") )
            { output = output.slice(6) };

            return output;
        }
    // ------------------------------------------------------------------------------------------------------------------------
    // .naming :: praxis : Medium.naming( {} ); <- "Object"



    // .permit :: detail : returns a new property descriptor based on shorthand string for Holder.memory.Permit object keys
    // ------------------------------------------------------------------------------------------------------------------------
        static permit ( intake="", detail, joiner="," )
        {
            if ( !Medium.expect( intake, "stri,arra" ) )
            { return };

            if ( (typeof intake) === "string" )
            { intake = intake.toLowerCase().split( ( (intake.includes(joiner) || intake.length > 3) ? joiner : "" ) ) };

            let output = Object.assign( Object.create(Medium.stored.Permit), Medium.stored.Permit);
            let choice = Object.keys( output ), chosen;

            output.value = detail;

            while ( choice.length > 0 )
            {
                let option = choice.shift();

                for ( chosen of intake )
                {
                    if ( option.startsWith( chosen ) )
                    { output[option] = true;  break };
                };
            };

            return output;
        }
    // ------------------------------------------------------------------------------------------------------------------------
    // .permit :: praxis : Medium.permit("cw"); <- { configurable:true, enumerable:false, writable:true, value:undefined }



    // .indice :: detail : keys of given intake
    // ------------------------------------------------------------------------------------------------------------------------
        static indice ( intake, select, retain, ignore, joiner="," )
        {
            let detect = Medium.expect( intake, "stri,arra,obje,func,clas" );
            let output = [], indice, origin, aspect;
                ignore = ( !ignore ? Medium.stored.Innate : (!Medium.detect(ignore,"array") ? [ignore] : ignore) );
                retain = ( !retain ? ["name"] : (!Medium.detect(retain,"array") ? [retain] : retain) );
            let symbol = "$@"; // select by glyph .. $ = (typeof symbol) .. @ = circular

            if ( !detect ){ return [] }; // already moaned about it in above Medium.expect() .. an array is expected as output
            if ( detect === "stri" )
            { intake = intake.split( (joiner||"") );  detect="arra" };

            indice = ( ((select === "*") || (intake instanceof Medium)) ? Reflect.ownKeys( Medium.origin(intake) ) : [] );
            indice = indice.concat( Reflect.ownKeys( intake ) );

            for ( aspect of indice )
            {
                if ( output.includes(aspect) || (ignore.includes(aspect) && !retain.includes(aspect))  )
                { continue };

                if ( select === aspect )
                { return true };

                if ( !symbol.includes( select ) )
                { output.push( aspect ); continue };

                if ( (select === "$") && ((typeof aspect) === "symbol") )
                { output.push( aspect ); continue };

                if ( (select === "@") && (intake === intake[aspect]) )
                { output.push( aspect ); continue };
            };

            return ( ((typeof select)==="number") ? output.slice(select,1) : output );
        }
    // ------------------------------------------------------------------------------------------------------------------------
    // .indice :: praxis : Medium.indice({foo:2,bar:1}); <- [ "foo", "bar" ]



    // .values :: detail : values of given intake
    // ------------------------------------------------------------------------------------------------------------------------
        static values ( intake, select, joiner="," )
        {
            let detect = Medium.expect( intake, "stri,arra,obje,func,clas" );
            if ( !detect ){ return [] }; // already moaned about it in above Medium.expect() .. an array is expected as output

            if ( detect === "stri" )
            { intake = intake.split( (joiner||"") ) };

            if ( ((typeof select) === "number") && (select < 0) )
            { select = (indice.length + select) };

            let indice = Medium.indice( intake, select, joiner );
            let output = [];

            while ( indice.length > 0 )
            {
                let aspect = indice.shift();

                if ( select === indice.length )
                { return intake[aspect] };

                output.push( intake[aspect] );
            };

            return output;
        }
    // ------------------------------------------------------------------------------------------------------------------------
    // .values :: praxis : Medium.values({foo:2,bar:1}); <- [ 2, 1 ]



    // .merged :: detail : assign all properties of all arguments to new object
    // ------------------------------------------------------------------------------------------------------------------------
        static merged ( )
        {
            let intake = [ ...arguments ];
            let expect = "object,function";
            let output = {};

            intake.map(( object )=>
            {
                if ( !expect.includes(typeof object) )
                { return };

                Medium.indice( object ).map(( aspect )=>
                { output[aspect] = object[aspect] });
            });

            return output;
        }
    // ------------------------------------------------------------------------------------------------------------------------
    // .merged :: praxis : Medium.merged({foo:2},{bar:1}); <- {foo:2,bar:1}



    // .secure :: detail : short-hand for Object.defineProperty() .. user config from Medium.permit
    // ------------------------------------------------------------------------------------------------------------------------
        static secure ( target, holder, detail, ruling, forced )
        {
            if ( !detail && ((typeof holder)==="function") && !!holder.name )
            {
                detail = holder;
                holder = detail.name;
            };


            let format = [ Medium.detect(holder) , Medium.detect(detail) ];
            let forjim = format.join(",");
            let indice = [], values = [], aspect, permit, config;
            let exists = Medium.indice( target );


            if ( format[1] === "unde" )
            {
                indice = ( (format[0] === "stri") ? Medium.values(holder) : Medium.indice(holder) );
                values = ( (format[0] === "stri") ? [] : Medium.values(holder) );
            }
            else if ( ("stri,symb").includes(format[0]) )
            {
                indice = [holder];
                values = [detail];
            }
            else
            { moan( "invalid specifications" ); return };


            while ( indice.length )
            {
                aspect = indice.shift();
                aspect = ( !!aspect.trim ? aspect.trim() : aspect );
                detail = values.shift();
                format = (typeof aspect);
                permit = ruling;

                if ( (format === "string") && aspect.includes("@") )
                {
                    parted = aspect.split("@");
                    aspect = parted[0].trim();
                    permit = (parted[1]||"").trim();
                };

                if ( ("stri,unde|symb,unde").includes( forjim ) )
                {
                    detail = ((Medium.detect(aspect,"stri")&&(aspect.toUpperCase()===aspect))?Symbol("<"+aspect+">") : aspect);
                    detail = (exists.includes( aspect ) ? target[aspect] : detail);
                }
                else if ( (detail===undefined) && (indice.length > values.length) )
                { detail = target[aspect] };

                config = Medium.permit( permit, detail);

                if ( forced ) // relax it's fine ... or else it will fail
                { delete target[aspect] };

                Object.defineProperty( target, aspect, config );
            };

            return target;
        }
    // ------------------------------------------------------------------------------------------------------------------------
    // .secure :: praxis : Medium.secure( {bar:0}, {bar:1} ).bar = 2; <- Error: cannot re-define 'bar'



    // .assign :: detail : multi-level and multi-object assign
    // ------------------------------------------------------------------------------------------------------------------------
        static assign ( intake, design, permit="CEW", levels=999 )
        {
            if ( !design ){ design = [] };
            if ( !Array.isArray( design ) ){ design = [design] };

            design.map( function eachDesign( schema )
            {
                if ( !Medium.detect(schema,"obje,func,arra") )
                { return };

                Medium.indice( schema ).map( function eachAspect( aspect )
                {
                    Medium.secure( intake, aspect, schema[aspect], permit );

                    if ( (levels > 0) && Medium.detect(schema[aspect],"obje") )
                    {
                        levels--;

                        if ( Medium.detect(intake[aspect]) !== Medium.detect(schema[aspect]) )
                        { intake[aspect] = schema[aspect] };

                        Medium.assign( intake[aspect], schema[aspect], permit, levels );
                    };
                });
            });

            return intake;
        }
    // ------------------------------------------------------------------------------------------------------------------------
    // .assign :: praxis : ?



    // .anneal :: detail : multi-level Medium.secure
    // ------------------------------------------------------------------------------------------------------------------------
        static anneal ( intake, design, levels=1 )
        {
            return Medium.assign( intake, design, "E", levels );
        }
    // ------------------------------------------------------------------------------------------------------------------------
    // .anneal :: praxis : ?



    // .supply :: detail : supply what is missing only
    // ------------------------------------------------------------------------------------------------------------------------
        static supply ( intake, supply, levels=999, change=[undefined,null] )
        {
            let indice = Medium.indice( supply );
            let exists = Medium.indice( intake );

            while ( (indice.length > 0) &&  (levels > 0) )
            {
                let aspect = indice.shift();

                if ( !exists.includes( aspect )  )
                {
                    try { intake[aspect] = supply[aspect];  continue; }
                    catch( failed ){};
                };

                let format = [ Medium.detect( intake[aspect] ), Medium.detect( supply[aspect] ) ];

                if ( !("obje,arra").includes( format[0] ) && !("obje,arra").includes( format[1] ) )
                {
                    if ( change.indexOf( intake[aspect] ) > -1 )
                    { intake[aspect] = supply[aspect] };
                    continue;
                };

                levels -= 1;

                if (  levels > 0 )
                {
                    Medium.supply( intake[aspect], supply[aspect], levels, change );
                    continue;
                };

                moan(`maximum level exceeded .. provide levels (number)`);
            };

            return intake;
        }
    // ------------------------------------------------------------------------------------------------------------------------
    // .supply :: praxis : ?



    // .reform :: detail : combines `method` and `design` as one thing
    // ------------------------------------------------------------------------------------------------------------------------
        static reform ( method, design )
        {
            return Medium.anneal( method, design );
            // return Medium.anneal( method, Medium.create( method, design ) ); // old?
        }
    // ------------------------------------------------------------------------------------------------------------------------
    // .reform :: praxis : ?



    // .herald :: detail : normalised intake for signals
    // ------------------------------------------------------------------------------------------------------------------------
        static herald ( origin, intake, custom )
        {
            if ( !Expect((origin||{}).signal).detect("function", "as origin.signal") )
            { return };

            if (Medium.strain(intake) === "Arguments")
            { intake = [ ...intake ] };

            if ( !Expect(intake).detect("array", "as intake") )
            { return };

            let family = Medium.detect( intake[0], 9 );
            let action = ( (custom||"") + family.toCamelCase() + "Intake" );
            let output = origin.signal( action, {detail:family, intake} );

            if ( (typeof output) !== "undefined" )
            { return output };

            // TODO !!
            // if ( Medium.detect(output,"func") && output.name.startsWith("filter") && Medium.detect(output.apply,"func") )
            // {
            //     if ( Script.envars.ScrumCallAutoApply )
            //     { return output.apply() };
            // };

            // output = origin.signal( (Script.facing.handle + action), intake );
            // return output;
        }
    // ------------------------------------------------------------------------------------------------------------------------
    // .parcel :: praxis : ?



    // .cloned :: detail : copy anything ?
    // ------------------------------------------------------------------------------------------------------------------------
        static cloned ( intake, copies=1, parent=undefined )
        {
            let detect = Medium.detect( intake, 9 );
            let output = [], entity, number = 0;

            while ( number < copies )
            {
                if (detect === "array")
                {
                    entity = [];
                    intake.scroll( function cloner ( object, number )
                    { entity[ number ] = Medium.cloned( intake[number], 1 )[0] });
                }
                else if (detect === "object")
                {
                    entity = {};
                    Medium.indice( intake ).scroll( function cloner ( aspect )
                    { entity[ aspect ] = Medium.cloned( intake[aspect], 1, entity )[0] });
                }
                else if (detect === "function")
                {
                    entity = Medium.create( intake.name, intake );
                    Medium.indice( intake, null, [] ).scroll( function cloner ( aspect )
                    {
                        entity[ aspect ] = Medium.cloned( intake[aspect], 1, entity )[0]
                    });
                };

                output.push( entity || intake );
                number++;
            };

            return output;
        }
    // ------------------------------------------------------------------------------------------------------------------------
    // .cloned :: praxis : ?
    };
// ----------------------------------------------------------------------------------------------------------------------------





// secure :: global : assets .. from here on these global aspects will be read-only and hidden (not enumerable)
// ----------------------------------------------------------------------------------------------------------------------------
    void Medium.secure( globalThis, "ABSENT,HOLDER,Null,Undefined,Medium" ); // references
    void Medium.secure( globalThis, "SELECT,CREATE,MODIFY,DELETE,EXISTS" );  // crud
    void Medium.secure( globalThis, "PLUGIN,CLIENT,SERVER,MASTER,WORKER" );  // facing
    void Medium.secure( globalThis, "STARTS,FINISH,SIGNAL" );  // system
    void Medium.secure( Symbol, "isProxy", Symbol("isProxy") );  // system
// ----------------------------------------------------------------------------------------------------------------------------





// global :: Script : ?
// ----------------------------------------------------------------------------------------------------------------------------
    void Medium.secure( globalThis, "Script", Medium.anneal( function Script ( intake )
    {

    },
    {
        config:
        {
            prefix: "../../",
            suffix: ".mjs",
        },


        recent: {},


        intake ( )
        {
            let intake = [ ...arguments ];

            if ( (intake.length === 1) && ((Array.isArray( intake[0] )) || (Medium.strain(intake[0]) === "Arguments")) )
            { return [...(intake[0])] };

            return intake;
        },



        differ (  )
        {
            let intake = Script.intake( arguments );
            let string = Script.texted( intake.shift(), true );
            let object;

            for ( object of intake )
            {
                if ( string !== Script.texted(object,true) )
                { return true };
            };

            return false;
        },



        render ( script, intake={}, assign )
        {
            let indice = Medium.indice( intake );
            let values = Medium.values( intake );
            let output = (Function( ...indice ,"return ( " + script + " )" )( ...values ));

            if ( !!assign )
            { Medium.assign( output, assign ) };

            return output;
            // WARNING :: tried and failed : ({[naming]:method})[naming]; .. returns "method" (variable name)
            // NOTICE :: history : https://stackoverflow.com/questions/5905492/dynamic-function-name-in-javascript
        },



        texted: Object.assign ( function texted ( intake, option )
        {
            let detect = Medium.detect( intake, 9 );
            let string = (("null,undefined").includes(detect) ? detect : (!intake.toString ? (intake+"") : intake.toString()));

            if ( Medium.stored.Primes.includes(detect) || (!option && (string !== "[object Object]")) )
            { return string }; // no option given to serialize, so this works for Function, RegExp, Symbol, etc.

            return Script.texted[detect]( intake, option );
        },
        {
            array ( intake, option )
            {
                let output = [], object;

                for ( object of intake )
                { output.push( Script.texted( object, option ) ) };

                output = ( "[" + output.join(",") + "]" );
                return output;
            },


            object ( intake, option )
            {
                let indice = Medium.indice( intake )
                let output = [], aspect, detail;
                let circle = Medium.select( intake, "@" );

                for ( aspect of indice )
                {
                    if ( circle.includes(aspect) )
                    { detail = "CIRCULAR" }
                    else
                    { detail = Script.texted( intake[aspect], option ) };

                    aspect = Script.texted( aspect );
                    output.push( aspect + ":" + detail );
                };

                output = ( "{" + output.join(",") + "}" );
                return output;
            },


            function ( intake, option )
            {
                let output = intake.toString();

                if ( !option )
                { return output };

                let assign = Script.texted.object( intake, option );

                if ( assign !== "{}" )
                { output = `Object.assign( (${output}), (${assign}) )` };

                return output;
            },


            class ( intake, option )
            {
                return Script.texted.function( intake, option )
            },
        }),



        hashed ( )
        {
            let string, output = [];

            [ ...arguments ].map( function onEach( object )
            {
                string = Script.texted(object, true);
                output.push( string );
            });

            return output.join("");
        },



        primed ( source, prefix, suffix )
        {
            let config = Script.config;

            source = (Script.facing[source] || source);
            prefix = ( prefix || config.prefix );
            suffix = ( suffix || config.suffix );

            if ( !source.startsWith("node:") )
            {
                if ( !source.startsWith("/") && !source.startsWith(".") && !source.startsWith(prefix) )
                { source = ( prefix + source ) };

                if ( !source.endsWith("/") && !source.endsWith(suffix) )
                { source = ( source + suffix ) };
            };

            return source;
        },


        async import( source, prefix, suffix )
        {
            source = Script.primed( source, prefix, suffix );

            return await import( source );
        },


        facing: function facing ()
        {
            let result = [], length, divide;
            let params = [...arguments];
            let format = (typeof params[0]);

            if (format === "number"){ length = params.shift() } else
            if (format === "string"){ divide = params.shift() };
            if (!params[0]){ params = Reflect.ownKeys(this) };

            params.map((symbol)=>{ if (!!this[symbol]){ result.push(this[symbol]) } });

            if (!!divide)
            { return result.join(divide).toLowerCase() };

            result = result.join("");
            return ((length < 0) ?  result.slice(length) : result.slice(0,length));
        }
        .bind((( $, appServer, childProc, appClient, appWorker  )=>
        {
            appServer = (((typeof process) === "object") && ((typeof process.versions.node) === "string")); // NodeJS only
            childProc = (appServer && (process.argv[2] === "child")); // WARNING :: this needs more reliable detection
            appClient = (((typeof globalThis.Window) === "function") && ((typeof window) === "object"));
            appWorker = (((typeof WorkerGlobalScope) !== "undefined") && (self instanceof WorkerGlobalScope));

            $[PLUGIN] = ((appClient && (!!window.chrome && !!chrome.runtime && !!chrome.runtime.id)) ? "Plugin" : "");
            $[CLIENT] = (appClient ? "Client" : "");
            $[SERVER] = (appServer ? "Server" : "");
            $[MASTER] = (((appClient && !appWorker) || (appServer && !childProc)) ? "Master" : "");
            $[WORKER] = (!$[MASTER] ? "Worker" : "");
            return $
        })({}))
    }));


    (( facing )=>
    {
        Medium.anneal( facing,
        {
            global: facing(6),   // e.g:  Server  ..  Client  ..  Plugin
            mantle: facing(-6),  // e.g:  Master  ..  Worker
            handle: facing(),    // e.g:  ServerMaster  ..  ClientWorker  ..  PluginMaster

            lobase: facing(6).toLowerCase(),  // e.g:  server  ..  client  ..  plugin
            lorole: facing(-6).toLowerCase(), // e.g:  master  ..  worker
        });

        Medium.anneal( facing,
        {
            access: ( facing.global + "/access/" + facing.lobase + ".access." + facing.lorole ),
            facing: ( facing.global + "/facing/" + facing.lobase + ".facing." + facing.lobase ),
            device: ( "Device/facing/device.facing." + facing.lobase ),

            server: ( "Server/facing/server.facing." + facing.lobase ),
            client: ( "Client/facing/client.facing." + facing.lobase ),
            plugin: ( "Plugin/facing/plugin.facing." + facing.lobase ),
        });
    })
    ( Script.facing );
// ----------------------------------------------------------------------------------------------------------------------------





// global :: Expect : flow control helper mechanism
// ----------------------------------------------------------------------------------------------------------------------------
    void Medium.secure( globalThis, "Expect", Medium.reform
    (
        function Expect ( )
        {
            let manage = Medium.routed( Expect, arguments );

            return manage;
        },
        {
            detect ( expect, reason )
            {
                let detect = (this[SYSTEM].intake[1] || Medium.detect( this[SYSTEM].target ));

                if ( expect.includes(detect) )
                { return true };

                reason = ( !reason ? (" - but " + detect + " given" ) : (" - "+(reason+"").trim()) );

                moan( "expecting : " + expect + reason + "\n", Medium.caller("*"));
                return false;
            },


            strain ( expect, reason )
            {
                if ( expect.includes( Medium.strain( this[SYSTEM].target ) ) )
                { return true };

                reason = ( !reason ? "" : (reason+"").trim() );

                moan( "expecting : " + expect + " - " + reason );
                return false;
            },
        }
    ));
// ----------------------------------------------------------------------------------------------------------------------------





// global :: Signal : reference
// ----------------------------------------------------------------------------------------------------------------------------
    void Medium.secure( globalThis, function Signal ( action, detail )
    {
        let signal = this;
        let called = !( this instanceof Signal );

        if ( called )
        { return Global.signal(action,detail) };

        if ( !Medium.detect(detail,"object") || !Medium.indice(detail,"detail") )
        { detail = {detail:detail} };

        Medium.supply( detail,
        {
            action: ( detail.action || action ),
            halted: ( detail.halted ? true : false ),

            cancel ()
            {
                this.halted = true;
                return this;
            },

            convey ( sensor, string, detail )
            {
                if ( detail !== undefined )
                { this.detail = detail };

                return sensor.signal( string, this )
            },
        });

        Object.assign( signal, detail );
        return signal;
    });
// ----------------------------------------------------------------------------------------------------------------------------





// global :: Listen : events
// ----------------------------------------------------------------------------------------------------------------------------
    void Medium.secure( globalThis, Medium.anneal
    (
        function Listen ( target, events )
        {
            Medium.supply( target, Medium.cloned(Listen)[0] );
            Medium.supply( target.listen, {marked:[]} );

            if ( (typeof events) === "function" )
            {
                events = { [ (Medium.naming( events )) ]: events };
            };

            if ( !Medium.detect(events,"obje") )
            { return target };

            Medium.indice( events ).map(( action )=>
            {
                void target.listen( action, events[action] );
            });

            return target;
        },
        {
            events: (new (function events(){})),



            ignore ( action, callee )
            {
                let events = ( this.events[action] || [] );
                let method, number;

                for ( number=0; number<events.length; number++ )
                {
                    method = events[ number ];

                    if ( method === callee )
                    {
                        this.events[action].splice(number,1);
                        this.events[action] = [ ...(this.events[action]) ];
                        return this;
                    };
                };

                return this;
            },



            listen ( intake, callee )
            {
                let detect = { intake:(typeof intake), callee:(typeof callee) };
                let indice, action;

                // if ( (detect.intake === "string") && (detect.callee === "function") ) // old
                if ( ("string,symbol").includes( detect.intake ) && (detect.callee === "function") )
                {
                    intake = { [intake]:callee };
                }
                else if ( detect.intake === "function" )
                {
                    if ( !Expect(intake.name).detect("string","as function name") )
                    { return }; // named function required

                    intake = { [intake.name]:intake };
                }
                else if ( detect.intake !== "object" )
                {
                    moan("invalid instructions");
                    return;
                };


                indice = Medium.indice( intake );

                for ( action of indice )
                {
                    if ( ((typeof action) === "string") && action.includes(String.stored.marker) )
                    { this.listen.marked.push( action ) };

                    if ( !this.events[action] )
                    { this.events[ action ] = [] };

                    this.events[ action ].push( intake[action] );
                };


                return this;
            },



            signal ( action, detail )
            {
                let dejavu = ( action instanceof Signal );

                if ( dejavu )
                {
                    detail = action;
                    action = detail.action;
                };

                if ( !Expect(action).detect("stri,symb") )
                { return };

                let marked = ( this.events[ ([action].locate(...(this.listen.marked))[0]) ] || [] );
                let events = [ ...marked, ...(this.events[action]||[]), ...(this.events[SIGNAL]||[]) ];
                let signal = ( !dejavu ? (new Signal(action, detail)) : detail );
                let number = events.length, output;

                while ( number > 0 )
                {
                    number--; // last listener first
                    output = events[ number ].apply( this, [ signal, action ] );

                    if ( (typeof output) !== "undefined" )
                    { return output }; // stop signalling if accepted

                    if ( signal.halted )
                    {
                        signal.output = output;
                        signal.origin = this;
                        return signal; // stop signalling if halted
                    };
                };

                return; // undefined
            },


            // convey ( design )
            // {
            //     let aspect, indice = Medium.indice( design );
            //
            //     for ( aspect of indice )
            //     {
            //         this.listen(  ){}
            //     };
            // },
        }
    ));
// ----------------------------------------------------------------------------------------------------------------------------





// global :: Evolve : modify prototype
// ----------------------------------------------------------------------------------------------------------------------------
    void Medium.secure( globalThis, Medium.anneal
    (
        function Evolve ( )
        {
            let output = Medium.routed( Evolve, {},  ...arguments );
            return output;
        },
        {
            secure ( design )
            {
                this[SYSTEM].intake.map( function eachAspect ( target )
                {
                    Medium.secure( Medium.origin(target), design );
                });
            },
        }
    ));
// ----------------------------------------------------------------------------------------------------------------------------





// extend :: String : stored
// ----------------------------------------------------------------------------------------------------------------------------
    Medium.secure ( String, "stored", { joiner:'⨝', marker:'𐘥' } );
// ----------------------------------------------------------------------------------------------------------------------------





// evolve :: String.toCamelCase : shim
// ----------------------------------------------------------------------------------------------------------------------------
    Evolve ( String ).secure
    ({
        toCamelCase ( intake )
        {
            intake = [...arguments];
            intake[0] = (intake[0] || " ");
            intake[1] = (intake[1] || "-");

            let source = (this+"").toLowerCase();
            let joiner = String.stored.joiner;
            let output = [];

            while ( intake.length > 0 )
            {
                let slicer = intake.shift();
                source = source.split(slicer).join(joiner);
            };

            source.split(joiner).map((string)=>
            {
                string = string.trim();
                if ( !string ){ return };
                 output.push ( (string[0].toUpperCase() + string.slice(1)) );
            });

            return output.join("");
        },


        toCamelBack ( )
        {
            let output = (this+"").toCamelCase();
            return ( output[0].toLowerCase() + output.slice(1) );
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------





// evolve :: Array.scroll : shim
// ----------------------------------------------------------------------------------------------------------------------------
    Evolve( Array ).secure
    ({
        scroll ( callee )
        {
            let number, object, result, method;

            for ( number=0;  number < this.length;  number++ )
            {
                object = this[ number ];
                result = callee.apply(this,[object,number]);

                if ( result === FINISH )
                { return this };

                if ( (typeof result) !== "undefined" )
                { return result };
            };

            return this;
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------





// evolve :: Array.locate : shim that finds things as one would expect
// ----------------------------------------------------------------------------------------------------------------------------
    Evolve ( Array ).secure
    ({
        locate (  )
        {
            let search = Script.intake( arguments );
            let values = Medium.values( this ), object;
            let output = [], vIndex, sIndex, string;
            let locate = "", exists, marked, parted;
            let marker = String.stored.marker;

            for ( vIndex in values )
            {
                exists = Script.texted( values[vIndex], true );

                for ( sIndex in search )
                {
                    locate = Script.texted( search[sIndex], true );

                    if ( !locate.includes(marker) )
                    {
                        if ( exists === locate ){ output.push( vIndex ) };
                        continue;
                    };

                    parted = locate.split( marker );

                    if ( locate.startsWith(marker) && locate.endsWith(marker) ) // includes .. e.g: 𐘥foo𐘥
                    {
                        string = Script.unwrap( locate );
                        if ( exists.includes(string) ){ output.push( locate ) };
                        continue;
                    };

                    if ( locate.endsWith(marker) ) // startsWith(string) .. e.g: foo𐘥
                    {
                        string = parted[0];
                        if ( exists.startsWith(string) ){ output.push( locate ) };
                        continue;
                    };

                    if ( locate.startsWith(marker) ) // endsWith(string) .. e.g: 𐘥foo
                    {
                        string = parted[1];
                        if ( exists.endsWith(string) ){ output.push( locate ) };
                        continue;
                    };

                    string = parted.splice( 1,1 )[0];

                    if ( (string === "") && (parted.length === 2) )
                    {
                        if ( exists.startsWith(parted[0]) && exists.endsWith(parted[1]) )
                        { output.push( locate ) }
                    };
                };
            };

            return output;
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------





// evolve :: Array.count : shim that returns number of items found
// ----------------------------------------------------------------------------------------------------------------------------
    Evolve ( Array ).secure
    ({
        count (  )
        {
            let search = Script.intake( ...arguments );
            let values = Medium.values( this ), object;
            let output = 0;

            for ( object of values )
            {
                if ( search.locate( object ).length < 1 )
                { output++ };
            };

            return output;
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------





// evolve :: Function.assign : shim
// ----------------------------------------------------------------------------------------------------------------------------
    Evolve( Function ).secure
    ({
        assign ( design )
        {
            return Medium.assign( this, ...arguments );
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------





// global :: Global : reference
// ----------------------------------------------------------------------------------------------------------------------------
    void Medium.secure( globalThis, "Global", new (class Global extends Medium
    {
        constructor ( )
        {
            super
            (
                function Global( intake )
                {
                    let Global = globalThis.Global;
                    let detect = Medium.detect( intake );

                    if ( ("stri,symb,arra").includes(detect) )
                    { return Medium.access( Global.entity, intake ) };

                    if ( detect === "obje" )
                    {
                        Medium.anneal( Global.entity, intake );
                        return Global;
                    };

                    if ( ("func,clas").includes(detect) && !!intake.name )
                    {
                        Medium.secure( Global.entity, intake.name, intake, "E" );
                        return Global;
                    };
                },

                globalThis
            );


            Medium.secure ( this[SYSTEM].router,
            {
                construct ( target, intake )
                {
                    let detect = ([ Medium.detect( intake[0] ), Medium.detect( intake[1] ) ]).join(",");
                    let length = intake.length;
                    let naming = Medium.naming( intake[0] );

                    naming = ( (naming === "Object") ? "" : naming );
                    intake = ( (intake.length > 1) ? Medium.anneal( ...intake ) : intake[0] );

                    if ( !naming )
                    {
                        Medium.secure( globalThis, intake );
                        return this;
                    };

                    Medium.secure( globalThis, naming, intake );
                    return this;
                },
            });


            return this;
        }
    }));
// ----------------------------------------------------------------------------------------------------------------------------
// Global.listen
// ({
//     [ SIGNAL ]: function sing ( signal )
//     {
//         if ( signal.action.startsWith("ClientMaster") || (signal.action === "MediumAwaits") ){ return };
//         dump( signal );
//     }
// });
// ----------------------------------------------------------------------------------------------------------------------------





// global :: Create : ?
// ----------------------------------------------------------------------------------------------------------------------------
    void new Global ( function Create ( )
    {
        return Medium.herald( Create, arguments );
    });

    Listen( Create );
// ----------------------------------------------------------------------------------------------------------------------------





// listen :: Create : Global
// ----------------------------------------------------------------------------------------------------------------------------
    Create.listen( "FunctionIntake", function createGlobal( signal )
    {
        if ( Medium.naming(signal.intake[0]) !== "Global" )
        { return };

        let output = (new (function createNewGlobal(){}));

        Object.assign( output, {using ( )
        {
            let intake = [ ...arguments ];
            let detail = intake.shift();
            let naming = Medium.naming( detail );

            if ( Medium.indice(globalThis).includes( naming ) )
            {
                moan(`Global.${naming} - is already defined`);
                return false;
            };

            Medium.anneal( detail, intake );
            Medium.secure( globalThis, naming, detail );

            if ( (typeof detail.signal) === "function" )
            {
                detail.signal("GlobalExists", naming)
            };
        }});

        return output;
    });
// ----------------------------------------------------------------------------------------------------------------------------





// global :: Modify : ?
// ----------------------------------------------------------------------------------------------------------------------------
    void new Global ( function Modify ( )
    {
        return Medium.herald( Modify, arguments );
    },
    {
        commit ( )
        {
            return this[SYSTEM].target;
        },



        origin ( intake )
        {
            Medium.origin( this[SYSTEM].target, intake );
            return this;
        },



        assign ()
        {
            [ ...arguments ].map(( object )=>
            {
                Object.assign( this[SYSTEM].target, object );
            });

            return this;
        },



        secure ()
        {
            [ ...arguments ].map(( object )=>
            {
                Medium.secure( this[SYSTEM].target, object );
            });

            return this;
        },



        anneal ()
        {
            [ ...arguments ].map(( object )=>
            {
                Medium.anneal( this[SYSTEM].target, object );
            });

            return this;
        },



        supply ( supply, levels=999, change=[undefined,null] )
        {
            let result = this[SYSTEM].target;

            void Medium.supply( result, supply, levels, change );

            return this;
        },



        remove ( aspect, retain=false )
        {
            if ( !retain )
            {
                Medium.access( this[SYSTEM].target, aspect, DELETE );
                return this;
            };

            return Medium.access( this[SYSTEM].target, aspect, DELETE );
        },



        vacuum ( aspect, retain=false )
        {
            let target = this[SYSTEM].target;
            let output = {};

            if ( aspect !== undefined )
            { target = Medium.access( target, aspect ) };

            Medium.indice( target ).map(( aspect )=>
            {
                output[aspect] = target[ aspect ];
                delete target[ aspect ];
                number++;
            });

            if ( retain )
            { return output };

            return this;
        },
    });

    Listen( Modify );
// ----------------------------------------------------------------------------------------------------------------------------





// listen :: Modify : ?
// ----------------------------------------------------------------------------------------------------------------------------
    Modify.listen
    ({
        ObjectIntake ( signal )
        {
            let manage = Medium.routed( this, signal.intake );
            return manage;
        },


        FunctionIntake ( signal )
        {
            let manage = Medium.routed( this, signal.intake );
            return manage;
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------





// global :: Select : ?
// ----------------------------------------------------------------------------------------------------------------------------
    void new Global ( function Select ( )
    {
        return Medium.herald( Select, arguments );
    });

    Listen( Select );
// ----------------------------------------------------------------------------------------------------------------------------





// global :: Insert : ?
// ----------------------------------------------------------------------------------------------------------------------------
    void new Global ( function Insert ( )
    {
        return Medium.herald( Insert, arguments );
    });

    Listen( Insert );
// ----------------------------------------------------------------------------------------------------------------------------





// global :: Update : ?
// ----------------------------------------------------------------------------------------------------------------------------
    void new Global ( function Update ( )
    {
        return Medium.herald( Update, arguments );
    });

    Listen( Update );
// ----------------------------------------------------------------------------------------------------------------------------





// global :: Render : ?
// ----------------------------------------------------------------------------------------------------------------------------
    void new Global ( function Render ( )
    {
        return Medium.herald( Render, arguments );
    });

    Listen( Render );
// ----------------------------------------------------------------------------------------------------------------------------





// global :: Remove : ?
// ----------------------------------------------------------------------------------------------------------------------------
    void new Global ( function Remove ( )
    {
        return Medium.herald( Remove, arguments );
    });

    Listen( Remove );
// ----------------------------------------------------------------------------------------------------------------------------







// global :: Pledge : promise
// ----------------------------------------------------------------------------------------------------------------------------
    void new Global ( function Pledge ( method )
    {
        if ( !this ) // called without `new`
        { return (new (class Pledge { then = method })) };

        let result = (class Pledge extends Promise { });
        result = new result(method);
        return result;
    });
// ----------------------------------------------------------------------------------------------------------------------------





// evolve :: String.locate : shim
// ----------------------------------------------------------------------------------------------------------------------------
    Evolve ( String ).secure
    ({
        locate( intake, symbol=EXISTS )
        {
            let detect = (typeof intake);
            let string = (this+""), sample, output;

            if ( detect === "symbol" )
            {
                intake = arguments[1];
                symbol = arguments[0];
                detect = (typeof intake);
            };

            if ( detect === "string" )
            { intake = [intake] };

            if ( !Expect(intake).detect("array") || !Expect(symbol).detect("symbol") )
            { return };

            for ( sample of intake )
            {
                switch ( symbol )
                {
                    case EXISTS : output = (string.indexOf(sample) > -1); break;
                    case STARTS : output = string.startsWith(sample); break;
                    case FINISH : output = string.endsWith(sample); break;
                };

                if ( output )
                { return sample };
            };
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------





// evolve :: Array.shuffle : shim
// ----------------------------------------------------------------------------------------------------------------------------
    Evolve ( Array ).secure
    ({
        shuffle ( )
        {
            for (let i = this.length - 1; i > 0; i--)
            {
                const j = Math.floor(Math.random() * (i + 1));
                [this[i], this[j]] = [this[j], this[i]];
            };

            return this;
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------





// extend :: Number.ranged : method
// ----------------------------------------------------------------------------------------------------------------------------
    Modify( Number ).secure
    ({
        ranged ()
        {
            let ranges = [...arguments];
            let result = [];

            if ((typeof ranges[0]) === "number")
            { ranges = [ranges] }; // ease of use

            ranges.map((ranger)=>
            {
                let starts = ranger[0];
                let finish = ranger[1];
                let grower = (ranger[2] || 1);
                let number = starts;

                while (number <= finish)
                {
                    result.push(number);
                    number += grower;
                };
            });

            return result;
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------





// extend :: Number : aspects
// ----------------------------------------------------------------------------------------------------------------------------
    Modify( Number ).secure
    ({
        stored:
        {
            Base62: String.fromCharCode( ...(Number.ranged([48,57], [65,90], [97,122])) ),
        },



        random (finish=999, starts=0)
        {
            return Math.floor(Math.random() * (starts - finish + 1) + finish)
        },



        timing (  )
        {
            let tmilli = (new Date()).getTime();
            let maxmil = (((60 * 60) * 24) * 1000); // 1 day of millisec
            let tmicro = (((performance.now() + "").split(".").shift() * 1) % maxmil); // client + server compliant

            return  (tmilli + tmicro);
        },



        unique: function unique(  )
        {
            this.seed++;
            return (Number.timing() + this.seed);
        }
        .bind({ seed:0 }),
    });
// ----------------------------------------------------------------------------------------------------------------------------





// extend :: Number : base62
// ----------------------------------------------------------------------------------------------------------------------------
    Modify ( Number ).secure
    ({
        enBase62 ( intake )
        {
            let source = Number.stored.Base62;
            let result = "";

            if (intake === 0)
            { return source[0] }

            while (intake > 0)
            {
                result = (source[intake % 62] + result);
                intake = Math.floor(intake / 62);
            }

            return result;
        },



        deBase62 ( intake )
        {
            let source = Number.stored.Base62;
            let result = 0;
            let length = intake.length,
                string, number;

            for (number = 0; number < length; number++)
            {
                string = intake.charCodeAt(number);
                string -= ((string < 58) ? 48 : ((string < 91) ? 29 : 87));
                result += (string * Math.pow(62, length - number - 1));
            };

            return result;
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------





// extend :: Number.toBase : shim
// ----------------------------------------------------------------------------------------------------------------------------
    Modify( Number ).secure
    ({
        toBase ( intake )
        {
            let number = ( this * 1 );
            let naming = ( "enBase" + intake );

            if ( !!Number[ naming ] )
            { return Number[ naming ]( number ) };

            return number.toString( intake );
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------





// extend :: String : methods
// ----------------------------------------------------------------------------------------------------------------------------
    Modify ( String ).secure
    ({
        unwrap ( string, casing, starts=1, finish=-1,  )
        {
            if ( !casing )
            { return string.slice( starts, finish ) };

            starts = string.slice( 0, starts );
            finish = string.slice( finish, string.length );

            return [ starts, finish ];
        },



        random ( length=32 )
        {
            let output = "";

            while (output.length < length)
            { output += (Number.stored.Base62).split("").shuffle().join("") };

            return output.slice(0,length);
        },



        unique ( length )
        {
            let output = Number.unique().toBase(62);

            if ( (typeof length) !== "number" )
            { return output };

            while (output.length < length)
            { output += (Number.stored.Base62).split("").shuffle().join("") };

            return output.slice(0,length);
        },



        parsed ( string, indice, values )
        {
            string = (string+"").trim(); // now a safer copy

            if (string.endsWith(";") || string.endsWith(",")){ string = string.slice(0,-1).trim() }; // cleaned up a bit
            if ((string.length < 1) || (string === "undefined")){ return }; // .. `undefined` if as such, or invalid

            let skills = Medium.indice(String.parser);
            let method, output;

            for (method of skills)
            {
                output = Script[ method ]( string, indice, values );
                if ( (typeof output) !== "undefined" )
                { return output };
            };

            return output;
        },


        parser:
        {
            string ( string )
            {
                let quoted = ['""',"''",'``'];
                let padded = (string.slice(0,1) + string.slice(-1)); // .. combined the first and last characters

                if (quoted.includes(string)){ return "" }; // empty string
                try { result = JSON.parse(string);  return result }catch( failed ){ }; // works for most things, incl. null

                if ((quoted.includes(padded) && (padded !== '``')) || ((padded === '``') && !string.includes("${")) )
                { return string.slice(1,-1) }; // plain string
            },



            switch ( string )
            {
                let locase = string.toLowerCase();

                if ( ([ "true", "yes", "on", "in" ]).includes ( locase ) )
                { return true };

                if ( ([ "false", "no", "off", "out" ]).includes ( locase ) )
                { return false };
            },



            reckon ( string, indice, values )
            {
                let padded = (string.slice(0,1) + string.slice(-1)); // .. combined the first and last characters

                if ( Medium.detect(indice,"obje,func") )
                {
                    values = Medium.values( indice );
                    indice = Medium.indice( indice );
                };

                while ( (padded === "()") )
                {
                    string = string.slice(1,-1);
                    string = string.trim();
                    padded = (string.slice(0,1) + string.slice(-1));
                };

                let design = string.split(" ")[0];
                let isfunc = ((padded === "(}") || ("function,class".includes(design) && string.endsWith("}")));

                if ( !string.startsWith("try") )
                { string = ( "try { return (" + string + ") } catch ( failed ) { moan(failed) };" ) };

                let output = Function( ...indice, string )( ...values );

                return output;
            },
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------





// global :: Device : ?
// ----------------------------------------------------------------------------------------------------------------------------
    void new Global ( async function Device( schema )
    {
        if ( !Expect(schema).detect("obje") )
        { return };

        let indice = Medium.indice( schema );
        let aspect, number=0;

        while ( indice.length > 0 )
        {
            let aspect = indice.shift();
            let source = schema[ aspect ];

            if ( !!Device[ aspect ] )
            {
                moan( "Device :: already loaded driver: " + aspect );
                continue;
            };

            let target = await Script.import( source );
            let driver = new Medium( target );

            driver.system.source = source;

            Medium.secure( Device, aspect, driver );
            Device.signal( "driverLoaded", aspect );
        };

        return Device;
    });

    Listen( Device );

    Medium.secure( Device, {[SYSTEM]: (new (function Device(){}))} );
// ----------------------------------------------------------------------------------------------------------------------------





// global :: Timing :
// ----------------------------------------------------------------------------------------------------------------------------
    void new Global ( function Timing ( )
    {
        let output = Medium.routed( Timing, arguments );
        output[SYSTEM].source = null;

        return output;
    },
    {
        Number:
        {
            cancel ()
            {
                clearTimeout( this[SYSTEM].source );
            },

            detain ( method )
            {
                this[SYSTEM].source = setTimeout( method.bind(this), this[SYSTEM].target );
                return this;
            },

            replay ( method )
            {
                this[SYSTEM].source = setInterval( method.bind(this), this[SYSTEM].target );
                return this;
            },
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------





// listen :: Modify :
// ----------------------------------------------------------------------------------------------------------------------------
    // Timing.listen
    // ({
    //     NumberIntake ( signal )
    //     {
    //         let manage = Medium.routed( this, signal.intake );
    //         return manage;
    //     },
    // });
// ----------------------------------------------------------------------------------------------------------------------------





// global :: Filter : TODO !!
// ----------------------------------------------------------------------------------------------------------------------------
    void new Global ( function Filter ( target )
    {
        let output = Medium.routed( Filter, target );
        output[SYSTEM].filter = {};

        return output;
    },
    {
        apply ( ){},


        fetch ( )
        {
            let intake = Script.intake( arguments );
            let detect = Medium.detect( intake[0] );
            let length = intake.length;
            let filter = this;

            if ( length < 1 )
            { return filter }

            filter.fetch = {};

            if ( detect === "obje" )
            {
                Object.assign( filter.fetch, intake[0] );
            }
            else if ( detect === "stri"  )
            {
                intake.map(( aspect )=>
                { filter.fetch[aspect] = aspect });
            };

            return filter;
        },

        where ( ){},
        group ( ){},
        limit ( ){},
    });
// ----------------------------------------------------------------------------------------------------------------------------





// import :: script : platform facing support library
// ----------------------------------------------------------------------------------------------------------------------------
    !(async function importFacingModule ()
    {
        await Script.import( "facing" );
    })();
// ----------------------------------------------------------------------------------------------------------------------------



/*
*/
