


// readme :: info
// ----------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------




// refine :: dump + moan : for server-side
// ----------------------------------------------------------------------------------------------------------------------------
    Medium.secure
    (
        globalThis,
        {
            dump()
            {
                let strace = Medium.caller("*");
                let called = strace[ 1 ];
                let detail = `NOTICE :: dump :: ${called.from} : ${called.file} : ${called.line}`;
                let lining = Medium.cloned( "-", detail.length );
                let finish = Medium.cloned( "=", detail.length);

                console.log( `\n${detail}\n${lining}\n`, ...arguments, `\n${finish}\n\n` );
            },


            moan()
            {
                let strace = Medium.caller("*");
                let called = strace[ ((strace[1].from === "Expect.detect") ? 3 : 1) ];
                let detail = `FAILED :: moan :: ${called.from} : ${called.file} : ${called.line}`;
                let lining = Medium.cloned( "-", detail.length );
                let finish = Medium.cloned( "=", detail.length);
                let header = ['\x1b[31m', `\n${detail}\n${lining}\n`];
                let footer = [`\n${lining}`, '\x1b[0m', "\n"];

                console.error( ...header, ...arguments, ...footer, strace, `\n${finish}\n\n` );
            },
        }
    );
// ----------------------------------------------------------------------------------------------------------------------------




// depend :: device : server
// ----------------------------------------------------------------------------------------------------------------------------
    !(async function extendDeviceAccess ()
    {
        await Device
        ({
            disk: "node:fs",
            http: "node:http",
            host: "node:os",
            proc: "node:process",
            bash: "node:child_process",
            repl: "node:repl",
        });

        await Script.import( "client" );

        Timing( 0 ).detain( ()=>
        {
            Global.signal("deviceLoaded");
        });

    })();
// ----------------------------------------------------------------------------------------------------------------------------




// listen :: deviceLoaded : supply config to Device.disk
// ----------------------------------------------------------------------------------------------------------------------------
    void Global.listen( "deviceLoaded", function extendDeviceConfig()
    {
        Modify( Device.disk.system ).supply
        ({
            config:
            {
                intake:
                {
                    string:
                    {
                        relate: [ "/", "./", "../", "~/", "$" ],
                        prefix: "../../",
                        suffix: "",
                    },
                },
            }
        });
    });
// ----------------------------------------------------------------------------------------------------------------------------




// listen :: deviceLoaded : extend Device.disk.select
// ----------------------------------------------------------------------------------------------------------------------------
    void Global.listen("deviceLoaded", function extendDeviceSelect()
    {
        Modify( Device.disk.system ).anneal
        ({
            accept ( intake )
            {
                let relate = this.config.intake.string.relate;
                let output = ( intake.locate(relate,STARTS) ? intake : false );

                if ( !!output && output.startsWith("~/") )
                { output = ( process.env.HOME + output.slice(1) ) };

                return output;
            },


            select: Medium.create
            (
                function select ( intake, callee )
                {
                    return this.system.select[ (!callee ? "insync" : "nosync") ]( this.system.accept(intake) );
                },
                {
                    filter:
                    {
                        apply ( intake )
                        {
                            let detect = Medium.detect(intake);

                            if ( !intake || (detect === "stri") )
                            {
                                intake = (((intake||"*") === "*") ? "data" : intake);
                                return this.result[ intake ];
                            };

                            if ( detect !== "obje" )
                            { return this.result };

                            Object.keys( intake ).map(( aspect )=>
                            {
                                this[aspect]( intake[aspect] );
                            });

                            return this.result;
                        },


                        fetch ( intake, length )
                        {
                            let detect = Medium.detect( this.result.data );
                            let format = Medium.detect( intake );
                            let result = ( (detect === "stri") ? "" : ((format === "obje") ? {} : []) );
                                length = ( length || this.result.length );

                            if ( (format === "unde") || (intake === "*") )
                            { return this };

                            if ( format === "numb" )
                            {
                                this.result.data = this.result.data.slice(intake,length);
                                return this;
                            };

                            if ( (format === "stri") )
                            {
                                intake = intake.split(",");
                                format = "arra";
                            };

                            if ( (format === "arra") )
                            {
                                Object.keys( this.result ).map(( aspect )=>
                                {
                                    if ( !intake.includes(aspect) )
                                    { delete this.result[aspect] };
                                });
                            };

                            return this;
                        },


                        where ()
                        {
                            return this;
                        },


                        group()
                        {
                            return this;
                        },


                        limit()
                        {
                            return this;
                        },
                    },



                    insync ( target ) // sync
                    {
                        let output = Object.assign( (function filterSelect(){}), this.filter );
                        let naming = ( target.endsWith("/") ? target.slice(0,-1) : target ).split("/").pop();
                        let result = { name:naming, type:"?", path:target, size:0, data:null };

                        if ( !(Device.disk.existsSync( target )) )
                        {
                            moan( target + " - does not exist" );
                            output.result = result;
                            return output;
                        };

                        let status = Device.disk.lstatSync( target );

                        if ( status.isFile() )
                        {
                            result.type = "file";
                            result.data = Device.disk.readFileSync( target, "utf8" );
                            result.size = result.data.length;
                        }
                        else if ( status.isDirectory() )
                        {
                            result.type = "fold";
                            result.data = Device.disk.readdirSync( target );
                            result.size = result.data.length;
                        }
                        else if ( status.isSymbolicLink() )
                        {
                            result.type = "link";
                            result.data = Device.disk.readlinkSync( target );
                            result.size = result.data.length;
                        };

                        output.result = result;
                        return output;
                    },

                    nosync () // async
                    {
                        moan("TODO :: implement : Device.disk.select.nosync");
                    },
                }
            ),
        });
    });
// ----------------------------------------------------------------------------------------------------------------------------




// listen :: deviceLoaded : listen on Select `StringIntake` event for calls to Device.disk.select
// ----------------------------------------------------------------------------------------------------------------------------
    void Global.listen( "deviceLoaded", function conveySelectIntake()
    {
        Select.listen( "StringIntake", function selectDiskByString ( signal )
        {
            if ( !Device.disk.accept(signal.detail) )
            { return }; // not interested

            return Device.disk.select( signal.detail );
        });
    })
// ----------------------------------------------------------------------------------------------------------------------------




// listen :: deviceLoaded : modify Script cli intake params
// ----------------------------------------------------------------------------------------------------------------------------
    void Global.listen( "deviceLoaded", function modifyScriptParams()
    {
        Timing( 0 ).detain( ()=>
        {
            let params = [ ...(process.argv) ];
            let medium = params.shift();
            let string = params.shift();
            let parent = string.split("/"); parent.pop();
                parent = parent.join("/");
            let origin = ( string.endsWith("/aarden") ? string : parent );
            let envars = JSON.parse(Device.disk.readFileSync((origin+"/dist/.aard.env"),"utf8"));

            Modify( process.env ).assign( envars );

            Modify( Script ).anneal
            ({
                envars: new Medium( envars, process.env ),
                medium, origin, params
            });

            Modify( Device.host.system ).anneal
            ({
                osname: ( Device.host.release().split("-").pop().toCamelCase() + Device.host.platform().toCamelCase() )
            });

dump( ">>>>", Script.envars.HOME );

            Global.signal( "systemLoaded" );
        });
    })
// ----------------------------------------------------------------------------------------------------------------------------



/*
*/
