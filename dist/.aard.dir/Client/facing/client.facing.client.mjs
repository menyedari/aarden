


// readme :: info
// ----------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------





// global :: Client : combined window and navigator
// ----------------------------------------------------------------------------------------------------------------------------
    Create( Global ).using
    (
        new (class Client extends Medium
        {
            constructor ()
            {
                super( window, navigator );
            }
        })
    );
// ----------------------------------------------------------------------------------------------------------------------------





// global :: Canvas : combined document and window.screen
// ----------------------------------------------------------------------------------------------------------------------------
    Create( Global ).using
    (
        new (class Canvas extends Medium
        {
            constructor ()
            {
                super( document, window.screen );

                this.config
                ({
                    renderTarget: "#renderTarget",
                    heraldedKeys: "render,create,select,change,remove,insert,modify,update",
                });

                this.relate( "get", this.config.heraldedKeys, "entity.herald" );
            }
        }),
        {
            define ( intake, method )
            {
                method = (method||this.htNode.invoke);  let output;
                output = Medium.herald( this, [intake,method], "Define" );

                if ( (typeof output) === "undefined" )
                { output = this };

                return output;
            },


            herald ( )
            {
                return Medium.herald( Canvas, arguments, this.name.toCamelCase() );
            },


            isNode ( intake )
            {
                let strain = Medium.strain( intake );
                return ( strain.startsWith("HTML") && strain.endsWith("Element") );
            },


            htNode:
            {
                invoke: function ( )
                {
                    return Canvas.create( this.name, [...arguments] )
                },



                define:
                {
                    padded: ["ᐸ","ᐳ"],
                    htmTag: ("a,abbr,address,area,article,aside,audio,b,base,bdi,bdo,blockquote,body,br,button,canvas,"+
                            "caption,cite,code,col,colgroup,data,datalist,dd,del,details,dfn,dialog,div,dl,dt,em,embed,"+
                            "fieldset,figcaption,figure,footer,form,h1,h2,h3,h4,h5,h6,head,header,hgroup,hr,html,i,"+
                            "iframe,img,input,ins,kbd,label,legend,li,link,main,map,mark,menu,meta,meter,nav,noscript,"+
                            "object,ol,optgroup,option,output,p,picture,pre,progress,q,rp,rt,ruby,s,samp,script,search,"+
                            "section,select,slot,small,source,span,strong,style,sub,summary,sup,table,tbody,td,template,"+
                            "textarea,tfoot,th,thead,time,title,tr,track,u,ul,var,video,wbr").split(","),
                },



                change:
                {
                    class ( entity, detail )
                    {
                        entity.className = detail;
                        return entity;
                    },
                },
            }
        }
    );
// ----------------------------------------------------------------------------------------------------------------------------





// listen :: Canvas : MediumAwaits - define html tags as functions .. use like: ᐸdivᐳ({id:"foo"}) .. note the `ᐸᐳ` is unicode
// ----------------------------------------------------------------------------------------------------------------------------
    Canvas.listen
    ({
        MediumAwaits ( signal )
        {
            Medium.secure( Modify, {change( intake )
            {
                if ( !intake ){ return };  // undefined -or invalid
                if ( !Expect(intake).detect("object") ){ return }; // invalid

                let system = Medium.indice( this, "$" )[0];
                let config = Canvas.htNode.change;
                let target = this[system].target;
                let indice = Object.keys( intake );

                indice.scroll( function changeAttribute( naming )
                {
                    if ( (typeof intake[naming]) === "function" )
                    { detail(target,intake[naming]); return }; // config ovverride
                    target.setAttribute( naming, intake[naming] );
                })

                return target;
            }});


            Timing( 0 ).detain( function awaitsListen()
            {
                this.entity.htNode.define.htmTag.scroll( function onEach( htmTag )
                {
                    this.define( htmTag );
                }.bind(this.medium));

                this.signal("MediumFinish");

            }.bind(this));
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------





// listen :: Select : StringIntake .. convey global Select(string) to Canvas.select - which emits: `SelectStringIntake`
// ----------------------------------------------------------------------------------------------------------------------------
    Select.listen
    ({
        StringIntake ( signal )
        {
            return Canvas.select( ...(signal.intake) );
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------





// listen :: Modify : StringIntake .. convey global Modify(string) to Canvas.modify - which emits: `ModifyStringIntake`
// ----------------------------------------------------------------------------------------------------------------------------
    Modify.listen
    ({
        StringIntake ( signal )
        {
            return Canvas.modify( ...(signal.intake) );
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------





// listen :: Render : <Family>Intake .. convey global Render(<Family>) to Canvas.render - which emits: `Render<Family>Intake`
// ----------------------------------------------------------------------------------------------------------------------------
    Render.listen
    ({
        𐘥Intake ( signal )
        {
            return Canvas.render( ...(signal.intake) );
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------





// listen :: Insert : <Family>Intake .. convey global Insert(<Family>) to Canvas.insert - which emits: `Insert<Family>Intake`
// ----------------------------------------------------------------------------------------------------------------------------
    Insert.listen
    ({
        𐘥Intake ( signal )
        {
            return Canvas.insert( ...(signal.intake) );
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------





// listen :: Update : <Family>Update .. convey global Update(<Family>) to Canvas.update - which emits: `Update<Family>Intake`
// ----------------------------------------------------------------------------------------------------------------------------
    Update.listen
    ({
        StringIntake ( signal )
        {
            return Canvas.update( ...(signal.intake) );
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------





// listen :: Remove : StringIntake .. convey global Remove(string) to Canvas.remove - which emits: `RemoveStringIntake`
// ----------------------------------------------------------------------------------------------------------------------------
    Remove.listen
    ({
        StringIntake ( signal )
        {
            return Canvas.remove( ...(signal.intake) );
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------





// listen :: Canvas : DefineStringIntake .. emitted by `Canvas.define()` - which is triggered above at: `this.define( htmTag )`
// ----------------------------------------------------------------------------------------------------------------------------
    Canvas.listen
    ({
        DefineStringIntake ( signal )
        {
            let htmTag = signal.intake[0];
            let method = signal.intake[1];
            let padded = this.htNode.define.padded;
            let naming = ( (padded[0]||"") + htmTag + (padded[1]||"") );
                method = Medium.create(naming,method);

            Create( Global ).using( method.bind(method) );
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------





// listen :: Canvas : ModifyStringIntake .. as in: `Canvas.modify()`
// ----------------------------------------------------------------------------------------------------------------------------
    Canvas.listen
    ({
        ModifyStringIntake ( signal )
        {
            let intake = [ ...(signal.intake) ];
            let target = Select (signal.intake[0])[0];
            return Medium.routed( Modify, target, signal.intake );
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------





// listen :: Canvas : SelectStringIntake .. as in: `Canvas.select()` .. implements `querySelectorAll`
// ----------------------------------------------------------------------------------------------------------------------------
    Canvas.listen
    ({
        SelectStringIntake ( signal )
        {
            let string = signal.intake[0];
            let holder = signal.intake[1];
            let prefix = string[0];
            let select = "#.[", listed;
            let htmTag = ( (string[0] + string.slice(-1)) === "<>" );

            if ( !select.includes(prefix) && !htmTag )
            { return }; // not for us

            let target = ( htmTag ? string.slice(1,-1) : string );
            let output = [ ...(document.querySelectorAll(target)) ];

            if ( (output.length < 1) && (target === "text") )
            {
                holder = (holder || document.body);
                listed = [ ...(holder.childNodes) ];

                listed.map( function eachNode ( object )
                {
                    if ( (Medium.strain(object) === "Text") && !object.tagName && !object.innerHTML )
                    { output.push( object ) };
                });
            };

            return output;
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------





// listen :: Canvas : RemoveStringIntake .. like: `querySelectorAll`
// ----------------------------------------------------------------------------------------------------------------------------
    Canvas.listen
    ({
        RemoveStringIntake ( signal )
        {
            Script.intake( signal.intake ).map( function eachUnwanted ( string )
            {
                Select( string ).map( function eachToRemove ( object )
                {
                    object.parentNode.removeChild( object )
                });
            });
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------





// listen :: Canvas : CreateStringIntake .. from like: Canvas.create("div")
// ----------------------------------------------------------------------------------------------------------------------------
    Canvas.listen
    ({
        CreateStringIntake ( signal )
        {
            let naming = signal.intake[0]; // tagname
            let casing = String.unwrap( naming, true );
            let padded = Canvas.htNode.define.padded;
            let htmTag = ( Script.differ(casing,padded) ? naming : naming.slice(1,-1)); // tagname
            let intake = Script.intake( signal.intake[1] ); // list of arguments
            let detail = intake.shift(); // attributes
            let detect = Medium.detect( detail, 9 );
            let output = document.createElement( htmTag );

            let herald = this.config.heraldedKeys.split(",");
            let method;

            herald.scroll(( aspect )=>
            {
                method = function ()
                {
                    let intake = [ ...arguments ];
                    let output = Canvas[ this.aspect ]( intake[0], this.entity, intake[1] );
                    return output;
                };

                output[ aspect ] = method.bind( Medium.create(aspect, {aspect,entity:output}) );
            });

            Medium.secure( output, {insert( intake, backup )
            {
                Canvas.insert( intake, this, backup, false );
                return this
            }});

            if ( detect === "object" ) // change attributes
            { return Canvas.change( output, detail ) };

            if ( detect !== "undefined" ) // insert contents
            { return Canvas.insert( detail, output ) };

            return output;
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------





// listen :: Canvas : ChangeObjectIntake .. from like: `Canvas.change()`
// ----------------------------------------------------------------------------------------------------------------------------
    Canvas.listen
    ({
        ChangeObjectIntake ( signal )
        {
            let entity = signal.intake[0];
            let design = signal.intake[1];

            Medium.indice( design ).map( function assignAspect ( aspect )
            {
                let detail = design[ aspect ];

                if ( !!Canvas.htNode.change[aspect] )
                { Canvas.htNode.change[aspect](entity, detail ) }
                else
                {
                    entity.setAttribute( aspect, detail );
                    entity[ aspect ] = detail;
                }
            });

            return entity;
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------





// listen :: Canvas : Insert<Family>Intake .. as in: `Canvas.insert( ᐸbrᐳ, document.body )`
// ----------------------------------------------------------------------------------------------------------------------------
    Canvas.listen
    ({
        Insert𐘥 ( signal )
        {
            let intake = signal.intake, htNode;
            let target = ( intake[1] || this.config.renderTarget );
            let htmTag = signal.intake[2];
            let isNode = { intake:Canvas.isNode(intake[0]), target:Canvas.isNode(target) };
                intake = intake[0];


            if ( !isNode.target && ((typeof target) === "string") )
            {
                target = Canvas.select(target)[0];

                if ( !Canvas.isNode(target) )
                { moan( `expecting HTML-Element as target` );  return };
            };


            if ( !isNode.intake )
            { intake = Script.texted( intake ) };


            if ( !!htmTag )
            {
                htNode = document.createElement( htmTag );
                htNode.innerHTML = intake;
                isNode.intake = true;
                intake = htNode;
            };


            if ( isNode.intake )
            {
                target.appendChild( intake );
                return target;
            };


            htNode = document.createElement( htmTag || "holder" );
            htNode.innerHTML = intake;

            [ ...(htNode.childNodes) ].scroll(( object )=>
            {
                if ( object.nodeName === "#text")
                {
                    htNode = document.createElement( "text" );
                    htNode.innerHTML = object.textContent;
                    object = htNode;
                };

                target.appendChild( object );
            });


            return target;
        },



        InsertFunctionIntake ( signal )
        {
            let intake = signal.intake;
            let naming = Medium.naming( intake[0] );
            let casing = String.unwrap( naming, true );
            let padded = Canvas.htNode.define.padded;

            if ( !Script.differ(casing,padded) )
            { intake[0] = intake[0]() };

            while ( intake.length < 3 )
            { intake.push( undefined ) };

            if ( intake.length < 4 )
            { intake.push( false ) };

            return this.insert( ...(intake) );
        },



        InsertArrayIntake ( signal )
        {
            var intake = signal.intake;
            var target = ( intake[1] || this.config.renderTarget )
            var htmTag = intake[2];

            intake[0].scroll( function insertArrayItem ( object )
            { Canvas.insert( object, target, htmTag, intake[3] ) });

            if ( !!target )
            { return target };

            return this;
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------





// listen :: Canvas : Render<Family> .. as in: `Canvas.render()`
// ----------------------------------------------------------------------------------------------------------------------------
    Canvas.listen
    ({
        Render𐘥 ( signal )
        {
            let target = ( signal.intake[1] || this.config.renderTarget );

            if ( (typeof target) === "string" )
            { target = Canvas.select(target)[0] };

            if ( !Canvas.isNode(target) || Medium.detect(signal.intake[2],"symb") )
            { return }; // not for us

            target.innerHTML = "";
            let intake = Script.intake( ...(signal.intake) );

            while ( intake.length < 3 )
            { intake.push( undefined ) };

            return Canvas.insert( ...intake, false );
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------





// listen :: Canvas : RenderStringIntake .. as in: `Canvas.render()`
// ----------------------------------------------------------------------------------------------------------------------------
    Canvas.listen
    ({
        RenderStringIntake ( signal )
        {
            let target = document.getElementById("renderTarget");
            let intake = signal.intake;
            let string = intake[0];
            let method = intake[1];

            target.innerHTML = "";

            return Canvas;
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------





// listen :: Canvas : UpdateStringIntake .. as in: `Canvas.update()`
// ----------------------------------------------------------------------------------------------------------------------------
    Canvas.listen
    ({
        UpdateStringIntake ( signal )
        {
            return Canvas;
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------





// listen :: Canvas : MediumFinish
// ----------------------------------------------------------------------------------------------------------------------------
    Canvas.listen
    ({
        MediumFinish ()
        {
            Timing( 50 ).detain( function awaitsCanvas()
            {
                Global.signal( "CanvasAwaits" );
            });
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------





// listen :: Global : CanvasAwaits .. wrap up .. of course this can be overriden with your own listener
// ----------------------------------------------------------------------------------------------------------------------------
    Global.listen
    ({
        CanvasAwaits ( signal )
        {
            Remove( "<text>" ); // remove unwanted whitespace
            // console.clear();

            // Canvas.listen
            // ({
            //     [ SIGNAL ]: function sing ( signal )
            //     {
            //         // if ( signal.action.startsWith("ClientMaster") || (signal.action === "MediumAwaits") ){ return };
            //         dump( signal );
            //     }
            // });
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------



/*
*/
