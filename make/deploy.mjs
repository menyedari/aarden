

// depend :: device : master
// ----------------------------------------------------------------------------------------------------------------------------
    import "../dist/.aard.dir/Device/access/device.access.master.mjs";
// ----------------------------------------------------------------------------------------------------------------------------




// listen :: systemLoaded : wait for system to load, then install the `aarden` command
// ----------------------------------------------------------------------------------------------------------------------------
    Global.listen( "systemLoaded", function deploySystemAccess ( signal )
    {
        let target = Script.source.parent;
        let assign = ( "PATH=$PATH:" + target );
        let bashrc = Select( "~/.bashrc" ).yield();
        let exists = { inPATH:process.env.PATH.includes(target), onBOOT:bashrc.includes(assign) };

        if ( !exists.inPATH )
        { Device.bash.execSync( "export " + assign ) };

        if ( !exists.onBOOT )
        {
            target = Device.disk.accept( "~/.bashrc" );
            Device.disk.writeFileSync( target, ( bashrc + "\n\n" + assign ) );
            console.log( `aarden deployed .. you may need to reboot` );
        };
    });
// ----------------------------------------------------------------------------------------------------------------------------
