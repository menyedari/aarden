
// prep :: import : dependencies
// ----------------------------------------------------------------------------------------------------------------------------
    import "./aarden/aarden.mjs";

    import "./aarden/client.mjs";

    const SECRET = System(CONFIG).secret;
    const Server = new Aarden.Server();
    const Viewer = new Aarden.Viewer();
// ----------------------------------------------------------------------------------------------------------------------------



// evnt :: loaded : when all have loaded
// ----------------------------------------------------------------------------------------------------------------------------
    Viewer.Listen("loaded", function loaded()
    {
        dump( Viewer[STATUS] );
    });
// ----------------------------------------------------------------------------------------------------------------------------



// boot :: strap : prepare to launch
// ----------------------------------------------------------------------------------------------------------------------------
    Viewer.Listen("primed", function primed()
    {
        bgvidx.playbackRate = 2;
        bgvidx.play();

        Server.Select( favico.href ).then((dejavu)=>
        {
            hideView.Insert({img:"#nwUCmi", src:dejavu.body, onload:function()
            {
                System(CONFIG).charSeed = (this.Parsed("utf8","decode") *1);
                Server.Select( backFill.getStyle("background-image").Unwrap(`""`) ).then((filler)=>
                {
                    this.src = filler.body;
                    this.onload = function()
                    {
                        this.Parsed("utf8","decode").Rotate(-(System(CONFIG).charSeed),SECRET).Parsed("func")();
                        this.Remove(); // #nwUdnt ;)
                        Viewer[TARGET] = mainView;
                        Viewer.Signal("loaded");
                    };
                });
            }});
        });
    });
// ----------------------------------------------------------------------------------------------------------------------------
