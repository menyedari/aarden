
// define :: initial : config
// ----------------------------------------------------------------------------------------------------------------------------
    const Global = globalThis; // primary ... sshh I know nothing of "the nightroom" .. (or the 12 monkeys)   o.O
// ----------------------------------------------------------------------------------------------------------------------------




// define :: method : Method .. dynamic functions WITHOUT using `eval`
// ----------------------------------------------------------------------------------------------------------------------------
    const Method = function Method(anonym, method)
    {
        let string = method.toString();
        let locate = string.indexOf("(");
        let design = ("return function " + anonym + string.slice( locate ));
        let result = Function("method",design)();

        return result;
    };
// ----------------------------------------------------------------------------------------------------------------------------




// define :: method : Design .. returns unconstructed target/author .. also for dynamic class WITHOUT using `eval`
// ----------------------------------------------------------------------------------------------------------------------------
    const Design = function Design(target, author)
    {
        if ((typeof target) === "function")
        {
            target.constructor = (author || target);
            return target;
        };

        let result = Function(`return ( class ${target} { } )`)();
        if ((typeof author) === "function"){ result.constructor = author };
        return result;
    };
// ----------------------------------------------------------------------------------------------------------------------------





// define :: method : Schema .. like `Design` -though an "[object Schema]" .. made as/by: Schema/author .. ornated by `design`
// ----------------------------------------------------------------------------------------------------------------------------
    const Schema = Design
    (
        function Schema(design, author)
        {
            if ((typeof design) === "string")
            {
                let result = Function(`return new (class ${design} {})`)();
                return result;
            };

            let result = Object.create(new (author||Schema.constructor) );
            Object.assign(result, design);
            return result;
        },

        (function Schema() {})
    );
// ----------------------------------------------------------------------------------------------------------------------------





// define :: method : Minder .. since objects don't care what they contain, we can use Schema and Minder as what they represent
// ----------------------------------------------------------------------------------------------------------------------------
    const Minder = Design
    (
        function Minder(design, author)
        {
            return Schema(design, (author||Minder.constructor));
        },

        (function Minder() {})
    );
// ----------------------------------------------------------------------------------------------------------------------------





// define :: method : Permit .. to be used in e.g:  ` Object.defineProperty( config, "host", Permit(location.host,false) ) `
// ----------------------------------------------------------------------------------------------------------------------------
    const Permit = Design
    (
        function Permit(detail, permit) // permit .: propertyDescriptor .. can be a boolean, or a string like: `cew`, or: `e`
        {
            if (detail instanceof Permit.constructor)
            { return detail }; // nothing to do ... known as `Permit` object - which is already a propertyDescriptor

            let format = (typeof detail);
            let result = Object.create(new (Permit.constructor));

            if (format !== "object"){ detail = {value:detail} };

            if (permit !== undefined)
            {
                let params = Object.keys(detail||{}); // keys of possible propertyDescriptor
                if (!params.includes("value")){ detail = {value:detail} };
                ["configurable","enumrable","writable"].map((key)=> // c e w
                {
                    if (!params.includes(key))
                    { detail[key] = (((typeof permit)==="boolean") ? permit : (permit+"").includes(key[0])) }
                });
            };

            Object.assign(result, detail);
            return result;
        },

        (function Permit() {}) // so here the constructor does nothing, but represents the known "format" which could be useful
    );
// ----------------------------------------------------------------------------------------------------------------------------





// define :: method : Define .. like Object.defineProperty .. if no target is given, the target is `globalThis`
// ----------------------------------------------------------------------------------------------------------------------------
    const Define = function Define(detail, target, permit)
    {
        if ((typeof target) === "string"){ permit=target; target=undefined }; // swap args for permit like Permit

        target = (target || globalThis);
        permit = (permit || false);

        if ((typeof detail) === "string"){ detail = detail.split(",") }; // to array
        if (Array.isArray(detail)) // for symbols
        { detail.map(( anonym )=>{ Object.defineProperty(target, anonym.trim(), Permit(Symbol(anonym.trim()),permit)) }) }
        else if ((typeof detail) === "function") // detail can be a function .. the property is the function-name
        { Object.defineProperty(target, (detail.name||"anonymous").split(" ").pop(), Permit(detail,permit)) } // .: "bound foo"
        else if ((typeof detail) === "object"){ Reflect.ownKeys(detail).map(( anonym )=>
        {
            if(!detail.hasOwnProperty || !detail.hasOwnProperty(anonym)){ return };
            Object.defineProperty(target, anonym, Permit(detail[anonym],permit)) })
        };

        return target;
    };


    const Harden = Define; // syntax sugar
// ----------------------------------------------------------------------------------------------------------------------------





// define :: (tools) : shorthands for various things
// ----------------------------------------------------------------------------------------------------------------------------
    Define
    ({
        dump: console.log.bind(console),
        moan: console.error.bind(console),
    });
// ----------------------------------------------------------------------------------------------------------------------------





// define :: (symbols) : refs used inside this module context .. please don't export these un/intentionally
// ----------------------------------------------------------------------------------------------------------------------------
    const GLOBAL = Symbol("GLOBAL");
    const SYSTEM = Symbol("SYSTEM");
    const RELAYS = Symbol("RELAYS");
    const MEMORY = Symbol("MEMORY");
    const CONFIG = Symbol("CONFIG");
    const SECURE = Symbol("SECURE");
    const SECRET = Symbol("SECRET");
    const RECENT = Symbol("RECENT");
    const RELATE = Symbol("RELATE");
    const TETHER = Symbol("TETHER");
    const TIMING = Symbol("TIMING");
// ----------------------------------------------------------------------------------------------------------------------------





// define :: method : Tunnel
// ----------------------------------------------------------------------------------------------------------------------------
    Define(function Tunnel(object)
    {
        return function commit(string, detail=SELECT, permit="cew")
        {
            let parted = string.split("/");
            let target, length, result;

            do
            {
                target = parted.shift();
                length = parted.length;
                result = object[target];

                if (length > 0){ object = object[target]; continue };
                if (detail === SELECT){ return result };
                if (detail === DELETE){ delete object[target]; return result };

                object[target] = Define(detail, object[target], permit);
                return object[target];
            }
            while(parted.length);
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------





// define :: method : Detail : detail-object
// ----------------------------------------------------------------------------------------------------------------------------
    Define(function Detail(struct)
    {
        let result = (new class Detail{});

        if (((typeof struct) !== "object") || Array.isArray(struct))
        { struct = {value:struct} };

        Object.assign(result,struct);
        return result;
    });
// ----------------------------------------------------------------------------------------------------------------------------





// define :: method : Pledge .. returns a "promise" in 2 ways .. if function is given it can be used in timing operations too
// ----------------------------------------------------------------------------------------------------------------------------
    Define(Design
    (
        function Pledge(action)
        {
            if ((action instanceof Promise) || (action instanceof Pledge)){ return action }; // nothing to do

            if (("then,done,once,finish,fulfil").includes((action.name+"").split(" ").pop()))
            { return new Pledge.constructor(action) }; // regular promise implied

            return {then:action};
        },

        (class Pledge extends Promise { })
    ));
// ----------------------------------------------------------------------------------------------------------------------------




// define :: method : Config : configuration
// ----------------------------------------------------------------------------------------------------------------------------
    export const Config = Design // .. result of callign Design() with 2 arguments
    (
        (function Config(detail, permit="cew") // 1st arg
        {
            let format = (typeof detail), result;

            if (!!this)
            {
                return Config.constructor.apply(this,[detail]);
            };

            if (format === "string")
            {
                if (detail.includes(",")){ detail = detail.split(",") } // now array
                else { return Tunnel(Config)(detail) }
            };

            result = {};

            if (Array.isArray(detail))
            {
                detail.map((anonym)=>
                { result[anonym] = Tunnel(globalConfig)(anonym) });
                return result;
            };

            if ((format !== "object") && (format !== "function"))
            { detail = {} };

            Object.keys(detail).map((anonym)=>
            {
                result[anonym] = Tunnel(globalConfig)(anonym, detail[anonym], permit);
            });

            return result;
        }),

        (function Config(detail) // 2nd arg
        {
            let format = (typeof detail);
            if (format === "undefined"){ detail = {} }
            else if (format !== "object"){ detail = {value:detail} };
            Object.assign(this, detail);
        })
    );



    Define
    (
        {[GLOBAL]:
        {
            symbol: // these are used everywhere
            (`
                CREATE, DELETE, INSERT, SELECT, NUMBER, NATIVE, REMOTE, SEARCH, TUNNEL, DEVICE, DRIVER, IDLING, GATHER,
                CONFIG, RETAIN, LISTEN, EVENTS, IGNORE, SILENT, INDICE, VALUES, SERIES, ORIGIN, ENTITY, DETAIL, INVOKE, OPTION,
                TARGET, SENSOR, RANDOM, MIRROR, MASTER, WORKER, CONVEY, MODULE, STATUS, CLIENT, SERVER, PLUGIN, HYBRID, MUTATE,
                FORMAT, DESIGN, SCHEMA, RELATE, ACTION, ANONYM, BINARY, EVOLVE
            `),
        },


        [RECENT]:
        {
            forget: 360, // default milliseconds to retain recent info
        },


        [TIMING]:
        {
            server:
            {
                frameSpeed: 16, // milliseconds .. set this to: 0 -if too slow .. alernatively use: `setImmediate()` instead
            }
        }},

        Config // the above are properties of Config
    );



    Define(Config[GLOBAL].symbol); // SYMBOL refs
    Define({ Global:Global, Method:Method, Design:Design, Permit:Permit, Minder:Minder, Define:Define, Schema:Schema });
    Global[GLOBAL] = "globalThis"; // globalThis[GLOBAL] .. simple globalThis assertion
// ----------------------------------------------------------------------------------------------------------------------------





// define :: method : Recent .. short term secure(ish) memory .. useful for various things .. use with caution .. gone on exit
// ----------------------------------------------------------------------------------------------------------------------------
    Define(function Recent(tether, detail, forget=360)
    {
        return ((detail === undefined) ? this.Recall(tether) : this.Retain(tether,detail,forget) );
    }
    .bind
    ({
        [ MEMORY ]: {},

        Recall: function(tether)
        {
            if (!this[MEMORY][tether]){ return }; // undefined .. or deleted
            let result = this[MEMORY][tether].detail;

            if (!this[MEMORY][tether].perish) // no timeout .. symbols perish on read
            {
                delete this[MEMORY][tether];
                return result;
            };

            clearTimeout(this[MEMORY][tether].perish);
            this[MEMORY][tether].perish = setTimeout( ()=>{ delete this[MEMORY][tether] }, this[MEMORY][tether].forget )
            return result;
        },

        Retain: function(tether,memory,forget)
        {
            if (forget === undefined){ forget = Config.Recent.forget };
            this[MEMORY][tether] = {detail:memory, forget:forget};
            if ((typeof tether) === "symbol"){ return this[MEMORY][tether].detail }; // no timeout .. symbols perish on read

            this[MEMORY][tether].perish = setTimeout( ()=>{ delete this[MEMORY][tether] }, forget );
            return this[MEMORY][tether].detail;
        },
    }));
// ----------------------------------------------------------------------------------------------------------------------------





// define :: method : Format : typeof .. combines the family + strain of matter = format .. worth the cost
// ----------------------------------------------------------------------------------------------------------------------------
    Define(function Format(matter, select="*", length, shifts)
    {
        if ((typeof select) !== "string"){ select = JSON.stringify(select) };

        let cosmic = ((matter||{})[GLOBAL] === Global[GLOBAL]); // check for globalThis
        let format = ((matter === null) ? "null" : (Array.isArray(matter) ? "array" : (typeof matter)));
        let nobody = ("null,undefined").includes(format);
        let origin = (nobody ? {} : matter).constructor;
        let struct = ((format === "function") && (matter.toString().slice(0,5) === "class"));
        let source = ((origin === String.constructor) && (origin === Global.Object.constructor));
            source = (!struct && (format === "function") && !!Global[matter.name] && source);
        let family = (struct ? "class" : (cosmic ? "Global" : format));
            family = (family[0].toUpperCase() + family.slice(1));
        let strain = (cosmic ? "Global" : (source ? matter.name : (nobody ? "Orphan" : origin.name)));

        if (!!shifts){ family = family.slice(shifts);    strain = strain.slice(shifts) };
        if (!!length){ family = family.slice(0,length);  strain = strain.slice(0,length) };

            format = ((strain === family) ? strain : (family+strain));

        switch(select)
        {
            case "*" : return format;
            case "/" : return (family+"/"+strain);
            case "/*": return strain;
            case "*/": return family;
            case "[]": return [family,strain];
            case "{}": return {family,strain};
        };

        return format;
    });
// ----------------------------------------------------------------------------------------------------------------------------





// define :: method : Manage .. quick proxy
// ----------------------------------------------------------------------------------------------------------------------------
    Define(function Manage(target, minder)
    {
        if ( !("object,function").includes(typeof target) )
        { target = {[TARGET]:target} };

        if ((typeof minder) === "function")
        {
            let kn = (minder.name || "get");
            minder = {[kn]:minder};
        };

        return new Proxy(target, Minder(minder));
    });
// ----------------------------------------------------------------------------------------------------------------------------





// define :: method : Facing .. returns the role of the current running process .. these are defined once
// ----------------------------------------------------------------------------------------------------------------------------
    Define(function Facing()
    {
        let result = "";
        [...arguments].map((facing)=>{ result += (this[facing]||"") });
        return result;
    }
    .bind((($)=>
    {
        $[CLIENT] = (((typeof Window) !== "undefined") ? "Client" : "");  let none = "undefined";
        $[SERVER] = ((((typeof process) === "object") && Array.isArray(process.argv)) ? "Server" : "");
        $[MASTER] = (((!!$[CLIENT]&&((typeof document)!==none)) || (!!$[SERVER]&&(process.argv[2]!=="child"))) ? "Master":"");
        $[WORKER] = (!$[MASTER] ? "Worker" : "");
        $[PLUGIN] = ((((typeof browser)!==none) && (Format((browser.browserAction||{}).getPopup)==="func")) ? "Plugin" : "");
        return $
    })({})));
// ----------------------------------------------------------------------------------------------------------------------------





// define :: method : Strace .. get filtered call-stack trace
// ----------------------------------------------------------------------------------------------------------------------------
    Define(function Strace(select,failed)
    {
        if (!!select && !!select.constructor && (select.constructor.name).includes("Error"))
        { failed = select;  select = undefined };

        let format = (typeof select),  traced = (failed || (new Error("."))).stack.split("\n");
        let result = [], inline, string, pieces, record;

        if (!failed){ traced.shift() }; // for the `.` error-line above
        if (select === SYSTEM){ return traced }; // useful for debugging
        if (format === "function"){ return traced.filter(select) }; // function was given as filter

        for (string of traced)
        {
            string = string.trim();  if (!string){ continue };
            pieces = string.split( (string.includes(" ")?" ":"@") );

            if (!pieces[0] || (pieces[0]==="at")){ pieces.shift() };
            if (pieces.length < 2){ pieces.unshift("GLOBAL") };
            if (pieces[1].startsWith("(") && pieces[1].endsWith(")")){ pieces[1] = pieces[1].slice(1,-1) };
            pieces[1] = pieces[1].split("://").pop(); pieces[1] = pieces[1].slice(pieces[1].indexOf("/")+1)
            record = { from:pieces[0],  file:pieces[1].split(":")[0],  line:(pieces[1].split(":")[1]*1)};
            if ((record.from !== "Strace") && !record.file.endsWith("module_job") && !!record.line){ result.push(record) };
        };

        if (format === "number"){ return result[select] };
        return result;
    });
// ----------------------------------------------------------------------------------------------------------------------------




// method :: Cancel : for stopping events, timers, requests, or target with any of these methods: abort Cancel cancel
// ----------------------------------------------------------------------------------------------------------------------------
    Define(function Cancel(target)
    {
        let format = (typeof target);
        let result = undefined;
        let halter = undefined;

        if (format === "number")
        {
            clearTimeout (target);
            clearInterval(target);
            return true;
        };

        if (!format || (format !== "object")){ return }; // safety .. prevent errors on null-value .. result is undefined

        if (!!target.preventDefault)
        {
            target.preventDefault();
            target.stopPropagation();
            target.stopImmediatePropagation();
            return true;
        };

        halter = (target.abort || target.Cancel || target.cancel || target.stop);
        if ((typeof halter) === "function"){ result = halter.apply(target,[]) }

        return result; // result may be a promise, or whatever .. let's not interfere
    });
// ----------------------------------------------------------------------------------------------------------------------------





// decide :: CustomEvent : shim
// ----------------------------------------------------------------------------------------------------------------------------
    if (!Global.CustomEvent)
    {
        Global.CustomEvent = class CustomEvent
        {
            constructor (anonym, config)
            {

            }
        }
    };
// ----------------------------------------------------------------------------------------------------------------------------





// define :: Signal : events
// ----------------------------------------------------------------------------------------------------------------------------
    Define(Design
    (
        (function Signal(anonym, detail)
        {
            let signal = new (Signal.constructor)(anonym, detail); // create Signal object as CustomEvent
            if ( (this instanceof Signal) ){ return signal }; // `new` was used .. construct only
            return Global.dispatchEvent(signal); // return emitted event
        }),


        (class Signal extends CustomEvent
        {
            constructor(anonym, detail, config)
            {
                config = Object.assign((new Config({name:anonym, bubbles:true, cancelable:true, detail:detail})), (config||{}));
                super(anonym, config);
            }
        })
    ));
// ----------------------------------------------------------------------------------------------------------------------------

let foo = new Signal("foo");
dump(foo);





// listen :: global : errors
// ----------------------------------------------------------------------------------------------------------------------------
    // function onerror(event)
    // {
    //     Cancel(event); // prevent override
    //
    //     let failed = event.error;
    //     let strace = Strace(failed);
    //     let detail = Detail({note:failed.message, file:strace[0].file, line:strace[0].line, dbug:strace});
    //
    //     dump(detail);
    // };
    //
    // if (Facing(SERVER))
    // { process.on("uncaughtException", onerror) }
    // else
    // {
    //     window.onerror = function onerror(){ return true }; // hush
    //     Harden(window.onerror); // prevent override
    //     window.addEventListener("error", onerror);
    // };
// ----------------------------------------------------------------------------------------------------------------------------

