



// load :: dependencies
// ----------------------------------------------------------------------------------------------------------------------------
    import * as tXml from "./txml.mjs"; // awesome XML parsing lib .. https://github.com/TobiasNickel/tXml.git
// ----------------------------------------------------------------------------------------------------------------------------




// func :: dump/moan : shorthands for `console.log` & console.error
// ----------------------------------------------------------------------------------------------------------------------------
    const dump = console.log.bind(console);
    const moan = console.error.bind(console);
// ----------------------------------------------------------------------------------------------------------------------------




// func :: descry : concise `typeof` .. returns "class/extension"
// ----------------------------------------------------------------------------------------------------------------------------
    const descry = function descry(defn)
    {
        let resl = Object.prototype.toString.call(defn).toLowerCase().slice(1,-1).split(" ").join("/");
        let omit = ["object/undefined", "object/null"];

        if (omit.indexOf(resl) > -1)
        {
            return (resl.split("/").pop());
        };

        return resl;
    };
// ----------------------------------------------------------------------------------------------------------------------------




// func :: isConf : check if given arg is a config-object
// ----------------------------------------------------------------------------------------------------------------------------
    const isConf = function isConf(defn,flag)
    {
        if ((typeof defn) != "object"){ return };
        if ((typeof flag) != "string"){ flag = "meta" };
    };

    Object.assign(isConf,
    {
        meta: function meta(){}
    });
// ----------------------------------------------------------------------------------------------------------------------------





// shim :: Object.Modify : set any restricted/hidden property like: `obj.Modify({name:"foo", type:"bar"})`
// ----------------------------------------------------------------------------------------------------------------------------
    Object.defineProperty(Object.prototype,"Modify",{configurable:false,enumerable:false,writable:false,value:
    function Modify(defn)
    {
        if ((typeof defn) == "function"){ defn = {[(defn.name||"anonymous")]:defn} };
        if (!descry(defn).includes("object")){ moan("expecting 1st argument as object-like"); return this }; // must not fail

        Object.keys(defn).forEach((attr)=>
        {
            if (descry(defn[attr]).includes("object") && !defn[attr].name)
            { Object.defineProperty(defn[attr],"name",{configurable:true, enumerable:false, writable:false, value:attr}) };
            Object.defineProperty(this,attr,{configurable:true, enumerable:false, writable:false, value:defn[attr]});
        });

        return this;
    }});

    Object.defineProperty(String.prototype,"Modify",{configurable:true, enumerable:false, writable:false, value:Object.prototype.Modify.bind(String.prototype)});
    Object.defineProperty(Array.prototype,"Modify",{configurable:true, enumerable:false, writable:false, value:Object.prototype.Modify.bind(Array.prototype)});
// ----------------------------------------------------------------------------------------------------------------------------





// shim :: String.reckon : evaluate a string
// ----------------------------------------------------------------------------------------------------------------------------
    String.prototype.Modify(function Reckon(vars)
    {
        vars = (vars || {});
        let code,keys,vals,echo,none;

        code = (this+"");
        keys = Object.keys(vars).join(" ").trim().split(" ").join(","); // parameters
        vals = Object.values(vars); // arguments
        code = ((code.indexOf("return ") < 0) ? ("return "+code) : code); // normalize
        echo = (new Function("try{return function handle ("+keys+"){ "+code+" }}catch(e){}"))();

        return ( ((typeof echo) == "function") ? echo(...vals) : none );
    });
// ----------------------------------------------------------------------------------------------------------------------------





// tool :: Struct : return a new named structure
// ----------------------------------------------------------------------------------------------------------------------------
    class Struct
    {
        constructor(name)
        {
            let echo = (`class ${name}{}; return new ${name}`).Reckon();
            Object.defineProperty(echo,"name",{configurable:false, enumerable:false, writable:false, value:name});
            return echo;
        }
    }
// ----------------------------------------------------------------------------------------------------------------------------





// func :: struct : create a new structure without `new` -and holds library of useful objects
// ----------------------------------------------------------------------------------------------------------------------------
    const struct = function struct(defn)
    {
        if ((typeof defn) == "function")
        { defn = {[(defn.name||"anonymous")]:defn} };

        if ((typeof defn) != "string")
        { return this.auto(defn) };

        return ( this[defn] || new Struct(defn) );
    };

    Object.assign(struct,
    {
        auto: function auto(defn)
        {
            let echo = new Struct("object");
            Object.assign(echo,defn);
            return echo;
        },

        hard: function hard(defn)
        {
            let echo = new Struct("override");
            Object.assign(echo, {configurable:false, enumerable:false, writable:false, value:defn});
            Object.defineProperty(echo,"type",{configurable:false, enumerable:false, writable:false, value:"hard"});
            return echo;
        },

        soft: function soft(defn)
        {
            let echo = new Struct("override");
            Object.assign(echo, {enumerable:false, value:defn});
            Object.defineProperty(echo,"type",{configurable:false, enumerable:false, writable:false, value:"soft"});
            return echo;
        },

        trap: function trap(defn)
        {
            let echo = new Struct("override");
            if ((typeof defn) == "function")
            { defn = {get:defn, set:defn, apply:defn} };
            Object.assign(echo, defn);
            Object.defineProperty(echo,"type",{configurable:false, enumerable:false, writable:false, value:"trap"});
            return echo;
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------





// shim :: Object.assign : assign properties/methods to object
// ----------------------------------------------------------------------------------------------------------------------------
    Object.prototype.Modify(function assign(defn)
    {
        if (!descry(defn).includes("object")){ moan("expecting 1st argument as object"); return this }; // must not fail

        Object.keys(defn).forEach((attr)=>
        {
            let conf = defn[attr];
            if (!(conf instanceof override)){ conf = struct.soft(conf) };
            if (!!conf.value && !conf.value.name && !!conf.value.Modify){ conf.value.Modify({name:attr}) }
            Object.defineProperty(this,attr,conf);
        });

        return this;
    });
// ----------------------------------------------------------------------------------------------------------------------------





// defn :: (constants) : useful for if another script overwrites something we need
// ----------------------------------------------------------------------------------------------------------------------------
    Object.defineProperty(globalThis, "Device", struct.hard(globalThis));
    const VOID = (function(){}()); // undefined
// ----------------------------------------------------------------------------------------------------------------------------





// func :: update :
// ----------------------------------------------------------------------------------------------------------------------------
    // Device.Modify({});
// ----------------------------------------------------------------------------------------------------------------------------



// defn :: ENVITYPE : string reference as the type of environment this script is running in
// ----------------------------------------------------------------------------------------------------------------------------
    const ENVITYPE = (function()
    {
        if ( ((typeof process)!="undefined") && ((typeof __dirname)=="string") )
        { return "njs" };

        if (((typeof browser)!="undefined")&&!!browser.browserAction&&((typeof browser.browserAction.getPopup)=="function"))
        { return "ext" };

        return "web";
    }());

    const CLIENTSIDE = (ENVITYPE=="web");
    const SERVERSIDE = (ENVITYPE=="njs");
    const PLUGINSIDE = (ENVITYPE=="ext");
// ----------------------------------------------------------------------------------------------------------------------------




// defn :: PROCBASE,PROCROLE,PROCTYPE :
// ----------------------------------------------------------------------------------------------------------------------------
    const PROCBASE = ( SERVERSIDE ? "server" : "client" ); // "client" or "server"

    const PROCROLE = (function()
    {
        if (SERVERSIDE){ return ((process.argv[2]==="child") ? "worker" : "master") }; // server-side
        return (((typeof Device.document)==="undefined") ? "worker" : "master"); // client-side
    }());

    const PROCTYPE = ( PROCBASE +"-"+ PROCROLE ); // e.g: server-master .. or client-worker .. etc.
// ----------------------------------------------------------------------------------------------------------------------------




// func :: detect : concise `typeof` .. returns 4-letter word
// ----------------------------------------------------------------------------------------------------------------------------
    const detect = function detect(defn)
    {
        let type,kind;  if(defn === null){return "null"};

        type = (typeof defn).slice(0,4);  if (type == "unde"){ return "void" };
        kind = descry(defn).split("/").pop().slice(0,4);

        if((type=="func") && ((typeof defn.constructor)=="function") && (defn.constructor.name !== "Function")){return "tool"}
        else if ((type=="obje") && ((typeof defn.hasOwnProperty)!="function")){ return "tool" }
        else if(("arra argu list coll").indexOf(kind) > -1){ return "list" };

        return (this[kind] || this[type] || kind);
    }
    .bind
    ({
        wind:"main", glob:"main", numb:"numr", stri:"text", html:"node", arra:"list", obje:"knob", rege:"regx",
    });
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: Object.define : define properties .. shorhand for Object.defineProperty
// ----------------------------------------------------------------------------------------------------------------------------
    // Object.prototype.modify({});
    // Object.defineProperty(Object.prototype, "define", meta("hard",function define()
    // {
    //     let defn,type,temp,name,rigd,data;  defn=(arguments)[0];  type=detect(defn); // definition is first param
    //
    //     if ((type == "func") && !!defn.name)
    //     { defn = {[defn.name]:defn} } // make defn obj using function name .. try not to fail
    //     else if (detect(defn) == "text") // for words as flags
    //     {
    //         temp = defn.trim().split("\n").trim().join(" ").split(" "); // sanitize
    //         defn = {};  temp.forEach((item)=> // go through each, trim, validate, assign values as key-names wraped in ::
    //         { item=item.trim(); if (!item){return}; defn[item]=(":"+item+":")});  temp=VOID; // clean up! .. still in context
    //     };
    //
    //     if (detect(defn) != "knob"){ throw "expecting definition-object, or named-function, or words"; return }; // validation
    //
    //     for (name in defn) // defn is now an object .. validate and set rigitity
    //     {
    //         if (!name || !defn.hasOwnProperty(name)){continue};  rigd="hard"; // property validation .. rigidity is `hard`
    //         data = defn[name]; try{ delete this[name] }catch(e){}; // try remove before illegal-rename
    //         temp = ((!!data && !!data.constructor) ? data.constructor.name : "!"); // name of possible constructor .. or not
    //         if (("hard soft trap").indexOf(temp) > -1){ rigd=temp; data=data.data }; // implied rigidity structures with data
    //         if (((typeof data)=="function") && !data.name){Object.defineProperty(data,"name",{value:name})}; // for debugging
    //         Object.defineProperty(this, name, meta(rigd,data)); // apply definition .. if anything went wrong, check errors
    //     };
    //
    //     defn=VOID; temp=VOID; name=VOID; rigd=VOID; data=VOID;  // clean up!
    //     return this; // for your chainable pleasure ;)
    // }));
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: Array.trim() : apply `trim` to array of strings
// ----------------------------------------------------------------------------------------------------------------------------
    Array.prototype.define(function trim()
    {
        this.forEach((itm,idx)=>
        {
            if((typeof itm) !== "string"){return};
            this[idx] = itm.trim();
        });
        return this;
    });
// ----------------------------------------------------------------------------------------------------------------------------




// defn :: (global constants) : to use everywhere .. mostly for system flags and syntax-sugar
// ----------------------------------------------------------------------------------------------------------------------------
    Device.define
    (`
        A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
        OK NA
        ANY ALL ASC DSC GET SET RIP BGN END
        INIT AUTO COOL DARK LITE INFO GOOD NEED WARN FAIL NEXT SKIP STOP DONE ACTV NONE BUSY KEYS VALS ONCE EVRY BFOR AFTR
        UNTL EVNT FILL TILE SPAN OPEN SHUT SELF VERT HORZ DEEP OKAY DUMP PROC FILE FRST LAST MIDL SOME WRAP
        CLIENT SERVER PLUGIN SILENT UNIQUE COPIES FORCED PARTED EXCEPT
    `);
// ----------------------------------------------------------------------------------------------------------------------------




// func :: copied : duplicate .. if numr or text then n repeats n-times
// ----------------------------------------------------------------------------------------------------------------------------
    const copied = function copied(v,n, r,t)
    {
        t = detect(v); if((t=="void")||(t==="null")||(t=="bool")||(v==="")){return v}; // cannot copy
        if ((t==="numr")||(t==="text")){if(!n){return v}; v=(v+""); n=parseInt(n); r=""; for(let i=0;i<n;i++){r+=v}; return r};
        if (((typeof Element)!=="undefined")&&(v instanceof Element)){return (v.cloneNode(true))};
        if (t==="list"){r=[]; v=([].slice.call(v)); v.forEach((i)=>{r.push(copied(i))}); return r};
        if (t==="knob"){r={}; for(let k in v){if(!v.hasOwnProperty(k)){continue}; r[k]=copied(v[k])}; return r};
        if (t==="func")
        {
            r=new Function("try{return "+v.toString()+"}catch(e){return}")(); if(isVoid(r))
            {fail("copy :: something went wrong; check the console"); moan("tried to copy:\n"+v.toString()); return};
            Object.keys(v).forEach((fk)=>{r[fk]=copied(v[fk])});
            return r;
        };

        moan("failed to copy "+t);
    };
// ----------------------------------------------------------------------------------------------------------------------------




// func :: texted : returns text-version of anything given in `what`
// ----------------------------------------------------------------------------------------------------------------------------
    const texted = function texted(what)
    {
        if (!what){return (what+"")}; // anything that isn't a thing will be handled this way .. for realzies
        let tpe,rsl;  tpe = detect(what); if (tpe == "main"){return ":MAIN:"}; // woa -big one!
        if ((typeof what.toString)=="function"){return what.toString()}; // as intended .. namaste
        if(what instanceof HTMLElement){return what.outerHTML}; // gimme' dat html pleeeze
        if(what.body instanceof HTMLElement){return what.body.parentNode.outerHTML}; // oh yeah! -keep it comin'!
        if ((tpe != "knob") && (tpe != "list")){return (what+"")}; // whatever this was -is now text!

        rsl = ((tpe=="knob") ? {} : []); // re-build the result, but text everything eventually .. here we go!

        for (let idx in what)
        {
            if (!what.hasOwnProperty(idx)){continue}; // not interesting
            if (what[idx] === what){rsl[idx]=":CYCLIC:"; continue}; // dodged a bullet there .. whew
            rsl[idx] = texted(what[idx]); // however deep you're hiding, you will be texted!
        };

        rsl = JSON.stringify(rsl); // booYAA!!
        return rsl;
    };
// ----------------------------------------------------------------------------------------------------------------------------




// func :: parsed : returns implied data-version of text given in `defn` .. very useful
// ----------------------------------------------------------------------------------------------------------------------------
    const parsed = function parsed(defn)
    {
        if ((typeof defn) !== "string"){ return defn }; // already parsed
        let text,resl;  text = defn.trim();
        let dlim = text.hasAny("\n", ";", ",", ":","=");  defn=VOID; // clean up so we can start

        for (let type in this)
        {
            if (!this.hasOwnProperty(type)){ continue }; // not interesting
            resl = this[type](text,dlim);  if (resl === VOID){ continue }; // try next parser
            text=VOID;  break; // parser worked! .. clean up memory
        };

        if (!text && !!resl){ return resl }; // we have results!
        return text; // well at lest we trimmed it for you :D
    }
    .bind
    ({
        null: function(text)
        {
            text = text.toLowerCase().slice(0,9);
            return ((["?","null","undefined"].indexOf(text) > -1) ? null : VOID);
        },


        bool: function(text)
        {
            text = text.toLowerCase().slice(0,9);
            return ((this.TRUE.indexOf(text) > -1) ? true : ((this.FALS.indexOf(text) > -1) ? false : VOID));
        }
        .bind
        ({
            TRUE: ["true", "yes", "on", "good", "yebo", "y", "+"],
            FALS: ["false", "no", "off", "bad", "fals", "n", "-"],
        }),


        func: function(text)
        {
            if (!(text.startsWith("function ")||text.startsWith("("))){ return }; // does not look like a function
            if (!(text.hasAny("){",")=>",")\n")&&text.hasAny("}"))){ return }; // .. malformed, or broken
            return (new Function("let f="+text+"\nreturn f"))(); // happy hacking :D
        },


        href: function(text)
        {
            let resl;  if ((text.indexOf(" ") > -1) || (text.indexOf("\n") > -1)){ return }; // not URL
            try{ resl = new URL(text) }catch(er){ return };
            resl.searchParams = Object.fromEntries(resl.searchParams);
            return resl;
        },


        qstr: function(text)
        {
            let resl;  if (!(/^(\?|\&)([\w-]+(=[\w-]*)?(&[\w-]+(=[\w-]*)?)*)?$/).test(text)){ return }; // not QRY
            // if (!text.slice(0,1).hasAny("?","&") || text.hasAny(" ","\n") || (text.indexOf("=") < 1)){ return }; // not QRY
            try{ resl = new URLSearchParams(text) }catch(er){ return };
            resl = Object.fromEntries(resl);
            return resl;
        },


        txml: function(text)
        {
            let resl,wrap;  wrap = text.expose(FRST,LAST);
            if ((wrap!=`<>`) || !text.hasAny(`</`,`/>`)){ return }; // not XML
            try{ resl = tXml.parse(text) }catch(er){ return };
            return resl;
        },


        xatr: function(text)
        {
            if (text.hasAny("\n") || !text.hasAll(`=`,`"`,` `)){ return }; // not ATR .. or simple enough to split
            let resl = this.xml("<obj "+text+" ></obj>");  if (!resl){ return }; // not ATR
            return resl[0].attributes;
        },


        json: function(text)
        {
            let resl;  text = text.shaved(",");
            try{ resl = JSON.parse(text);  return resl }catch(er)
            {
                try { resl = JSON.parse("{"+text+"}");  return resl }catch(er) // ..try wrap with curlies
                { try{ resl = JSON.parse("["+text+"]"); return resl }catch(err){} } // ..try wrap with squaries
            };
        },


        mlti: function(text,dlim)
        {
            if (!dlim || !dlim.hasAny("\n",";") || !text.hasAny(dlim)){ return }; // not multi-line
            let resl = [];  text.split(dlim).forEach((line)=>
            {
                let temp = parsed(line);  if (temp === VOID){ return }; // next
                if ((detect(temp) != "knob") && !!resl.push){ resl.push(temp) }
                else{ temp.peruse((val,key)=>{resl[key]=val}) };  temp = VOID; // clean up!
            });

            text=VOID; // clean up!
            return resl;
        },


        list: function(text,dlim)
        {
            if (dlim !== ","){ return }; // not array
            let resl = [];  text.split(",").forEach((item)=>{ resl.push(parsed(item)) });
            return resl;
        },


        knob: function(text,dlim)
        {
            if ((dlim !== ":") && (dlim !== "=")){ return }; // not object
            let resl,name,valu; resl = {};  text = text.split(dlim);
            name = text[0].trim();  valu = text[1].trim();

            return resl;
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------




// func :: params : get/normalize a list of arguments .. if `a` is omitted then implied `argv[]` id used/improvised
// ----------------------------------------------------------------------------------------------------------------------------
    const params = function params(a, r)
    {
        if (a===VOID) // get server.argv, or client.URLSearchParams
        {
            if (CLIENTSIDE)
            { r = new URLSearchParams(location.search) }
            else
            {
                r = this.argv;  if (r){return r};
                r = process.argv;  r.shift();  r.shift();
                r = parsed(r.join("\n"));  if (isText(r)){ r = [r] };
                this.argv = r;

            }
            return r;
        };

        if (length(a) < 1){ return [] }; // void or empty
        r = ((detect(a)!="list") ? [a] : ((detect(a[0])=="list") ? a[0] : a)) // cast to array .. exhume 1st if it is array
        return ([].slice.call(r)); // normalize `arguments`
    }
    .bind({}); // keep this!
// ----------------------------------------------------------------------------------------------------------------------------




// func :: expect : assert identifier-type -or fail .. returns boolean
// ----------------------------------------------------------------------------------------------------------------------------
    const expect = function expect(what,tobe,func)
    {
        if (!isFunc(func))
        {
            func = function fulfil(type,tobe)
            {
                let resl = tobe.hasAny(type);  if (!!resl){return resl};
                throw `expecting ${tobe}`; return FALS;
            };
        };

        if(!!tobe && !!tobe.hasAny)
        {
            return func(detect(what),tobe);
        };

        return struct("methods").define
        ({
            as: function()
            {
                return func(detect(what),params(arguments));
            },

            tobe: function(that)
            {
                return (what === that);
            },
        });
    };
// ----------------------------------------------------------------------------------------------------------------------------




// func :: global : get/set global variables
// ----------------------------------------------------------------------------------------------------------------------------
    Device.define(function global(defn)
    {
        let type,temp,resl;
        type = detect(defn);

        if (type == "text")
        {
            // resl = (new Function(`try{return ${defn};}catch(e){};`))(); // constants .. this won't work in module
            resl = Device[defn];
            return resl;
        };

        Device.define(defn);
    });


    global
    ({
        Device: Device,
        VOID: VOID,
        NULL: NULL,
        TRUE: TRUE,
        FALS: FALS,
        dump: dump,
        moan: moan,
        descry: descry,
        ENVITYPE: ENVITYPE,
        CLIENTSIDE: CLIENTSIDE,
        SERVERSIDE: SERVERSIDE,
        PLUGINSIDE: PLUGINSIDE,
        PROCBASE: PROCBASE,
        PROCROLE: PROCROLE,
        PROCTYPE: PROCTYPE,
        detect: detect,
        texted: texted,
        struct: struct,
        copied: copied,
        parsed: parsed,
        params: params,
        expect: expect,
        hard: hard,
        soft: soft,
        meta: meta,
    });


    global
    ({
        RESERVED:
        {
            meta: "apply construct defineProperty deleteProperty get getOwnPropertyDescriptor getPrototypeOf has "+
                  "ownKeys preventExtensions set setPrototypeOf writable enumerable configurable value",
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------




// func :: stable : check if environment is stable .. designed to run fast as it is used in .peruse
// ----------------------------------------------------------------------------------------------------------------------------
    global(function stable(name)
    {
        if(!Device.HALT){return TRUE};
        moan("`"+name+"` is unstable");
        return FALS;
    });
// ----------------------------------------------------------------------------------------------------------------------------
