
// tool :: Aspect : entity generator
// ----------------------------------------------------------------------------------------------------------------------------
    class Aspect
    {
        constructor(trgt)
        {
            var echo;

            return this;
        }

        Absorb(){}

        Select(){}

        Create(){}

        Update(){}

        Delete(){}

        Couple(){}

        Detach(){}

        Modify(){}

        Insert(){}

        Filter(){}

        Descry(){}

        Config(){}

        Reckon(){}

        Assert(){}

        Vivify(){}

        Pacify(){}

        Revive(){}

        Render(){}

        Listen(){}

        Signal(){}

        Ignore(){}

        Import(){}

        Export(){}

        Parlay(){}

        Notify(){}

        Invite(){}

        Banish(){}

        Resume(){}

        Detain(){}

        Accept(){}

        Reject(){}

        Assign(){}

        Permit(){}

    }
// ----------------------------------------------------------------------------------------------------------------------------




// func :: metaConf/metaTrap : shorthand configuration
// ----------------------------------------------------------------------------------------------------------------------------
    Object.assign
    (
        Object,
        {
            metaConf: function metaConf(defn,opts)
            {
                let indx = {C:"configurable", E:"enumerable", W:"writable"};
                let echo = {configurable:false, enumerable:false, writable:false, value:defn};

                if (((typeof opts) != "string") || !opts){ return echo };
                ((opts||"")+"").toUpperCase().split("").forEach((char)=>
                {
                    let word = indx[char];
                    if (!!word){ echo[word]=true };
                });

                return echo;
            },


            metaTrap: function metaTrap(defn,opts)
            {
                let indx = {G:"get", S:"set", A:"apply"};
                let echo = {};

                if (((typeof opts) != "string") || !opts){ opts="GSA" };
                ((opts||"")+"").toUpperCase().split("").forEach((char)=>
                {
                    let word = indx[char];
                    if (!!word){ echo[word]=defn };
                });

                return echo;
            },


            isMeta: function isMeta(what)
            {
                if ((typeof what) != "object"){ return false }; // must be object
                let find = "configurable enumerable writable get set apply";
                let seen = Object.keys(what).filter((name)=>{ return find.includes(name) });
                return (seen.length > 0);
            },
        }
    );
// ----------------------------------------------------------------------------------------------------------------------------




// func :: Descry : concise `typeof` returns string
// ----------------------------------------------------------------------------------------------------------------------------
    Object.defineProperty(globalThis,"Descry",Object.metaConf(function Descry(defn,need,span)
    {
        if (!need || ((typeof need) == "number")){ span=need; need="/*" };
        if (!span){ span=4 };

        let resl = Object.prototype.toString.call(defn).toLowerCase().slice(1,-1).split(" ");
        let echo = {"*\\":(this.prim[resl[0]] || resl[0]), "/*":(this.seco[resl[1]] || resl[1])};

        echo = (echo[need]||"").slice(0,span);
        return echo;
    }
    .bind
    ({
        prim: {"undefined":"void", "null":"void"},
        seco: {},
    })));
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: (object/*) : Assign
// ----------------------------------------------------------------------------------------------------------------------------
    [Number,String,Array,Object,Function].forEach(function(glob)
    {
        Object.defineProperty(glob.prototype, "Assign", Object.metaConf(function Assign(defn,opts)
        {
            var self = (this || globalThis); // Assign() could have been called without e.g: parent.Assign()

            if ((typeof defn) == "function"){ defn = {[(defn.name||"anonymous")]:defn} }; // e.g: Assign(function bark(){});
            if (Descry(defn)!="obje"){ moan("expecting 1st argument as object or function"); return this }; // must not fail

            Object.keys(defn).forEach((attr)=>
            {
                let valu = (Object.isMeta(attr) ? attr : Object.metaConf(attr,opts));
                if (Descry(defn[attr]).includes("obje") && !defn[attr].name)
                { Object.defineProperty(defn[attr],"name",attr) };
                Object.defineProperty(self,attr,valu);
            });

            return self;
        }
        .bind(glob.prototype)));
    });
// ----------------------------------------------------------------------------------------------------------------------------


// func :: dump/moan : shorthands for `console.log` & console.error
// ----------------------------------------------------------------------------------------------------------------------------
    // const dump = console.log.bind(console);
    // const moan = console.error.bind(console);
// ----------------------------------------------------------------------------------------------------------------------------
