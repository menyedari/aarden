// load :: required : tools
// ----------------------------------------------------------------------------------------------------------------------------
    import * as tXml from "./txml.mjs"; // awesome XML parsing lib .. https://github.com/TobiasNickel/tXml.git
    import "./shim.mjs"; // provide expected functionality
    import "./base.mjs";
    // import "./func.mjs";
    // import "./shim.mjs";
    // import "./tool.mjs";
// ----------------------------------------------------------------------------------------------------------------------------




// load :: required : modules
// ----------------------------------------------------------------------------------------------------------------------------
    // import "../Aspect/aard.mjs";
    // import "../Memory/aard.mjs";
    // import "../Permit/aard.mjs";
    // import "../Server/aard.mjs";
    // import "../Client/aard.mjs";
    // import "../Viewer/aard.mjs";
// ----------------------------------------------------------------------------------------------------------------------------
