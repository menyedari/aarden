



// shim :: CustomEvent : for if missing
// ----------------------------------------------------------------------------------------------------------------------------
    if ((typeof CustomEvent) == "undefined")
    {
        global(class CustomEvent
        {
            constructor(name,data)
            {
                this.define
                ({
                    detail: data,
                    timeStamp: performance.now(),
                    type: name,
                });
            }

            preventDefault()
            {
                this.defaultPrevented = TRUE;
            }

            stopPropagation()
            {
            }

            stopImmediatePropagation()
            {
            }

            defaultPrevented = FALS
            detail = VOID
            timeStamp = 0
            type = VOID
        });
    };
// ----------------------------------------------------------------------------------------------------------------------------
