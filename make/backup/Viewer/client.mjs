



// tool :: Viewer : create Viewer as global Device
// ----------------------------------------------------------------------------------------------------------------------------
    global
    ({
        Viewer: new Device("Viewer").extend
        ({
            drivers: new Choice().listen("change",(viewerChoice)=>
            {
                let event1 = viewerChoice.detail;
                evnt.parent.driver = evnt.value;

                System.listen("viewerChange",(viewerChange)=>
                {
                    let event2 = viewerChange.detail;

                    if (isFunc(evnt.parent.driver.vivify))
                    {
                        evnt.parent.driver.vivify();
                    };
                    // evnt.parent.driver.vivify({get:function(a1,a2)
                    // {
                    //     dump("Viewer: ",a1,a2);
                    // }});
                });

                System.signal("viewerChange", evnt.parent.driver);
            }),
        }),
    });
// ----------------------------------------------------------------------------------------------------------------------------




// defn :: Viewer.drivers.DOM : Document Object Model
// ----------------------------------------------------------------------------------------------------------------------------
    Viewer.drivers.define
    ({
        DOM: new Driver(document).extend
        ({
            create: function()
            {
                // let args = params( arguments );
                // this.signal("viewerCreate", args);
            },


            select: function()
            {

            },


            remove: function()
            {

            },
        })
    });
// ----------------------------------------------------------------------------------------------------------------------------




// defn :: Viewer.drivers.WGL : ThreeJS
// ----------------------------------------------------------------------------------------------------------------------------
    Viewer.drivers.define
    ({
        WGL: new Driver(document).extend
        ({
            vivify: function()
            {
                import("three").then(()=>
                {
                    dump("ThreeJS loaded");
                });
            },

            create: function()
            {

            },


            select: function()
            {

            },


            remove: function()
            {

            },
        })
    });
// ----------------------------------------------------------------------------------------------------------------------------
