

// shim :: (symbols) : local (secure) refs .. used inside this module context only
// ----------------------------------------------------------------------------------------------------------------------------
    const SECRET = System(CONFIG).secret;
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: HTMLElement : tools
// ----------------------------------------------------------------------------------------------------------------------------
    HTMLElement.prototype.Supply
    ({
        getStyle(attrib)
        {
            return getComputedStyle(this).getPropertyValue(attrib);
        },

        setStyle(object)
        {
            object.Indice().map((attrib)=>
            {
                this.style[attrib] = object[attrib];
            });
            return this;
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: String.parser.rgba : provides e.g. `String.prototype.Parsed("rgba", new Image())`
// ----------------------------------------------------------------------------------------------------------------------------
    String.parser.Assign
    ({
        rgba: function(string, result)
        {
            if (!result){ result = document.createElement("img") };  let pixels = [];

            string = ("<"+string.Parsed("lz64","encode")+">"); // unicode-safe string
            string = String.charKeys( string ); // create char-index from string

            string.map((number)=> // `number` is now an ascii decimal number representing a character in the base-64 charset
            {
                number.toString(16).toUpperCase().split("").map((char)=> // process each char in loop to avoid duplicate code
                { pixels.push( (30 + (parseInt(char,16) * 15)) ) }); // add char as colour value ... less is more
            });

            let length = (pixels.length / 4); // 4 = r,g,b,a = 1px
            let square = (((Math.floor(Math.sqrt(length)) -1) || 1) +1);
            let remain = (length % square);
            let canvas = document.createElement("canvas");
            let contxt = canvas.getContext("2d");
            let matrix;  if (remain){ square++ };

            matrix = contxt.createImageData(square, square);
            result.Modify({ width:square, height:square }); // safe result dimensions
            canvas.Modify(result.Gather("width","height")); // set canvas dimensions
            matrix.data.set(pixels,0);
            contxt.putImageData(matrix, 0, 0);
            result.Modify({src:canvas.toDataURL("image/png",1)})

            return result;
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: Object.parser.utf8.html : provides e.g:  HTMLImageElement.Parsed("utf8")
// ----------------------------------------------------------------------------------------------------------------------------
    Object.parser.Assign
    ({
        utf8(object, option)
        {
            if (!(object instanceof HTMLImageElement))
            { return Object.parser.auto(object,option) };

            let canvas = object.toCanvas();
            let contxt = canvas.getContext("2d");
            let pixels = [...(contxt.getImageData(0, 0, canvas.width, canvas.height).data)];
            let record = false;
            let string = "";

            pixels.Gather(2).map((code,char)=>
            {
                code = [Math.round((code[0]-30) / 15), Math.round((code[1]-30) / 15)];
                code = parseInt( (code[0].toString(16).toUpperCase()+""+code[1].toString(16).toUpperCase()), 16 );
                if ((code < 43) || (code > 122)){ return }else{ char = String.fromCharCode(code) }; // filter out noise
                if (!record && (char === "<")){ record = true; return };
                if (record && (char === ">")){ record = false; return };
                if (!record || (System(CONFIG).base64.indexOf(char)<0)){ return }; // record only when needed
                string += char;
            });

            string = string.Parsed("lz64","decode");
            return string;
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: HTMLImageElement : secrets
// ----------------------------------------------------------------------------------------------------------------------------
    HTMLImageElement.prototype.Supply
    ({
        toCanvas(impose=false)
        {
            let canv = document.createElement("canvas");
            let cntx = canv.getContext("2d");

            canv.width = this.naturalWidth;
            canv.height = this.naturalHeight;

            this.getAttributeNames().filter((attr)=>
            {
                if (attr === "src"){ return };
                canv.setAttribute(attr, this.getAttribute(attr));
            });

            cntx.drawImage(this, 0, 0, this.width, this.height);

            if (impose){ this.replaceWith(canv) };
            return canv;
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: hslToRgb : helpers
// ----------------------------------------------------------------------------------------------------------------------------
    HTMLElement.prototype.Supply
    ({
        fadeOut(time=3)
        {
            return new Promise((done,fail)=>
            {
                let opac = 1;
                let decr = ((1 / 100) / time);
                let timr = setInterval(()=>
                {
                    this.style.opacity = opac;
                    opac -= decr;
                    if (opac <= 0)
                    {
                        this.style.opacity = 0;
                        clearInterval(timr); done();
                    };
                },10);
            });
        },

        fadeIn(time=3)
        {
            return new Promise((done,fail)=>
            {
                let opac = 0;
                let incr = ((1 / 100) / time);
                let timr = setInterval(()=>
                {
                    this.style.opacity = opac;
                    opac += incr;
                    if (opac >= 1)
                    {
                        this.style.opacity = 1;
                        clearInterval(timr); done();
                    };
                },10);
            });
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------




// tool :: Client
// ----------------------------------------------------------------------------------------------------------------------------
    Aarden.Create(class Client extends Driver
    {
        constructor()
        {
            super(...arguments);
        }


        Parley(wyth)
        {

        }


        Permit(whom)
        {

        }


        Select(what)
        {

        }
    });
// ----------------------------------------------------------------------------------------------------------------------------




// tool :: Viewer : object
// ----------------------------------------------------------------------------------------------------------------------------
    Aarden.Create(class Viewer extends Sensor
    {
        constructor(target, config, memory)
        {
            target = (target||document.body);
            config = (config||{});

            super(target, memory, config);
            this[MEMORY] = new Memory();

            this.emit.Listen("idling",(signal)=>{ signal.preventDefault() });
            window.Convey("load,resize", this, {}, this[MINDER]);

            this.Listen("settle",(signal)=>
            {
                let atrace = signal.detail.Gather("action").Unique();

                if (atrace.includes("load"))
                {
                    if (this[ENTITY].loaded){ return };
                    this.Status();  this[ENTITY].loaded = true;
                    this.Signal("primed");
                };

                let length = [window.innerWidth, window.innerHeight];
                let oratio = (length[0] / length[1]);

                (this[TARGET].Select("[forcefit]")).map((object)=>
                {
                    let xratio = JSON.parse("["+object.getAttribute("forcefit").split(":").join(",")+"]");
                    if (oratio >= (xratio[0]/xratio[1])){ bgvidx.setStyle({width:"100%", height:"auto"}) }
                    else { bgvidx.setStyle({width:"auto", height:"100%"}) };
                });
            });

            this.emit("resize", {action:"resize", matter:"init"});
        }


        Status(what)
        {
            let length = [window.innerWidth, window.innerHeight];
            let shaped = ((length[0]===length[1]) ? "even" : ((length[0]>length[1]) ? "wide" : "tall"));
            let gcdnum = Math.gcd(length[0], length[1], 40);
            let aspect = [Math.trunc(length[0]/gcdnum), Math.trunc(length[1]/gcdnum)];
            let source = location.href;
            let result = {length, shaped, aspect, source};
            let minder = [...arguments].pop().Summon();

            minder[STATUS].Update(result);
            return (what ? result[what] : result);
        }


        Render(what)
        {

        }
    });
// ----------------------------------------------------------------------------------------------------------------------------




// tool :: ServerDriver : Server driver
// ----------------------------------------------------------------------------------------------------------------------------
    Aarden.Create(class Server extends Driver
    {
        constructor(target, config, sencon, memcon)
        {
            memcon = (memcon || {}).Supply
            ({
                dbase:"Server", store:"Memory",
                arise:{origin:location.hostname}
            });

            super(target, config, sencon, memcon);
        }


        async Access(target, config)
        {
            config = (config || {}).Supply({method:"GET", format:"blob", filter:null}); // for e.g: filter:function(){}
            return new Promise(function then(done,fail)
            {
                var subcon = config.Remove("format","filter");

                return fetch(target,config).then((result)=>
                {
                    var finish = function finish(data)
                    {
                        if (subcon.filter !== null){ data = subcon.filter(data) };
                        done( data );
                    };

                    result[subcon.format]().then((data)=>
                    {
                        var head = {};
                        for (let pair of result.headers.entries()){ head[ pair[0] ] = pair[1].split('\"').join("") };
                        if (!data.toDataURL){ finish( {head:head, body:data} ); return };
                        data.toDataURL().then((text)=>{ finish( {head:head, body:text} ) }, fail);
                    }, fail)
                });
            });
        }


        async Select(target)
        {
            return new Promise(function then(done,fail)
            {
                this[MEMORY].Select(target).then((memory)=>
                {
                    if (memory !== undefined){ memory = JSON.parse(memory); done(memory); return };

                    this.Access(target).then((result)=>
                    {
                        let urlend = target.split("?")[0].split("/").pop();
                        let retain = (!!urlend ); // check if request-url ended in a slash
                        if (retain){ this[MEMORY].Create(target,JSON.stringify(result)) }; // retain this in memory
                        done(result);
                    });
                });
            }
            .bind(this));
        }


        async Permit(whom)
        {

        }
    });
// ----------------------------------------------------------------------------------------------------------------------------
