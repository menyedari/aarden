

// boot :: strap : run a few checks to see if the platform is safe .. could be reverse engineered from error logs via extention
// ----------------------------------------------------------------------------------------------------------------------------
    "use strict"; // modules already run in strict mode ;)

    if ((typeof undefined) !== "undefined"){ throw 420 }; // https://httpstatusdogs.com/420-enhance-your-calm

    // TODO :: more tests .. e.g: spoof check
// ----------------------------------------------------------------------------------------------------------------------------




// func :: dump/moan : shorthands for `console.log` & console.error
// ----------------------------------------------------------------------------------------------------------------------------
    globalThis.dump = console.log.bind(console);
    globalThis.moan = console.error.bind(console);
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: (symbols) : local (secure) refs .. used inside this module context only
// ----------------------------------------------------------------------------------------------------------------------------
    const SYSTEM = Symbol("SYSTEM");
    const RELATE = Symbol("RELATE");
    const TETHER = Symbol("TETHER");
    const NORMAL = Symbol("NORMAL");
    const JACKED = Symbol("JACKED");
    const INVOKE = Symbol("INVOKE");
    const SECRET = Symbol("SECRET");
    const FINITE = Symbol("FINITE");
    const SECURE = Symbol("SECURE");
// ----------------------------------------------------------------------------------------------------------------------------




// func :: Sample : concise `typeof` .. parses e.g: `[Object Array]` .. returns partial string .. usage: `Sample([])` -> arra
// ----------------------------------------------------------------------------------------------------------------------------
    globalThis.Sample = function Sample(defn, need="/*", span=4)
    {
        if ((typeof need) == "number"){ span=need; need="/*" }; // convenience
        let part = Object.prototype.toString.call(defn).toLowerCase().slice(1,-1).split(" ");
        let prim = (this.prim[part[1]] || part[0]).slice(0,span);
        let seco = (this.seco[part[1]] || part[1]).slice(0,span);
        let full = (prim+"/"+seco);

        switch(need)
        {
            case "/*": return seco;
            case "*/": return prim;
        };

        return full;
    }
    .bind
    ({
        prim: {"undefined":"void", "null":"void"},
        seco: {"window":"main", "global":"main"},
    });



    Object.assign(Sample,
    {
        isMeta: function isMeta(what,seek)
        {
            if (!what || ((typeof what) != "object")){ return false }; // must be object

            let keys = Object.keys(what);
            let find = "value configurable enumerable writable get set apply has ownKeys";
            let seen = 0;  for (let item of keys){ if(find.includes(name)){ seen++ } };
            let echo = (seen > 0);

            return (!seek ? echo : (echo && (keys.indexOf(seek)>-1)));
        },


        metaConf: function metaConf(defn,opts)
        {
            // if (this.isMeta(defn)){ return defn };
            let indx = {C:"configurable", E:"enumerable", W:"writable"};
            let echo = {configurable:false, enumerable:false, writable:false, value:defn};
            if (((typeof opts) != "string") || !opts){ return echo };
            (opts+"").toUpperCase().split("").forEach((char)=>
            {
                let word = indx[char];
                if (!!word){ echo[word]=true };
            });

            return echo;
        },


        metaTrap: function metaTrap(defn,opts)
        {
            if (this.isMeta(defn)){ return defn };
            let indx = {G:"get", S:"set", A:"apply", H:"has", O:"ownKeys"};
            let echo = {};

            if (((typeof opts) != "string") || !opts){ opts="GSAHO" };
            ((opts||"")+"").toUpperCase().split("").forEach((char)=>
            {
                let word = indx[char];
                if (!!word){ echo[word]=defn };
            });

            return echo;
        },


        protoRoot: function protoRoot(what,find)
        {
            let prto = undefined;
            let cntx = undefined;
            let resl = undefined;
            let indx = 0;
            let type = (typeof find);
            do
            {
                prto = (Object.getPrototypeOf(prto||what) || (prto||what).constructor);
                if (!prto){ break }else{ resl = prto };
                cntx = prto.name;
                if (!cntx){ break };
                if (!!globalThis[cntx] && globalThis[cntx].toString().endsWith("{ [native code] }")){ break };
                if ((type == "number") && (indx===find)){ break };
                if ((type == "string") && (cntx===find)){ break };
                indx++;
            }
            while(cntx);
            return resl;
        },


        allKeys: function allKeys(trgt,fltr)
        {
            let resl = Object.getOwnPropertyNames(trgt);
            let type = (typeof fltr);
            let cntx = "";
            let prto = undefined;

            if ((type == "string") || Array.isArray(fltr))
            {
                fltr = function(item)
                {
                    return this.fltr.includes((typeof trgt[item]))
                }.bind({fltr:fltr})
            }
            else if (type != "function"){ fltr = null };

            do
            {
                prto = Object.getPrototypeOf(prto||trgt); if(!prto){ break };
                cntx = prto.constructor.name;
                if (!!globalThis[cntx] && (globalThis[cntx].toString().endsWith("{ [native code] }"))){ break };
                resl = resl.concat(Object.getOwnPropertyNames(prto)
                       .filter((item)=>{ return (resl.indexOf(item) < 0) }));
            }
            while(prto!==null);

            return (!fltr ? resl : resl.filter(fltr));
        },


        bind: function bind(th1s,that,resl={})
        {
            if (!that){ that = th1s };
            Object.keys(that).forEach((item)=>
            {
                if ((typeof that[item]) != "function"){ return }; // ignored
                resl[item] = that[item].bind(th1s);
            });

            return resl;
        },


        relate: function relate(target,config)
        {
            if (!target[RELATE]){ Object.defineProperty(target,RELATE,Sample.metaConf({})) };
            Object.assign(target[RELATE],config);
            return target;
        },


        modify: function modifyProperty(target, anonym, config)
        {
            let detail = target[anonym],  rename = (config.name || anonym);
            if (target.hasOwnProperty(anonym)){ delete target[anonym] };
            Object.defineProperty(target, rename, Sample.metaConf(config));
            // if (target instanceof Sensor){ target.emit("modify", {matter:anonym,  detail:config}) }
            // else if (target instanceof EventTarget){ target.dispatchEvent( new ActionEvent("modify", config) ) };
            return target;
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: Assign : akin to Object.defineProperties
// ----------------------------------------------------------------------------------------------------------------------------
    [Object,Function,Array,String,Number].map((origin)=>
    {
        Object.defineProperty(origin.prototype, "Assign", Sample.metaConf(function Assign(struct,config)
        {
            let entity = (this || globalThis); // Assign() could have been called without e.g: parent.Assign()
            // let matter = (typeof struct).slice(0,4);

            if (!!struct && !!struct.constructor && (struct.constructor.name!=="Object"))
            { struct = {[(struct.name||struct.constructor.name)]:struct} }; // enables e.g: Assign(function bark(){});

            Object.keys(struct).map((anonym)=>
            {
                let matter = (typeof struct[anonym]);
                if ((matter === "function") && !struct[anonym].name)
                { Object.defineProperty(struct[anonym],"name",Sample.metaConf(anonym)) };
                if ((["object","function"].indexOf(matter) > -1)){ Sample.relate(struct[anonym],{parent:entity}) };
                let detail = (Sample.isMeta(struct[anonym],"value") ? struct[anonym] : Sample.metaConf(struct[anonym],config));
                Object.defineProperty(entity, anonym, detail);
            });

            return entity;
        }
        .bind(origin.prototype)));
    });
// ----------------------------------------------------------------------------------------------------------------------------



// func :: Global : get/set global identifiers
// ----------------------------------------------------------------------------------------------------------------------------
    Assign(function Global(defn,opts)
    {
        let type = (typeof defn);

        if (type === "string")
        { return globalThis[defn] };

        if ((type === "object") && !defn.name)
        { return Assign(defn,opts) }

        if (!opts){ opts = Sample.metaConf(defn) };

        return Object.defineProperty(globalThis, defn.name, opts);
    });
// ----------------------------------------------------------------------------------------------------------------------------




// tool :: Config : object
// ----------------------------------------------------------------------------------------------------------------------------
    Global(class Config
    {
        prototype = {}

        constructor(config)
        {
            return this.Supply( (config || {}) );
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------




// tool :: Minder : object
// ----------------------------------------------------------------------------------------------------------------------------
    Global(class Minder extends Config
    {
        constructor(config)
        {
            super(config);
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: (symbols) : global references .. insecure - only use as syntactical reference
// ----------------------------------------------------------------------------------------------------------------------------
    Global
    ({
        CREATE: Symbol("CREATE"),
        DELETE: Symbol("DELETE"),

        INSERT: Symbol("INSERT"),
        SELECT: Symbol("SELECT"),

        TIMING: Symbol("TIMING"),
        NUMBER: Symbol("NUMBER"),

        NATIVE: Symbol("NATIVE"),
        REMOTE: Symbol("REMOTE"),

        SEARCH: Symbol("SEARCH"),
        TUNNEL: Symbol("TUNNEL"),

        DEVICE: Symbol("DEVICE"),
        DRIVER: Symbol("DRIVER"),

        IDLING: Symbol("IDLING"),
        GATHER: Symbol("GATHER"),

        MEMORY: Symbol("MEMORY"),
        CONFIG: Symbol("CONFIG"),
        RETAIN: Symbol("RETAIN"),

        LISTEN: Symbol("LISTEN"),
        EVENTS: Symbol("EVENTS"),
        IGNORE: Symbol("IGNORE"),
        SILENT: Symbol("SILENT"),

        INDICE: Symbol("INDICE"),
        VALUES: Symbol("VALUES"),
        SERIES: Symbol("SERIES"),

        ORIGIN: Symbol("ORIGIN"),
        ENTITY: Symbol("ENTITY"),
        DETAIL: Symbol("DETAIL"),
        SUMMON: Symbol("SUMMON"),
        TARGET: Symbol("TARGET"),
        SENSOR: Symbol("SENSOR"),
        RANDOM: Symbol("RANDOM"),
        MINDER: Symbol("MINDER"),
        RELAYS: Symbol("RELAYS"),
        CONVEY: Symbol("CONVEY"),
        MIRROR: Symbol("MIRROR"),
        MODULE: Symbol("MIRROR"),
        STATUS: Symbol("STATUS"),
    });
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: Math.gcd : greatest common divisor
// ----------------------------------------------------------------------------------------------------------------------------
    Math.gcd = function gcd(grater, lesser, smlest=3)
    {
        let result = (grater % lesser);
        return ( ((result<=smlest) ? lesser : Math.gcd(lesser, result, smlest)) );
    };
// ----------------------------------------------------------------------------------------------------------------------------




// tool :: Parser : global parser object .. to be extended and used via *.Parsed("method", target)
// ----------------------------------------------------------------------------------------------------------------------------
    Global(new class Parser
    {
        "stri" = new Object
        ({

        })
    });
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: Number.prototype : CRUD
// ----------------------------------------------------------------------------------------------------------------------------
    Number.prototype.Assign
    ({
        Pruned(maxi=3)
        {
            return (this.toFixed(maxi)*1);
        },
    });


    Number.Assign
    ({
        vector(mini=RANDOM, maxi=RANDOM, incr=RANDOM)
        {
            if (mini === RANDOM){ mini = Number.random(1,1000) };
            if (incr === RANDOM){ incr = Number.random(1, 100) };
            if (maxi === RANDOM){ maxi = Number.random((mini+(incr*100)), (mini+(incr*1000))) };

            let echo = [];

            while(mini <= maxi)
            {
                echo.push(mini);
                mini += incr;
            };

            return echo;
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------




// func :: Harden : set hard property
// ----------------------------------------------------------------------------------------------------------------------------
    Global
    ({
        Harden(o,k,v)
        {
            if(!o||!o.hasOwnProperty){return}; if(v==undefined){v=o[k]};
            let c={enumerable:false,configurable:false,writable:false,value:v};
            let r=true; try{Object.defineProperty(o,k,c);}catch(e){r=false};
            return r;
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: Function.prototype
// ----------------------------------------------------------------------------------------------------------------------------
    Function.prototype.Assign
    ({
        Modify: function Modify(anonym, config)
        {
            return Sample.modify(this, anonym, config);
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------




// func :: Strace : get filtered call-stack trace
// ----------------------------------------------------------------------------------------------------------------------------
    Global(function Strace(find, from=1)
    {
        let type = (typeof find);
        let host = (location.protocol+"//"+location.host);
        let omit = ['_fake_'],  resl = [];
        let stak = (new Error(".")).stack.split("\n");
        let fidx = 0; // find-index .. skipped logs may cause `indx` to misalign with `find`

        stak.shift(); // for the `.` error-line above .. now `Strace` is 1st .. see `from=1` above
        if (find === SYSTEM){ return stak };
        if (type === "function"){ return stak.filter(find) }; // filter was given

        for (let indx=0; indx<stak.length; indx++)
        {
            if (indx < from){ continue };
            if (((type === "number") && (fidx < find) && (resl.length > 0)))
            { continue }; // start searching from/find >= fidx

            let prts = stak[indx].trim().split(" ("),  rowx = {};
            if (!(prts[1]||"").startsWith(host)){ continue }; // not interested
            if (prts[0].startsWith("at ")){ prts[0]=prts[0].slice(3) };
            let extn = prts[1].split("/").pop().split(":")[0].split(".").pop();
            rowx.call = prts[0],  prts = prts[1].split(`.${extn}:`);
            rowx.file = (prts[0]+"."+extn),  prts = prts[1].split(":");
            rowx.line = (prts[0]*1);
            if ((type === "number") && (fidx === find)){ return rowx };
            if ((type === "string") && (rowx.call.includes(find))){ return rowx };
            resl.push(rowx); fidx++;
        };

        return ((type === "undefined") ? resl : undefined);
    });
// ----------------------------------------------------------------------------------------------------------------------------




// func :: Tunnel
// ----------------------------------------------------------------------------------------------------------------------------
    Global(function Tunnel(what, path, valu=SELECT, meta="CEW")
    {
        let trgt = what;
        let prts = path.split(".");
        let last = (prts.length - 1);
        let name, resl;

        for (let levl=0; levl<prts.length; levl++)
        {
            name = path[levl];
            if (levl < last){ trgt = trgt[name]; continue };
            if (valu === SELECT){ resl = trgt[name] }
            else if (valu === DELETE){ delete trgt[name]; resl = trgt }
            else
            {
                Object.defineProperty(trgt, name, metaConf(valu,meta));
                trgt[name] = valu; resl = trgt
            };
        };

        return resl;
    });
// ----------------------------------------------------------------------------------------------------------------------------




// tool :: Timing : object
// ----------------------------------------------------------------------------------------------------------------------------
    Global(new class Timing
    {
        constructor()
        {
            this[MEMORY] = Object.create({});
        }


        epoch(when, mili=false)
        {
            if ((typeof when)==="boolean"){ mili=when; when=undefined }; // fix args order
            let args = (when ? [when] : []);
            let resl = new Date(...args).getTime();
            return (mili ? resl : Math.trunc(resl/1000));
        }


        await(what, fast=1)
        {
            return new Promise(function then(done,fail)
            {
                let timr = String.random(12);
                let type = (typeof what);

                if (type === "number")
                {
                    this[MEMORY][timr] = setTimeout(()=>
                    {
                        clearTimeout(this[MEMORY][timr]); done();
                    },what);
                }
                else if (type === "function")
                {
                    this[MEMORY][timr] = setInterval(()=>
                    {
                        let resl = what();
                        if (!!resl){ clearInterval(this[MEMORY][timr]); done(resl) };
                    },fast);
                };
            }
            .bind(this));
        }


        every(what, runx)
        {
            let timr = String.random(12);
            let type = (typeof what);

            if (type === "number")
            {
                this[MEMORY][timr] = setInterval(()=>
                {
                    runx(this[MEMORY][timr]);
                },what);

                return this[MEMORY][timr];
            };

        }
    });
// ----------------------------------------------------------------------------------------------------------------------------




// tool :: lzTool : utf8-friendly string converter .. https://github.com/pieroxy/lz-string
// ----------------------------------------------------------------------------------------------------------------------------
    globalThis.Harden
    (
        globalThis, "lzTool", function lzTool(){function o(o,r){if(!t[o]){t[o]={};for(var n=0;n<o.length;n++)t[o][o.charAt(n)]=n}return t[o][r]}var r=String.fromCharCode,n="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",e="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-$",t={},i={zipToB64:function(o){if(null==o)return"";var r=i._zip(o,6,function(o){return n.charAt(o)});switch(r.length%4){default:case 0:return r;case 1:return r+"===";case 2:return r+"==";case 3:return r+"="}},dezipB64:function(r){return null==r?"":""==r?null:i._dezip(r.length,32,function(e){return o(n,r.charAt(e))})},zipToUTF16:function(o){return null==o?"":i._zip(o,15,function(o){return r(o+32)})+" "},dezipUTF16:function(o){return null==o?"":""==o?null:i._dezip(o.length,16384,function(r){return o.charCodeAt(r)-32})},zipToUint8Array:function(o){for(var r=i.zip(o),n=new Uint8Array(2*r.length),e=0,t=r.length;t>e;e++){var s=r.charCodeAt(e);n[2*e]=s>>>8,n[2*e+1]=s%256}return n},dezipUint8Array:function(o){if(null===o||void 0===o)return i.dezip(o);for(var n=new Array(o.length/2),e=0,t=n.length;t>e;e++)n[e]=256*o[2*e]+o[2*e+1];var s=[];return n.forEach(function(o){s.push(r(o))}),i.dezip(s.join(""))},zipToURI:function(o){return null==o?"":i._zip(o,6,function(o){return e.charAt(o)})},dezipURI:function(r){return null==r?"":""==r?null:(r=r.replace(/ /g,"+"),i._dezip(r.length,32,function(n){return o(e,r.charAt(n))}))},zip:function(o){return i._zip(o,16,function(o){return r(o)})},_zip:function(o,r,n){if(null==o)return"";var e,t,i,s={},p={},u="",c="",a="",l=2,f=3,h=2,d=[],m=0,v=0;for(i=0;i<o.length;i+=1)if(u=o.charAt(i),Object.prototype.hasOwnProperty.call(s,u)||(s[u]=f++,p[u]=!0),c=a+u,Object.prototype.hasOwnProperty.call(s,c))a=c;else{if(Object.prototype.hasOwnProperty.call(p,a)){if(a.charCodeAt(0)<256){for(e=0;h>e;e++)m<<=1,v==r-1?(v=0,d.push(n(m)),m=0):v++;for(t=a.charCodeAt(0),e=0;8>e;e++)m=m<<1|1&t,v==r-1?(v=0,d.push(n(m)),m=0):v++,t>>=1}else{for(t=1,e=0;h>e;e++)m=m<<1|t,v==r-1?(v=0,d.push(n(m)),m=0):v++,t=0;for(t=a.charCodeAt(0),e=0;16>e;e++)m=m<<1|1&t,v==r-1?(v=0,d.push(n(m)),m=0):v++,t>>=1}l--,0==l&&(l=Math.pow(2,h),h++),delete p[a]}else for(t=s[a],e=0;h>e;e++)m=m<<1|1&t,v==r-1?(v=0,d.push(n(m)),m=0):v++,t>>=1;l--,0==l&&(l=Math.pow(2,h),h++),s[c]=f++,a=String(u)}if(""!==a){if(Object.prototype.hasOwnProperty.call(p,a)){if(a.charCodeAt(0)<256){for(e=0;h>e;e++)m<<=1,v==r-1?(v=0,d.push(n(m)),m=0):v++;for(t=a.charCodeAt(0),e=0;8>e;e++)m=m<<1|1&t,v==r-1?(v=0,d.push(n(m)),m=0):v++,t>>=1}else{for(t=1,e=0;h>e;e++)m=m<<1|t,v==r-1?(v=0,d.push(n(m)),m=0):v++,t=0;for(t=a.charCodeAt(0),e=0;16>e;e++)m=m<<1|1&t,v==r-1?(v=0,d.push(n(m)),m=0):v++,t>>=1}l--,0==l&&(l=Math.pow(2,h),h++),delete p[a]}else for(t=s[a],e=0;h>e;e++)m=m<<1|1&t,v==r-1?(v=0,d.push(n(m)),m=0):v++,t>>=1;l--,0==l&&(l=Math.pow(2,h),h++)}for(t=2,e=0;h>e;e++)m=m<<1|1&t,v==r-1?(v=0,d.push(n(m)),m=0):v++,t>>=1;for(;;){if(m<<=1,v==r-1){d.push(n(m));break}v++}return d.join("")},dezip:function(o){return null==o?"":""==o?null:i._dezip(o.length,32768,function(r){return o.charCodeAt(r)})},_dezip:function(o,n,e){var t,i,s,p,u,c,a,l,f=[],h=4,d=4,m=3,v="",w=[],A={val:e(0),pos:n,index:1};for(i=0;3>i;i+=1)f[i]=i;for(p=0,c=Math.pow(2,2),a=1;a!=c;)u=A.val&A.pos,A.pos>>=1,0==A.pos&&(A.pos=n,A.val=e(A.index++)),p|=(u>0?1:0)*a,a<<=1;switch(t=p){case 0:for(p=0,c=Math.pow(2,8),a=1;a!=c;)u=A.val&A.pos,A.pos>>=1,0==A.pos&&(A.pos=n,A.val=e(A.index++)),p|=(u>0?1:0)*a,a<<=1;l=r(p);break;case 1:for(p=0,c=Math.pow(2,16),a=1;a!=c;)u=A.val&A.pos,A.pos>>=1,0==A.pos&&(A.pos=n,A.val=e(A.index++)),p|=(u>0?1:0)*a,a<<=1;l=r(p);break;case 2:return""}for(f[3]=l,s=l,w.push(l);;){if(A.index>o)return"";for(p=0,c=Math.pow(2,m),a=1;a!=c;)u=A.val&A.pos,A.pos>>=1,0==A.pos&&(A.pos=n,A.val=e(A.index++)),p|=(u>0?1:0)*a,a<<=1;switch(l=p){case 0:for(p=0,c=Math.pow(2,8),a=1;a!=c;)u=A.val&A.pos,A.pos>>=1,0==A.pos&&(A.pos=n,A.val=e(A.index++)),p|=(u>0?1:0)*a,a<<=1;f[d++]=r(p),l=d-1,h--;break;case 1:for(p=0,c=Math.pow(2,16),a=1;a!=c;)u=A.val&A.pos,A.pos>>=1,0==A.pos&&(A.pos=n,A.val=e(A.index++)),p|=(u>0?1:0)*a,a<<=1;f[d++]=r(p),l=d-1,h--;break;case 2:return w.join("")}if(0==h&&(h=Math.pow(2,m),m++),f[l])v=f[l];else{if(l!==d)return null;v=s+s.charAt(0)}w.push(v),f[d++]=s+v.charAt(0),h--,s=v,0==h&&(h=Math.pow(2,m),m++)}}};return i}()
    );
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: String : methods
// ----------------------------------------------------------------------------------------------------------------------------
    String.Assign
    ({
        random(span, plan)
        {
            if (this.name === "Number")
            {
                let mini = Math.ceil((span || 0));
                let maxi = Math.floor((plan || 9));
                return Math.floor(Math.random() * (maxi - mini + 1) + mini);
            };

            let from = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            let last = (from.length - 1);
            let resl = "";

            while(resl.length < span)
            { resl += from[ Number.random(0,last) ] };

            return resl;
        },


        charKeys(text)
        {
            let span = text.length;
            let resl = [];

            for (let x=0; x<span; x++)
            { resl.push(text.charCodeAt(x)) };

            return resl;
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: String.parser : parsing
// ----------------------------------------------------------------------------------------------------------------------------
    String.parser.Assign
    ({
        unde: function(text)
        {
            let keys = Object.keys(this); keys.shift();
            let numr = keys.length;
            let unde = "undefined";

            text = (text+"").trim(); // prep .. avoid issues
            if (!text || (text==="?") || (text.toLowerCase()===unde)){ return }; // undefined

            for (let indx=0; indx<numr; indx++)
            {
                let temp = this[keys[indx]](text);
                if ((temp !== text) && ((typeof temp) != unde))
                { return temp };
            };

            return text;
        },


        null: function(text)
        {
            text = (text+"").trim().toLowerCase();
            if (!text || (text==="null")){ return null }; // null
            return text;
        },


        bool: function(text)
        {
            text = (text+"").trim().toLowerCase();
            if (!text){ return }; // undefined

            let yay = "+ y on yes true";
            let nay = "- n off no false";

            if (yay.indexOf(text) > -1){ return true };
            if (nay.indexOf(text) > -1){ return false };

            return text;
        },


        numr: function(text)
        {
            let temp = (text * 1);
            return (!isNaN(temp) ? temp : text);
        },


        blob: function(text)
        {
            text = (text+"").trim();
            if (!text.startsWith("data:") || !text.includes(";base64,")){ return text }; // not data-url
            let resl = document.CreateElement("object");
            let mime = text.split(";base64,")[0].split("data:").pop();
            resl.setAttribute("type", mime);
            resl.setAttribute("data", text);
            return resl;
        },


        func: function(text)
        {
            text = (text+"").trim();
            if (!text.startsWith("function(") && !text.startsWith("function ")  && !text.startsWith("(") ){ return text };
            if (!text.endsWith("}") && !text.endsWith(")") && !text.endsWith(");") && !text.endsWith("};")){ return text };
            try
            {
                let fail = String.random(9);
                let resl = (Function(" try{ return ("+text+"); }catch(fail){ moan(fail); return '"+fail+"'; }"))();
                if (resl === fail){ moan("...\n",text); return };
                return resl;
            }
            catch(fail)
            { moan(fail); dump(text); };
            return text;
        },


        obje: function(text, resl={})
        {
            try { let temp=JSON.parse(text); resl=resl.assign(temp) }
            catch(fail)
            {
                text.split("\n").join(",").split(";").join(",").split(",").forEach((valu)=>
                {
                    valu = valu.split(":");
                    if (valu.length < 2){ return };
                    if (valu[1].slice(0,2)==="//"){ return };

                    name = valu.shift().trim();
                    valu = this.unde(valu.join(":"));
                    resl[name] = valu;
                });

                if (Object.keys(resl).length > 0){ return resl };
            };
            return text;
        },


        qatr: function(text, resl={})
        {
            let attr = 0;
            text.split(" ").map((item)=>
            {
                let char = item.slice(0,1); item=item.slice(1);
                if((char === "#")){ resl.id=item; resl.name=item; attr++ }
                else if(char === ".")
                {
                    if(!resl.class){ resl.class="" };
                    resl.class = resl.class.trim().split(" ").Supply(item).join(" ").trim();
                    attr++
                };
            });

            if (!!attr){ return resl };
            resl.$ = text;
            return resl;
        },


        arra: function(text, resl=[])
        {
            try
            {
                temp = JSON.parse(text);
                resl = resl.concat(temp);
                return resl;
            }
            catch(fail)
            {
                if (!text.includes(",")){ return text };
                text.split(",").forEach((item)=>{ resl.push(this.unde(item)) });
                return resl;
            };
        },


        lz64: function (string, choice="encode")
        {
            let result = "";

            if (choice === "encode")
            {
                result = lzTool.zipToB64(string);
                while (result.endsWith("=")){ result = result.slice(0,-1) }; // if something goes wrong - comment out this
                return result;
            };

            if (choice === "decode")
            {
                result = lzTool.dezipB64(string);
                return result;
            };

            return string;
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: String.prototype : convert string into something useful
// ----------------------------------------------------------------------------------------------------------------------------
    String.prototype.Assign
    ({
        Indice(number)
        {
            let detail = Object.keys(this);
            if (number === undefined){ return detail };
            if (number < 0){ number = (detail.length - 1) };
            return detail[number];
        },



        Values(number)
        {
            let detail = Object.values(this);
            if (number === undefined){ return detail };
            if (number < 0){ number = (detail.length - 1) };
            return detail[number];
        },



        Parsed(parser, result)
        {
            parser = (((typeof parser)==="string") ? parser : Sample(parser));
            result = (String.parser[parser] ? (String.parser[parser](this,result) || result) : result);
            return result;
        },



        Unwrap(bgns,ends)
        {
            if (ends === undefined)
            {
                ends = bgns.slice((bgns.length/2));
                bgns = bgns.slice(0,(bgns.length/2));
            };

            let resl = (this+"");
            let bpos = resl.indexOf(bgns);
            let epos = resl.lastIndexOf(ends);

            if ((bpos<0) || (epos<0) || (bpos===epos))
            { return resl }; // nothing to do

            return resl.slice((bpos+bgns.length),epos);
        },



        Gather(arg1, arg2)
        {
            let typ1 = (typeof arg1);
            let typ2 = (typeof arg2);
            let text = (this+"");
            let span = text.length;
            let resl = [];

            if (span < 1){ return resl }; // expected to return array

            if (typ1 === "function") // filter
            {
                return resl.filter(arg1);
            };

            if ((typ1 === "number") && (typ2 === "undefined")) // chunk by number
            {
                for (let indx=0; indx<span; indx+=arg1)
                {
                    resl.push( text.slice(indx,(indx+arg1)) )
                };
                return resl;
            };

            if ((typ1 === "string") && (typ2 === "string")) // expose between strings
            {
                let blen = arg1.length, elen = arg2.length, bpos,epos,chnk,xpos=0;
                do
                {
                    bpos = text.indexOf(arg1,xpos);         if (bpos < 0){ return resl }; // bpos not found .. bpos needed next
                    epos = text.indexOf(arg2,(bpos+blen));  if (epos < 0){ return resl }; // ending not found ...
                    chnk = text.slice((bpos+blen), epos);   xpos = (epos+elen); // bpos must search from after epos+elen
                }
                while((bpos > -1) && (epos > -1));
                return resl;
            }
        },



        Invert(deep=false)
        {
            if (!deep){ return (this+"").split("").reverse().join("") };
            return String.charKeys(this+"");
        },



        Select(what, sliceB, sliceE)
        {
            let type = (typeof what);
            let text = (this+"");

            if (text.length < 1){ return "" }; // nothing to do

            if (type === "number")
            {
                return text.slice(...arguments);
            }


            if (type === "string")
            {
                if (what === "*"){ return text }; // all
                let args = [...arguments];

                if (!args.join("").includes("*"))
                {
                    return ((args.indexOf(text) > -1) ? text : "");
                }
                // { return ((text===what)?text:"") }; // match exact

                sliceB = (sliceB || 0);
                sliceE = (sliceE || text.length);

                if (what.startsWith("*") && what.endsWith("*")) // includes .. e.g: `("hello").Select("*el*")` -> hello
                {
                    what = what.slice(1,-1);
                    return (text.includes(what) ? text.slice(sliceB,sliceE) : "");
                };

                if (what.endsWith("*")) // begins-with (reversed) .. e.g: `("hello").Select("he*")` -> hello
                {
                    what = what.slice(0,-1);
                    return (text.startsWith(what) ? text.slice(sliceB,sliceE) : "");
                };

                if (what.startsWith("*")) // ends-with (reversed) .. e.g: `("hello").Select("*lo")` -> hello
                {
                    what = what.slice(1);
                    return (text.endsWith(what) ? text.slice(sliceB,sliceE) : "");
                };
            };

            return "";
        },


        Locate()
        {
            let args = [...arguments];
            let find = args.length;
            let seen = 0;

            if (find < 2) // single argument
            {
                return this.indexOf(args[0]);
            };

            args.map((search)=> // multiple arguments
            {
                if (this.indexOf(search) > -1){ seen++ };
            });

            return (seen / find); // return e.g:  0 is none .. 1 is all .. 0.7 is some
        },


        Rotate(next=1, what=NORMAL)
        {
            if ((what!==SECRET)&&(what!==SYSTEM)){ what=NORMAL };

            let text = (this+"");
            let resl = (((what===SECRET)||(what===SYSTEM)) ? String.charKeys(text) : text.split("") );

            if (what===NORMAL)
            { return resl.Rotate(next).join("") };

            resl = resl.map((numr,indx)=>
            { return String.fromCharCode(numr+next) });

            return resl.join("");
        },


        Prefix()
        {
            return [...this].Prefix(...[arguments]);
        },


        Suffix()
        {
            return [...this].Suffix(...[arguments]);
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: Array.prototype : CRUD
// ----------------------------------------------------------------------------------------------------------------------------
    Array.prototype.Assign
    ({
        Indice(number)
        {
            let detail = Object.keys(this);
            if (number === undefined){ return detail };
            if (number < 0){ number = (detail.length - 1) };
            return detail[number];
        },



        Values(number)
        {
            let detail = Object.values(this);
            if (number === undefined){ return detail };
            if (number < 0){ number = (detail.length - 1) };
            return detail[number];
        },



        Invert(deep=false, bool)
        {
            if (!deep){ return this.reverse() };
            return Object.entries(this).reduce((acc, [key, value]) => (acc[value] = (bool ? true : key), acc), {});
        },



        Rotate(next=1)
        {
            let todo = (next * ((next<0) ? -1 : 1));
            for (todo; (todo > 0); todo--)
            {
                if (next>0){ this.unshift(this.pop()) }
                else{ this.push(this.shift()); }
            };
            return this;
        },



        Insert(what, indx)
        {
            if (isNaN(indx)){ this[this.length]=what; return this };
            if (indx < 0){ indx = (this.length + indx) };
            this.splice(indx,0,what);
            return this;
        },



        Unique(from=[])
        {
            let item, data = this.concat(from), echo = [];

            for (item of data)
            {
                if ( ((echo.indexOf(item) < 0)) )
                { echo.push(item) }
            };

            return echo;
        },



        Differ(from)
        {
            let item, data = this.Unique(from), echo = [];

            for (item of data)
            {
                if ( ((this.indexOf(item) < 0)) )
                { echo.push(item) }
            };

            return echo;
        },



        Supply()
        {
            [...arguments].map((item)=>
            {
                if (this.indexOf(item) < 0){ this[this.length]=item } // no duplicates
            });

            return this;
        },


        Locate(what)
        {
            let args = [...arguments];
            let find = args.length;
            let seen = 0;

            if (find < 2) // single argument
            {
                return this.indexOf(what);
            };

            args.map((search)=> // multiple arguments
            {
                if (this.indexOf(search) > -1){ seen++ };
            });

            return (seen / find); // return e.g:  0 is none .. 1 is all .. 0.7 is some
        },



        Select(what)
        {
            let type = Sample(what);
            let echo = [];
            let args = [...arguments];

            if (this.length < 1)
            { return [] }; // nothing to do


            if (type === "numb")
            {
                if (what < 0){ what=(this.length+what) };
                echo = ((args.length===1) ? this[what] : this.slice(...args));
                return (Array.isArray(echo) ? echo : [echo]);
            };


            if (type === "stri")
            {
                this.map((item)=>
                {
                    if (!item || ((typeof item)!=="string")){ return };
                    item = item.Select(...args);
                    if (!!item){ echo.push(item) };
                });

                return echo;
            };


            if (type === "func")
            {
                this.map((item,indx)=>
                {
                    item = what(item,indx);
                    if (item !== undefined){ echo.push(item) };
                });
                return echo;
            };


            return echo;
        },



        Remove(indx)
        {
            if (isNaN(indx)){ return this };
            if (indx < 0){ indx = (this.length + indx) };
            this.splice(indx,1);
            return this;
        },


        Prefix()
        {
            let params = [...arguments],  result = [...this];
            if (params.length < 1){ return this[0] };

            if (Array.isArray(params[0])){ params = params[0]};
            while ((params.indexOf(result[0]) > -1) && (result.length>0)){ result.shift() };
            return params.concat(...result);
        },


        Suffix()
        {
            let params = [...arguments],  result = [...this];
            if (params.length < 1){ return this[ (this.length-1) ] };

            if (Array.isArray(params[0])){ params = params[0]};
            while ((params.indexOf(result[result.length-1]) > -1) && (result.length>0)){ result.pop() };
            return result.concat(...params);
        },



        Gather(arg1, arg2)
        {
            let typ1 = (typeof arg1);
            let typ2 = (typeof arg2);
            let matr = (typ1.slice(0,4)+"/"+typ2.slice(0,4));
            let span = this.length;
            let resl = [];

            if (matr === "numb/unde")
            {
                for (let indx=0; indx<span; indx+=arg1)
                { resl.push( this.slice(indx,(indx+arg1)) ) };
                return resl;
            };

            if (matr === "numb/unde")
            {
                for (let indx=0; indx<span; indx+=arg1)
                { resl.push( this.slice(indx,(indx+arg1)) ) };
                return resl;
            };

            if (matr === "stri/unde")
            {
                for (let indx=0; indx<span; indx+=1)
                {
                    resl.push( this[indx][arg1] )
                };
                return resl;
            };
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: Object.parser : utf8 .. convert object to string .. for e.g:  object.Parsed("utf8")
// ----------------------------------------------------------------------------------------------------------------------------
    // Object.parser.auto = function auto(object, option)
    // {
    //     let format = Sample(object);
    //     return (Object.parser[format] ? Object.parser[format](object,option) : (object+""));
    // };
    //
    // Object.parser.obje = function obje(object){ return JSON.stringify(object) };
    // Object.parser.func = function func(object){ return object.toString() };
    // Object.parser.html = function html(object){ return object.outterHTML };

    // Object.parser.Assign
    // ({
    //     auto(object, option)
    //     {
    //         let format = Sample(object);
    //         return (Object.parser[format] ? Object.parser[format](object,option) : (object+""));
    //     },
    //
    //     obje(object){ return JSON.stringify(object) },
    //     func(object){ return object.toString() },
    //     html(object){ return object.outterHTML },
    // });
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: Object.prototype : CRUD
// ----------------------------------------------------------------------------------------------------------------------------
    Object.prototype.Assign
    ({
        Indice(number)
        {
            let detail = Object.keys((this||globalThis));
            let format = (typeof number).slice(0,4);

            if (format === "unde"){ return detail };

            if (format === "numb")
            {
                if (number < 0){ number = (detail.length - 1) };
                return detail[number];
            };

            if (format === "stri")
            {
                return detail.Select(...arguments);
            };
        },



        Values(number)
        {
            let detail = Object.values((this||globalThis));
            let format = (typeof number).slice(0,4);

            if (format === "unde"){ return detail };

            if (format === "numb")
            {
                if (number < 0){ number = (detail.length - 1) };
                return detail[number];
            };

            if (format === "stri")
            {
                return detail.Select(...arguments);
            };
        },



        Create(config)
        {
            let entity = (this || globalThis),
                contxt = Sample(entity),
                matter = Sample(config),
                filter = (contxt +"/"+ matter),
                result, design, family, sprout;

            if (matter === "unde")
            {
                return ((contxt==="html") ? (new DocumentFragment()) : {}).Relate({parent:entity});
            };


            if ((filter === "obje/func") && config.name)
            {
                config = {[config.name]:config};
                filter = "obje/obje"; // works for functions and classes .. next: "obje/obje"
            };


            if (filter === "obje/obje")
            {
                config.Indice().map((anonym)=>
                {
                    this[anonym] = config[anonym];
                    return this;
                });
            };


            if (filter === "html/stri")
            {
                result = document.createElement(config);
                return result.Relate({parent:entity});
            };


            if (filter === "html/arra")
            {
                result = (new DocumentFragment()).Insert(config);
                return result;
            };


            if (filter === "html/obje")
            {
                design = config.Remove(0);
                family = design.Indice(0);
                sprout = (config.Remove("$","children") || {});
                config = design[family].Parsed("qatr").Supply(config);
                sprout = (sprout.Remove("$") || config.Remove("$") || {}).$;
                result = document.createElement(family).Modify(config).Insert(sprout);;
                return result;
            };
        },


        Update(config)
        {
            let entity = (this || globalThis),
                contxt = Sample(entity),
                matter = Sample(config),
                filter = (contxt +"/"+ matter);

            if (filter === "obje/obje")
            {
                config.Indice().map((anonym)=>
                {
                    this[anonym] = config[anonym];
                    return this;
                });
            };
        },


        Modify(struct, config)
        {
            let entity = (this || globalThis);
            let matter = Sample(entity);
            let format = Sample(struct);
            let filter = (matter +"/"+ format);
            let noAttr = "innerHTML textContent";
            let obSwap = {}; //  class:"className"

            if (filter === "obje/stri")
            { return Sample.modify(entity, struct, config) };

            if (filter === "html/obje")
            {
                for (let anonym of (Object.keys(struct)))
                {
                    if ((noAttr.indexOf(anonym) < 0) && ("number,string,boolean").includes(typeof struct[anonym]))
                    { entity.setAttribute(anonym, struct[anonym]) } // attribute
                    if (!entity[anonym]){ entity[anonym] = struct[anonym] }; // property
                };
                return entity;
            };

            moan(`undefined handler for: "${filter}"`);
            return entity;
        },



        Insert(detail)
        {
            let entity = (this||globalThis);
            let params = [...arguments];
            let matter = Sample(entity);

            if ((params.length < 2) && (matter==="html") && ((detail instanceof NodeList)||(detail instanceof HTMLCollection)))
            { detail = [...detail] };  if (Array.isArray(detail) && (params.length < 2)){ params = [...detail] };

            if (matter !== "html")
            {
                params.map((item)=>{ entity.Relate({sprout:item}) });
                return entity;
            };

            params.map((item)=>
            {
                let type = Sample(item);
                if (type === "obje")
                { entity.appendChild(entity.Create(item)) }
                else if (type === "html")
                { entity.appendChild(item) }
                else if (type !== "unde")
                { entity.innerHTML = Object.parser.auto(item) };
            });

            return entity;
        },



        Relate(detail)
        {
            let matter = (typeof detail);
            let result = (this[RELATE] || {});
            if (matter==="string"){ return result[detail] };
            if (matter==="object"){ return Sample.relate(this,detail) };
            return result;
        },



        Remove()
        {
            let entity = (this || globalThis);
            let params = [...arguments];
            let number = 0;
            let family = Sample(entity);
            let holder = ((family==="html") ? entity : Object.keys(entity)).Select(...params);
            let result = ((family==="html") ? [] : {});
            if ((family==="html") && !params[0]){ holder=[entity] };

            holder.map((struct)=>
            {
                if (family==="html")
                {
                    result.push(struct);
                    struct.parentNode.removeChild(struct);
                }
                else
                {
                    result[struct] = entity[struct];
                    delete entity[struct];
                };
                number++;
            });

            return ((number > 0) ? result : null);
        },



        Deploy()
        {
            var todo = arguments.length;
            var done = 0;

            [...arguments].map((object)=>
            {

            });

            return (todo / done).Pruned();
        },



        Supply(struct)
        {
            Reflect.ownKeys(struct).map((name)=>
            {
                if ((this[name] === undefined) || (this[name] === null) )
                { this[name] = struct[name] };
            });

            return this;
        },



        Locate(what)
        {
            if ((typeof what)==="string"){ what = what.Parsed({keys:what}) };
            let data = (what.keys ? Object.keys(this) : Object.values(this));
            let find = (what.keys || what.vals);
            if (!Array.isArray(find)){ find=[find]; }
            return data.Locate(...find); // return e.g:  0 is none .. 1 is all .. 0.7 is some
        },



        Gather()
        {
            let result = {};
            [...arguments].map((select)=>
            {
                result[select] = this[select];
            });

            return result;
        },



        Parsed(parser, result)
        {
            parser = (((typeof parser)==="string") ? parser : Sample(parser));
            result = (Object.parser[parser] ? (Object.parser[parser](this,result) || result) : result);
            return (result || (this+""));
        },



        Search(what)
        {
            let keys = Object.keys(this);
            for (let name of keys)
            {
                if (!Array.isArray(this[name])){ continue };
                if (this[name].indexOf(what) > -1){ return name };
            };
        },



        Select(detail)
        {
            let entity = (this||globalThis),
                params = [...arguments],
                family = Sample(entity),
                caller = (typeof detail).slice(0,4),
                indice, record, anonym,
                result = []; // expected result

            if ((caller === "numb") && (params.length === 1))
            {
                indice = Object.keys(entity);
                if (detail < 0){ detail=(indice.length+detail) };
                anonym = indice[detail];  if (!anonym){ return result };
                result = {[anonym]:entity[anonym]};
                return result;
            };


            if ((caller === "stri") && (family === "html") && !params[0].Locate("keys:","vals:"))
            {
                return [...entity.querySelectorAll(...params)];
            };


            if (caller === "stri")
            {
                let expr = (params[0].includes(":") ? params[0] : ("pick:"+params[0]));
                let part = expr.split(":");
                let find = part[0];  expr=part[1];  params[0]=expr;
                let keys = Object.keys(entity);

                if (find === "pick")
                {
                    let rowx = {},  done = 0;
                    keys.Select(...params).map((item)=>{ rowx[item] = entity[item]; done++ });
                    if (done > 0){ result.push(rowx) };
                    return result;
                };

                if (find === "keys")
                {
                    return keys.Select(...params);
                };

                if (find === "vals")
                {
                    return Object.values(this).Select(...params);
                };
            };


            return result;
        },

    });
// ----------------------------------------------------------------------------------------------------------------------------




// func :: System : private resource
// ----------------------------------------------------------------------------------------------------------------------------
    Global
    ({
        System: function System(farg, sarg)
        {
            if (!Strace(0)){ throw 420; return }; // security
            if ((typeof sarg) !== "undefined"){ farg={[farg]:sarg} };

            switch (typeof farg)
            {
                case "symbol" : return this[farg];
                case "string" : return Tunnel(this,farg,sarg);
                case "object" : ((keys)=>
                {
                    for (let indx=0; indx<keys.length; indx++)
                    {
                        if ((typeof keys[indx])==="symbol"){ this[keys[indx]] = farg[indx] }
                        else { Tunnel(this, keys[indx], farg[indx], null) };
                    };
                    return true;
                })
                (Reflect.ownKeys(farg));
            }

            moan("expecting any: symbol, string, object");
        }
        .bind(Object.create
        ({
            [ MEMORY ]: Object.create(null,{}),
            [ INVOKE ]: Object.create(null,{}),
            [ JACKED ]: Object.create(null,{}),
            [ MODULE ]: Object.create(null,{}),

            [ CONFIG ]: Object.create
            ({
                secret: SECRET, // gongrats! .. you've made it .. now wut?  ╭∩╮( ︶ヘ︶)╭∩╮
                base64: "+/0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz",
                base16: "0123456789ABCDEF",
            }),

            [ EVENTS ]: (((typeof window)==="undefined") ? [] : window.Select("keys:on*",2)
                        .Supply(Document.prototype.Select("keys:on*",2))
                        .Supply("select","create","modify","insert","update","delete")
                        .Supply("signal","handle","mutate","absorb","gather","settle","primed","idling","failed","closed")),
        }))
    });
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: CustomEvent.prototype
// ----------------------------------------------------------------------------------------------------------------------------
    CustomEvent.prototype.Assign
    ({
        Cancel(levl=2)
        {
            ["preventDefault", "stopPropagation", "stopImmediatePropagation"].forEach((meth,indx)=>
            {
                if (indx <= levl){ this[meth]() }
            });
            return this;
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: Object.prototype.Hijack
// ----------------------------------------------------------------------------------------------------------------------------
    Object.prototype.Assign
    ({
        Hijack(target, minder, pliant=false, dejavu)
        {
            if ((typeof target)==="object")
            {
                target.Indice().map((action)=>{ this.Hijack(action,target[action],pliant,JACKED) });
                return this;
            };

            if ((dejavu!==JACKED) && !Strace(0)){ throw 420; return };

            minder = new Object
            ({
                [target]: (this||globalThis)[target].bind(this||globalThis),
                handle: target,
                secret: SECRET,
                parent: (this||globalThis),
                minder: minder,
                config: System(CONFIG),
            });
            minder.minder = minder.minder.bind(minder);

            Object.defineProperty(minder.parent, target,
            {
                configurable: pliant,
                enumerable: true,
                set: function set(){ return false },
                has: function has(){ return true },
                get: function get()
                {
                    return (!Strace(1) ? this[this.handle] : this.minder)
                }
                .bind(minder),
            });

            return this;
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: Function.prototype.Absorb :
// ----------------------------------------------------------------------------------------------------------------------------
    Function.prototype.Assign
    ({
        Convey()
        {
            return "TODO :: develop : this is not implemented yet";
        },


        Absorb(idling=60, ovrflo)
        {
            let contxt = (new EventTarget()).Supply
            ({
                idling:idling, ovrflo:ovrflo, finish:null, buffer:[], action:this,
                parent:this.Relate("parent"),

                Gather(number)
                {
                    let diff,span,frst,heap;
                    diff = ((number < 0) ? (number*-1) : number),  span = this.buffer.length;
                    if (diff > span){ return [] };
                    frst = ((number < 0) ? (span+number) : 0),  heap = this.buffer.splice(frst,span);
                    if (heap.length < 1){ return [] };
                    if (number < 0){ heap = heap.reverse() };
                    this.dispatchEvent( new AbsorbEvent("gather",heap) );
                    // this.Signal("Gather", heap);
                    return heap;
                },
                Absorb()
                {
                    let bufr;  clearTimeout(this.finish);
                    this.finish = setTimeout(()=>
                    {
                        if (this.ovrflo){ this.Gather(this.buffer.length) };
                        this.dispatchEvent( new AbsorbEvent("settle",this.buffer) );
                        this.buffer = [];
                    }, this.idling);

                    try { bufr = this.action(...[...arguments,this]) }
                    catch(fail) { bufr = {matter:(this.action.name||"anonymous"), detail:fail} };

                    this.dispatchEvent( new AbsorbEvent("absorb",bufr) );
                    this.buffer.push(bufr);
                    if (!this.ovrflo){ return }; // keep this here .. for heap-collect feature in future
                    this.Gather(this.ovrflo || this.buffer.length); // -and also this
                }
            });

            contxt.Relate({parent:parent});
            contxt.Absorb = contxt.Absorb.bind(contxt); // what you're about to see may haunt your dreams ..
            contxt.Absorb.prototype = contxt; // ssshhh .. look away quickly .. too late .. sweet screams :D
            Object.setPrototypeOf(contxt.Absorb, contxt);

            contxt.addEventListener("idling", (signal)=>
            {
                Timing.await(0).then(()=>
                {
                    if (signal.defaultPrevented){ return };
                    contxt.dispatchEvent( new AbsorbEvent("primed",contxt) );
                });
            },{once:true});

            Timing.await(0).then(()=> // this may be fired before a listener was assigned, so we wait a bit
            {
                contxt.dispatchEvent( new AbsorbEvent("idling",contxt) );
            });

            return contxt.Absorb;
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: Blob.toDataURL :
// ----------------------------------------------------------------------------------------------------------------------------
    Blob.prototype.Assign
    ({
        toDataURL: function toDataURL()
        {
            return new Promise((done,fail)=>
            {
                let reader = new FileReader();
                reader.onload = function(evnt){ done(evnt.target.result) };
                reader.onerror = function(evnt){ fail(evnt) };
                reader.readAsDataURL(this);
            });
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------




// tool :: Signal : constructor
// ----------------------------------------------------------------------------------------------------------------------------
    Global(class Signal extends CustomEvent
    {
        constructor(type, data, conf)
        {
            conf = (conf || new Config()).Supply({name:type, bubbles:true, cancelable:true, detail:data});
            super(type, conf);
            // return echo;
        }
    });

    Global(class MutateEvent extends Signal{});
    Global(class ActionEvent extends Signal{});
    Global(class AbsorbEvent extends Signal{});
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: EventTarget.prototype : Listen/Signal .. e.g: dog.Listen("bark", ()=>{}, {once:true});
// ----------------------------------------------------------------------------------------------------------------------------
    EventTarget.prototype.Assign
    ({
        Listen: function(signal, invoke, config, minder)
        {
            let entity = (this || globalThis);
            let family = Sample(signal);
            let sensor = (entity instanceof Sensor);
            let events, target, absorb, mutate, htnode, convey=false;

            if (family === "func") // for e.g: EventTarget.Listen( ()=>{ return true }, ()=>{} ); .. .. from here just ET
            {
                if ((typeof invoke) !== "function"){ return Timing.await(signal) }; // for e.g: ET.Listen(()=>{}).then(()=>{});
                Timing.await(signal).then(invoke); return entity;   // for e.g: ET.Listen( ()=>{}, ()=>{} ).Listen( ... );
            };

            entity = (((typeof entity) === "function") ? entity.prototype : entity); // for: Function.prototype.Absorb <- find

            minder = [...arguments].Suffix(); // get the last item in the list .. which could be the minder from nosync/insync
            if (!minder || !minder.Summon){ minder = undefined } // no minder .. but we have to clear it out .. just in case
            else { minder = minder.Summon(); target = minder[TARGET] }; // thank you Wraith <- find

            if ((family === "stri") && signal.includes(":"))
            {
                signal = signal.Parsed("obje",{});
                config = (config || {}).Supply(signal.Values().Invert(true,true));
                signal = signal.Indice(0);
            };

            absorb = (config||{}).Remove("absorb","gather","settle"); // for e.g: EventTarget.Listen("resize:settle")

            // if (sensor && minder && ((typeof invoke)==="undefined"))
            // {
            //     convey = true;
            //     invoke = function convey(bubble)
            //     {
            //         // dump(bubble.type);
            //         this.minder.emit(bubble.type, {matter:bubble.type, detail:bubble});
            //     }
            //     .bind({minder:minder});
            // };


            // if (!!minder && !(invoke.name+"").startsWith("bound "))
            // { invoke = invoke.bind(minder) };

            if (family === "stri")
            {
                htnode = (((typeof HTMLElement)!=="undefined") && (target instanceof HTMLElement));
                mutate = ["mutate"];
                events = (!signal.includes("*") ? [signal] : entity.Select("keys:"+signal).Supply(System(EVENTS)));
                events = events.Select(signal);

                // if (!!absorb)
                // { convey = true; };

                if (!convey && (entity instanceof EventTarget))
                { events.map((matter)=>{ entity.addEventListener(matter, invoke, config) }) };

                if (!!target && (target instanceof EventTarget))
                { target.addEventListener(signal, invoke, config) };

                if (!target || !minder || !htnode || (events.Locate(...mutate)<0))
                { return this }; // no mutation infered directly .. nothing further to do

                if (!!sensor && !!minder && !!minder[TARGET] && !!minder[TARGET].Listen) // there may be mutation implied
                { target = minder[TARGET] } else { target = entity }; // we need it to do as implied
                if (!(target instanceof HTMLElement)){ return this }; // end of the line .. no mutations
                if (!!target[LISTEN]){ target[LISTEN].cancel() }; // woa! - safety first!! - prevent accidents

                target[LISTEN] = new Object
                ({
                    matter: "mutate",
                    entity: entity,
                    target: target,
                    minder: minder,
                    config: { attributes:true, characterData:true, childList:true, subtree:true },
                    cancel: function cancel(){ this.tether.disconnect(); delete this.target[LISTEN] },
                    invoke: function invoke(list)
                    {
                        let record = [];
                        for (const muta of list){ record.Supply((this.config[muta.type] ? muta : undefined)) };
                        record.map((object)=>
                        {
                            let convey = {matter:object.type, detail:object};
                            if (!this.minder || !this.minder.emit)
                            { this.entity.dispatchEvent( (new MutateEvent(this.matter,convey)) ) }
                            else { this.minder.emit(this.matter,convey) };
                        });
                    }
                });

                target[LISTEN].tether = new MutationObserver(target[LISTEN].invoke.bind(target[LISTEN]));
                target[LISTEN].tether.observe(target,target[LISTEN].config);

                return this;
            };


            if (family === "obje")
            {
                Object.keys(signal).map((type)=>
                {
                    let args = [type].concat((Array.isArray(signal[type]) ? signal[type] : [signal[type]]));
                    entity.Listen(...args);
                });
            };

            return this;
        },



        Signal: function (signal, detail, config)
        {
            let entity = (this || globalThis);
            let family = Sample(signal);


            if ((typeof entity) === "function"){ entity = entity.prototype };

            if (family === "stri")
            {
                entity.dispatchEvent( new Signal(signal, detail, config) );
                return this;
            };

            if (family === "obje")
            {
                Object.keys(signal).map((type)=>
                {
                    let args = [type].concat((Array.isArray(signal[type]) ? signal[type] : [signal[type]]));
                    entity.Signal(...args);
                });
                return this;
            };


            return this;
        },



        Ignore: function(signal, invoke, config)
        {
            let entity = (this || globalThis);
            let family = Sample(signal);

            if ((typeof entity) === "function"){ entity = entity.prototype };

            if (family === "stri")
            {
                entity.removeEventListener(signal, invoke, config);
                if (signal.Locate("mutate","*") && !!entity[TARGET] && !!entity[TARGET][LISTEN])
                { entity[TARGET][LISTEN].cancel() };
                return this;
            };

            if (family === "obje")
            {
                Object.keys(signal).map((type)=>
                {
                    let args = [type].concat((Array.isArray(signal[type]) ? signal[type] : [signal[type]]));
                    entity.Ignore(...args);
                });
                return this;
            };

            return this;
        },



        Convey: function (signal, target, config, minder)
        {
            let entity = (this || globalThis);
            let family = Sample(signal);
            if ((typeof entity) === "function"){ entity = entity.prototype };

            let invoke = function invoke(evnt)
            {
                if (!this.target.emit){ this.target.dispatchEvent( new (evnt.constructor)(evnt.type, evnt.detail) ) }
                else { this.target.emit(evnt.type, {matter:"convey", detail:evnt}) };
                // this.target.Signal(evnt.type, evnt.detail, this.config, this.minder);
                // setTimeout(()=>{  this.target.Signal(evnt.type, evnt.detail, this.config, this.minder);  },0);

            };

            if (family === "stri")
            {
                signal = signal.split(",");
                family = "arra";
            };

            if (family === "arra")
            {
                signal.map((action)=>
                {
                    // let cloned = (invoke.toString()).Parsed("func");
                    entity.Listen(action, invoke.bind({target:target, config:config, minder:minder}), config, minder)
                });

                return (this||globalThis);
            };
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------




// func :: Pledge : promisify anything
// ----------------------------------------------------------------------------------------------------------------------------
    Global(function Pledge(valu)
    {
        if (!(valu instanceof Promise))
        {
            valu = new Promise(function then(done,fail)
            {
                if ((typeof this.matter)!=="function"){ done(this.matter); return };
                try { done(this.matter() ) }catch(fail){ fail(fail) };
            }
            .bind({matter:valu}))
        };
        return valu;
    });
// ----------------------------------------------------------------------------------------------------------------------------




// tool :: Wraith : now you see me...
// ----------------------------------------------------------------------------------------------------------------------------
    class Wraith extends null
    {
        constructor(detail)
        {
            return Object.defineProperty(Object.create(Wraith.prototype), "Summon" , Sample.metaConf(function Summon()
            {
                return (((typeof this.detail)==="function") ? this.detail() : this.detail); // ...i am the void...
            }
            .bind({detail:detail})));
        }

        valueOf()
        {
            return ""; // ...ssshhhh...
        }
    }
// ----------------------------------------------------------------------------------------------------------------------------




// func :: Cloned : clone anything
// ----------------------------------------------------------------------------------------------------------------------------
    Global(function Cloned(target, copies=0)
    {
        let family = Sample(target,"*");
        let result;

        if (!copies){ return result };

        let gather = [];
        do { gather[gather.length]=target; copies-- }
        while ( copies > 0 );

        return gather;
    });
// ----------------------------------------------------------------------------------------------------------------------------




// tool :: devStorage : promise-ified key-val based IndexedDB with `localStorage` methods like: `inst.setItem("age",123)`
// ----------------------------------------------------------------------------------------------------------------------------
    class devStorage extends EventTarget
    {
        constructor(config)
        {
            super();

            this[CONFIG] = (config || {}).Supply
            ({
                [ ENTITY ] : this,
                dbase: "System",
                store: "Memory",
                index: "key",
                value: "val",
                allow: "readwrite",
                model: 1,
            });

            this[CONFIG].Supply({start:function start(evnt)
            {
                this[ DEVICE ] = evnt.target.result;
                this[ DRIVER ] = this[ DEVICE ].createObjectStore(this.store, {keyPath: this.index});
                if ((typeof this.arise)!=="object"){ return };
                this[ENTITY].getKeys((mykeys)=>
                {
                    let arkeys = this.arise.Indice();
                    let deploy = [];  arkeys.map((anonym)=>
                    {
                        if ( mykeys.includes(anonym) ){ return }; // already have it
                        this[ENTITY].setItem(anonym, this.arise[anonym]);
                    });
                });
            }
            .bind(this[CONFIG])});

            this.Vivify();
        }



        async Vivify()
        {
            return new Promise(function then(done,fail)
            {
                if ( !!this[TETHER] && ((typeof this[TETHER].transaction)==="function") )
                { done(this); return };

                this[TETHER] = indexedDB.open(this[CONFIG].dbase, this[CONFIG].model);
                this[TETHER].onupgradeneeded = this[CONFIG].start;
                this[TETHER].onerror = function onerror(e){ this.Signal("failed",e); fail(e) }.bind(this);
                this[TETHER].onsuccess = function onsuccess(e)
                {
                    this[TETHER] = e.target.result;
                    this[TETHER].onerror = function onerror(e){ this.Signal("failed",e); fail(e) }.bind(this);
                    this[TETHER].target =
                    this[TETHER].minder = this;
                    Timing.await(()=>{ return ((typeof this[TETHER].transaction)==="function") }).then(()=>
                    {
                        this.dispatchEvent( new Signal("primed",this) );
                        done(this)
                    });
                }.bind(this);
            }.bind(this));
        }



        async Pacify()
        {
            if (!this[TETHER]){ return false };
            this[TETHER].close(); delete this[TETHER];
            this.dispatchEvent( new Signal("closed",this) );
            return true;
        }



        async Commit(todo, args=[])
        {
            return new Promise(function then(done,fail)
            {
                this.Vivify().then(()=>
                {
                    let trgt = this[TETHER].transaction([this[CONFIG].store], this[CONFIG].allow);
                    trgt.onerror = function onerror(e){ this.Signal("failed",e); fail(e) }.bind(this);
                    let stor = trgt.objectStore(this[CONFIG].store);
                    let qery = stor[todo](...args);
                    qery.onsuccess = function onsuccess(e){ done(e.target.result) };
                });
            }.bind(this));
        }



        async setItem(name, valu)
        {
            return this.Commit("put", [{[ this[CONFIG].index ]:name, [ this[CONFIG].value ]:valu}]);
        }



        async getItem(name)
        {
            return new Promise(function then(done,fail)
            {
                this.Commit("get", [name]).then((resl)=>{ done((resl||{})[ this[CONFIG].value ]) }, fail);
            }.bind(this));
        }



        async getKeys()
        {
            return this.Commit("getAllKeys");
        }



        async getAll()
        {
            return new Promise(function then(done,fail)
            {
                this.Commit("getAll").then
                (
                    (data)=>
                    {
                        let resl = {};
                        data.filter((row)=>{ resl[ row[ this[CONFIG].index ] ] = row[ this[CONFIG].value ] });
                        done(resl);
                    },
                    fail
                );
            }.bind(this));
        }



        async removeItem(name)
        {
            return this.Commit("delete", [name]);
        }



        async clear()
        {
            return this.Commit("clear");
        }
    }
// ----------------------------------------------------------------------------------------------------------------------------




// tool :: Sensor : extensible EventTarget & Proxy
// ----------------------------------------------------------------------------------------------------------------------------
    Global(class Sensor extends EventTarget
    {
        constructor(target, minder)
        {
            super();


            minder = new Minder(minder).Supply
            ({
                [ IDLING ]: 360,
                [ GATHER ]: 0,
                [ ENTITY ]: this,
                [ TARGET ]: (target || (new EventTarget())),
                [ CONFIG ]: {},
                [ STATUS ]: {},
                [ RETAIN ]: [],
                [ RELAYS ]: {},
                [ CONVEY ]: ENTITY,
                [ INVOKE ]:
                {
                    nosync: async function nosync()
                    {
                        let params = [...arguments].Suffix( Cloned(undefined,9).Insert(this.minder) );
                        let matter = this.matter;
                        let minder = this.minder.Summon();
                        let action = "action",  detail = {matter:matter, detail:params},  output;
                        try{ output = (this.entity[this.matter])( ...params ) }
                        catch (fail){ action="failed";  output=fail };
                        if (minder[matter] !== this.entity[matter]){ minder.emit(action, detail) };
                        return output;
                    },

                    insync: function insync()
                    {
                        let params = [...arguments].Suffix( Cloned(undefined,9).Insert(this.minder) );
                        let output = this.entity[this.matter](...params);
                        return output;
                    }
                }
            });


            minder.Supply
            ({
                get(entity, matter)
                {
                    let signal = true;
                    let relays = this[RELAYS];

                    if (relays[MINDER].includes(matter)){ return ((matter === MINDER) ? this : this[matter]) }
                    else if (relays[TARGET].includes(matter)){ entity = this[TARGET] };

                    let result = entity[matter];
                    if (((typeof matter) === "symbol") || ((typeof result) !== "function")){ return result };
                    let nisync = (((result.constructor.name === "AsyncFunction") ? "no" : "in") + "sync");
                    if (!EventTarget.prototype[matter]){ this.emit("select",{matter:matter, detail:result}) };
                    return this[INVOKE][nisync].bind({entity:entity, matter:matter, minder:new Wraith(this)});
                },


                set(entity, matter, detail)
                {
                    let action = ((Object.keys(entity).indexOf(matter) < 0) ? "create" : "update");
                    entity[matter] = detail;
                    if ((typeof matter)==="symbol"){ return true };
                    this.emit(action, {matter:matter, detail:detail});
                    return true;
                },


                getPrototypeOf(entity)
                {
                    return (entity.constructor.prototype);
                },


                ownKeys(entity)
                {
                    let action = "descry";
                    let result = Reflect.ownKeys(entity);
                    this.emit(action, {matter:"ownKeys", detail:result});
                    return result;
                },


                has(entity, matter)
                {
                    let action = "exists";
                    let result = (Sample.allKeys(entity).indexOf(matter) > -1);
                    this.emit(action, {matter:matter, detail:result});
                    return result;
                },


                modifyProperty(entity, config)
                {
                    let action = "modify";
                    Sample.modify(entity,config);
                    this.emit(action,{matter:config.name, detail:config});
                    return self;
                },


                deleteProperty(entity, anonym)
                {
                    let action = "remove";
                    this.emit(action,{matter:anonym, detail:entity[anonym]});
                    delete entity[anonym];
                    return true;
                },


                emit: function emit(action, detail, silent=false)
                {
                    if (!silent)
                    { this.parent.dispatchEvent( new ActionEvent(action,detail)) }; // do not change this to: "Listen"
                    return {action:action}.Supply(detail);
                }
                .Relate({parent:minder})
                .Absorb(minder[IDLING], minder[GATHER])
                .Convey("idling,primed,absorb,gather,settle", minder[ (minder[CONVEY]) ], {}, minder)
            });

            minder[RELAYS].Supply
            ({
                [ MINDER ]: Reflect.ownKeys(minder).Insert(MINDER),
                [ TARGET ]: [],
                [ ENTITY ]: Reflect.ownKeys(this),
            });

            minder.Supply
            ({
                [ MIRROR ]: new Proxy(this,minder),
            });

            return minder[ MIRROR ];
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------




// tool :: Memory : storage hybrid .. first arg can be `undefined`, or any of: sessionStorage, localStorage, IndexedDB
// ----------------------------------------------------------------------------------------------------------------------------
    Global(class Memory extends Sensor
    {
        constructor(target=sessionStorage, config)
        {
            if (!target || !target.getItem)
            { throw "expecting 1st argument as any: sessionStorage, localStorage, or IndexedDB"; return };

            super(target, config);

            if (this[TARGET].addEventListener)
            {
                this.Listen("idling",(signal)=>{ signal.preventDefault() });
                this[TARGET].addEventListener("primed", ()=>{ this[ENTITY][STATUS]="primed" }, {once:true});
            };
        }



        async Select(params)
        {
            let target = [...arguments].pop().Summon()[TARGET];

            if ((typeof params) === "string")
            {
                let result = await target.getItem( btoa(params, SECRET) );
                if ((typeof result) !== "string"){ return }; // undefined
                return atob(result, SECRET);
            };
        }



        async Create(params, ovalue)
        {
            let minder = [...arguments].pop().Summon()
            let target = minder[TARGET];
            if ((typeof params) === "string")
            {
                let secret = [btoa(params,SECRET), btoa((ovalue+""),SECRET)];
                let result = await target.setItem(...secret);
                return minder[MIRROR];
            };
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------




// tool :: Driver : extensible EventTarget & Proxy
// ----------------------------------------------------------------------------------------------------------------------------
    Global(class Driver extends Sensor
    {
        constructor(target, config, sencon, memcon)
        {
            super(target, sencon); // extended class

            memcon = (memcon || {}).Supply({dbase:"Driver", store:"Memory"});
            memcon = new devStorage(memcon);

            this[ENTITY][MEMORY] = new Memory(memcon);
            this[ENTITY][CONFIG] = new Config(config);
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------




// prep :: boot : strap .. nothing to do with Bootstrap's boot-straps .. well maybe a bit .. what aaRRR u doin?
// ----------------------------------------------------------------------------------------------------------------------------
    Global({IndexedDB: new devStorage()});

    localStorage.Assign
    ({
        getAll()
        {
            let resl = {};
            Object.keys(this).map((name)=>
            { resl[name] = this.getItem(name) });
            return resl;
        }
    });

    System(MODULE).Aarden = class Aarden {};
    Global({Aarden: new (System(MODULE).Aarden)});
// ----------------------------------------------------------------------------------------------------------------------------
