# aarden
> *technical documentation*

## Introduction
This document expects that you have a very basic grasp of the following languages: JavaScript, CSS, HTML and SQL. Knowing more or less how these are expressed in technical terminology will help a lot in answering "why" Aarden exists, but for now: "Aarden is an extensible CRUD-based tool library".

As you may know, "crud" is an acronym that refers to well known verbs used in any programming language, especially SQL; in Aarden-speak "crud" means: `Select Create Remove Update Modify`, or SCRUM.

Aarden aims to simplify the grammar used to interact with devices, either front-end, or back-end, but not in limiting expression, quite the opposite, though easily understandable by simply reading the code as a human.


## Global SCRUM functions
SCRUM functions (verbs) exist globally as event emitters, though these are not elaborate blocks of hard-coded logic, instead each simply returns the result of an event listener. This needs more clarification, the following shows how a global SCRUM word like Select is made:

#### example 1.0 - how to create global scrum functions
```js

    new Global ( function Select ( intake )
    {
        return Medium.herald( Select, intake );
    });

```

What is not shown is that `Medium.herald()` does something like `return Select.signal("StringIntake",intake)` .. this is to minimize code duplication, but here is a listener that goes with the above example, elaboration follows:

#### example 1.1 - how to intercept scrum functions
```js

    Select.listen( "StringIntake", function denyRootAccess( signal )
    {
        if ( signal.detail === "/" )
        { return new Error("access denied") };
    });

```

As expected, now we can simply call `Select("/")` - which returns an error with a stack log that shows it happened at `denyRootAccess`. Aarden encourages named functions (and methods) as this is useful for debugging process flow.

The simple answer to why Aarden works this way is: extensibility, for your own unique project needs, it provides a familiar way to deal with pretty much anything, from file access and complex data queries, to client-side DOM+CSS or 3D game development.

There are different ways to use Aarden depending how it was installed/included in your project; you can choose to have Aarden automatically detect the end of a scrum-query, instead of having to use `.apply()` every time to conclude the statement.



#### Example 1.0 : Server side
```js
    // fetch all folder-names inside a folder
    let foo = Select( `/srv/http/` ).fetch("*").where(`type = "fold"`);

    // fetch all hidden items inside a folder
    let foo = Select( `/srv/http/` ).fetch("name").where(`name[0] = "."`);

    // append "hello" into a file: `/srv/http/foo.txt` .. created if not defined
    Insert( {"foo.txt":"hello"} ).using("/srv/http/");

    // Create empty file
    Create( "/srv/http/foo.txt" );

    // Create file with contents
    Create( "/srv/http/foo.txt" ).write("hello");
```


### Select
This is usually called with a string or object; the following explains:


### Create
This is usually called with a string or object; the following explains:

#### CSS
```js

    Create( ".darkTheme" ).apply
    ({
        "background-color": "black",
        "color": "white"
    });

```

#### HTML
```js

    Create(  );
    Render( {curtain:".darkTheme"} ).using( "#body" );

```

***

## Installation
