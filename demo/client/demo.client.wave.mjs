
// load :: client : module
// ----------------------------------------------------------------------------------------------------------------------------
    import "../../dist/client/aard.client.mjs";
// ----------------------------------------------------------------------------------------------------------------------------





// draw :: render : view
// ----------------------------------------------------------------------------------------------------------------------------
    let wave = Array.Create({wave:"sine", length:90});

    let time = Timing.frame(wave.buffer).then(function(buffer)
    {
        let length = (wave.radius * 2), number = (0|buffer.y);
        let filler = ("#").Cloned(number);
        let spaces = (" ").Cloned((length - ((number<0)?(number*-1):number)));
        dump( filler + spaces +"      "+ buffer.y );
    });

    Timing.await(9000).then(()=>{ time.cancel() });
// ----------------------------------------------------------------------------------------------------------------------------
