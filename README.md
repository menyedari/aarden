# aarden
Abstract Bios Evolution Core


## Introduction
If you just want to install **Aarden**, see the [installation section](#installation) for quick, clear and simple instructions; else if you seek knowledge, [orientation](#orientation) follows.

![relax](bits/images/relax.jpg)

## Orientation
**Aarden** is a mighty yet tiny tool-library that runs on any platform capable of running JavaScript and is built from the ground up using *CANDRYKIS* and *SCUD* (CRUD) principles, elaborated on below.

Using a simple, yet effective extensibility approach, at its core, Aarden uses 2 classes: `Device` and `Driver`. These can be extended and instantiated for everything Aarden needs; liberal use of these are encouraged as they are but structured classes and methods with neat capabilities that compliment events, memory, proxies and configuration.

Even though these tools compliment secure `viewer <-> client <-> server` API integration, it is made to run on any host, or platform, so the logic of which runs where is up to the design specifications of what you need to build; Aarden won't get in the way, on the contrary; you can build/erect anything, from web servers to interactive VR apps, with very little code -and reading the code (in English) should make sense.

### CANDRYKIS
A software design pattern that favors portability of its constituent parts, interchangeable with other parts, for rapid prototyping and familiar structure in each component.

- **CAN** ~ Compartmentalized And Nomadic
- **DRY** ~ Don't Repeat Yourself
- **KIS** ~ Keep It Simple

### SCUD (CRUD)
Database-like methods are very useful for all things database-related; however, also useful for literally everything else, so Aarden extends on this bringing useful methods to the table; these are accessible globally as functions, and as methods of most objects, arrays and functions; for everything else that will not be handled into submission, you can use a `Driver` as mediator (proxy).

All SCUD-methods are globally -and prototypically available and context-aware, hence the global SCUD-methods assumes the `global/window` context, naturally.

These methods (functions) can be easily identified; their names have very specific rules. Each SCUD method MUST:
- begin with a capital letter
- be exactly 6 letters in length
- can be a fictional word
- the (implied) meaning of the word should be reflected in functionality


Without further adieu here are the most common SCRUM-functions in Aarden:
- `Select()` ~ returns an array of what was found in context
- `Create()` ~ creates an item in context
- `Update()` ~ alters value(s) in context
- `Insert()` ~ insert record
- `Modify()` ~ modify table
- `Remove()` ~ deletes properties/objects
- `Define()` ~ defines a named function/class in the given scope

The following may come in handy:
- `Couple()` ~ connect live resource/context
- `Detach()` ~ disconnect live resource
- `Modify()` ~ change/create field/property-names of objects
- `Insert()` ~ insert object(s) into a list/array/holder
- `Filter()` ~ callback called
- `Descry()` ~ describe some thing, context related
- `Config()` ~ get/set internal configuration -or logic
- `Handle()` ~ get/set internal configuration -or logic
- `Reckon()` ~ run native (and relative) script in context
- `Assert()` ~ used to test for non-false/empty/null output from callback called
- `Vivify()` ~ start service, or engage/activate and return promise
- `Pacify()` ~ stop service, or close -and return promise
- `Revive()` ~ Pacify -> Detach -> Couple -> Vivify .. forceful is optional
- `Render()` ~ output expected to be returned, or displayed
- `Listen()` ~ adds event and callback to events in `this` context
- `Signal()` ~ calls event by name in context
- `Ignore()` ~ remove event from emitter in context
- `Import()` ~ import string/file into context
- `Export()` ~ export context to string/file
- `Parley()` ~ negotiation API, or dialogue/wizard .. specified by context
- `Notify()` ~ negotiation API
- `Invite()` ~ negotiation API
- `Banish()` ~ negotiation API
- `Resume()` ~ negotiation API
- `Detain()` ~ negotiation API
- `Accept()` ~ negotiation API
- `Reject()` ~ negotiation API
- `Assign()` ~ assign properties + values
- `Permit()` ~ Person  API
***


## Globals






## Installation
The **aarden** library is a set of tools to use in JavaScript and runs both back-end and front-end, running inside the NodeJS -and web-browser platforms -respectively.

- server side: `npm install aarden`
- client side: `<script type="module" src="./aarden/index.mjs"></script>`



## Examples
The full documentation elaborates a lot more than the few examples below, but these should get you up to speed read quick:

```bash
aarden Server vivify
```


## Structure

### bootstrap
Every folder in aarden is potentially a globally available function (method), or object available by name; case sensitive (always).
- Each of these folders must contain 2 files:
 1. aard.client.mjs
 1. aard.server.mjs

These load resource modules and massage the expected functionality into uniformity client-side or server-side



## Process Flow :: CLI
1. the `aarden` command is available as it's installed as `./local/bin/aarden`, or: `/usr/bin/aarden`.
2. the `aarden` command relays the current process through `./local/shared/aarden`, or in SDK mode in `~/Source/code/aarden`
3. the `package.json` file directs the process to the `main` module.


### (more to come)
This section currently under construction, more info to be disclosed, though the library works.



### scud words
count fetch using alter write claim touch where group order limit
parse shape apply erase purge debug dbase table field sproc funct
after basis named param parts
